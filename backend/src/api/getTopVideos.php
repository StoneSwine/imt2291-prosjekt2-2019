<?php
/**
 * Created by PhpStorm.
 * User: joakimellestad
 * Date: 2019-04-07
 * Time: 17:30
 */

require_once realpath(dirname(__FILE__)) . "/../model/DB.php";
require_once realpath(dirname(__FILE__)) . "/../model/checkAuthentication.php";

$http_origin = $_SERVER['HTTP_ORIGIN'];
header("Access-Control-Allow-Origin: $http_origin");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=utf-8");

/* Get top videos */
$res = array();
try{
    $conn = DB::getVideoDBConnection();

    $sql = "SELECT `uuid` AS id, `title`, `description` FROM VideoMetadata WHERE `uuid` IN (SELECT `video_ref` FROM VideoRank GROUP BY `video_ref` ORDER BY AVG(`rankValue`)) LIMIT 10";
    $stmt = $conn->prepare($sql);
    $stmt->execute();

    $videos = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $res["STATUS"] = "SUCCESS";
    $res["topVideos"] = $videos;

    echo json_encode($res);

}catch(PDOException $e){
    $error["STATUS"] = "FAILURE";
    $error["message"] = "";
    echo json_encode($error);
}



