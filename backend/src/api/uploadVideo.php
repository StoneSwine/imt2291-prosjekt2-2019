<?php
/**
 * Created by PhpStorm.
 * User: Joakim
 * Date: 04/23/2019
 * Time: 7:42 PM
 */
/**
 * Her kan man enten laste opp en ny video eller oppdatere en eksisterende.
 * Om man ønsker å oppdatere en eksisterende på man ta med videoid.
 * For å oppdatere er title, description og video-fil påkrevd
 */
session_start();
require_once "../model/checkAuthentication.php";
include_once "../model/Video.php";
include_once "../model/Playlist.php";

$http_origin = $_SERVER['HTTP_ORIGIN'];
header("Access-Control-Allow-Origin: $http_origin");
header("Access-Control-Allow-Methods: POST, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=utf-8");
$res = array();
if($isLoggedIn and ($isTeacher or $isAdmin)){
    $theVideo = null;
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        if(isset($_POST["videoid"])){
            /**
             * Updating existing video
             *
             */
            $videoid = $_POST["videoid"];
            $theVideo = Video::withVideoId($videoid, $_SESSION["sessionid"]);

        }else{
            /**
             * New video
             */

            if( /* These are required to be filled*/
                !isset($_POST["title"]) or
                !isset($_POST["description"]) or
                !isset($_FILES["video"])
            ){
                // Hvis ikke title, description eller video finnes så kan vi ikke lage en ny video
                $res["status"] = "FAILURE";
                $res["message"] = "title, description and video is required.";
                echo json_encode($res);
                die();
            }
            try {
                $theVideo = Video::Video($_SESSION["sessionid"]);
            }catch(Exception $e){
                $res["status"] = "FAILURE";
                $res["message"] = "Could not create new video";
                die();
            }

            if(is_uploaded_file($_FILES["video"]["tmp_name"])) {
                try {
                    $theVideo->setVideo($_FILES["video"]);
                }catch(Exception $e){
                    $res["status"] = "FAILURE";
                    $res["message"] = "Could not upload video";
                    echo $res;
                    die();
                }
            }

        }

        /**
         * Felles setting av video metadata
         */
        if(isset($_POST["title"])){
            $title = $_POST["title"];
            $theVideo->setTitle($title);
        }

        if(isset($_POST["description"])){
            $desc = $_POST["description"];
            $theVideo->setDescription($desc);
        }

        if (isset($_POST["course"])) {
            $course = $_POST["course"];
            $theVideo->setCourse($course);
        }

        if (is_uploaded_file($_FILES["thumbnail"]["tmp_name"])) {
            /* user uploaded thumbnail*/
            /* Get base64 of it and save to db */
            $data = file_get_contents($_FILES["thumbnail"]["tmp_name"]);
            $base64 = base64_encode($data);
            $theVideo->setThumbnail($base64);
        }else if(isset($_POST["thumbnailBlob"])){
            $theVideo->setThumbnail($_POST["thumbnailBlob"]);
        }

        /**
         * Set tag på video
         */
        $tags = array();
        if (isset($_POST["tags"])) {
            $tagsString = $_POST["tags"];
            $tags = explode(",", $tagsString);
            foreach ($tags as &$tag) {
                $tag = strtolower($tag); // lowercase
                $tag = str_replace(' ', '', $tag); // remove spaces
                try {
                    $theVideo->removeAllTags(); // Remove before adding the replaced tags
                    $theVideo->addTag($tag);
                } catch (PDOException $e) {
                    $res["message"] = "Kunne ikke legge til tags";
                }
            }
        }

        /**
         * Legger video til i spilleliste
         */
        if (isset($_POST["playlist"])) {
            $playlistId = $_POST["playlist"];
            // TODO: Make sure it is a valid playlist
            // TODO: Make sure uploader owns the playlist
            $playlist = Playlist::setPlaylistId($playlistId);
            $playlist->addVideo($videoId);
        }

        /**
         * Legger til undertekst hvis brukeren har lastet det opp
         * Kan senere legge til funksjonalitet for flere språk via "lang" parameter
         */
        if(is_uploaded_file($_FILES["subtitle"]["tmp_name"])){
            try{
                $theVideo->setSubtitle($_FILES["subtitle"]);
            }catch(Exception $e) {
                $res["message"] = "Kunne ikke legge til undertekst";
            }
        }

        /**
         * If we get this far, which is really impressive, we can return a success
         */
        echo json_encode(array("status"=>"SUCCESS", "message"=>"Video ble oppdatert"));
        die();
    }else{
        // GET is not supported
    }
}else{
    $res["status"] = "FAILURE";
    $res["message"] = "You must be logged in to make this request.";
    echo json_encode($res);
    die();
}