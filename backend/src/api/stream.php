<?php
/**
 * Created by PhpStorm.
 * User: Joakim
 * Date: 02/17/2019
 * Time: 4:18 PM
 */

/**
 * stream - stream a video file to client
 *
 *
 */

include_once realpath(dirname(__FILE__)) . "/../config/video_config.php";
include_once realpath(dirname(__FILE__)) . "/../model/VideoStream.php";

if(isset($_GET["id"])){
    $id = $_GET["id"];
    /*Check if we actually have that file*/

    $filePath = VIDEO_S3_ROOT . $id . ".mp4";
    if(file_exists($filePath)){
        $stream = new VideoStream($filePath);
        $stream->start();

    }else{
        http_response_code(404);
        echo "File at: " . $filePath . " doesnt exist";
        die();
    }

}

//header("HTTP/1.0 404 Not Found");