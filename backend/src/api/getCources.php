<?php

//start session
session_start();

//Requirements
require_once "../model/User.php";
require_once "../model/Resource.php";
require_once "../model/DB.php";

$user = new User(DB::getAccountsDBConnection());

//Check if the user is already logged in (existing session OR valid cookies)
//Get the login-status of the user
require_once "../model/checkAuthentication.php";

$http_origin = $_SERVER['HTTP_ORIGIN'];
header("Access-Control-Allow-Origin: $http_origin");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=utf-8");


//The user is already logged in
if ($isLoggedIn) {

$courses = array();
        try {
            $conn = DB::getAccountsDBConnection();
            $coursesSQL = "SELECT `course_title` FROM Course";
            $stmt = $conn->prepare($coursesSQL);
            $stmt->execute();
            $courses = $stmt->fetchAll(PDO::FETCH_COLUMN, "course_title");

            echo json_encode($courses);

        } catch (PDOException $e) {
            $error = array("STATUS" =>"FAILURE", "message"=>"Could not get cources");
            echo json_encode($error);
        }
    }