<?php
/**
 * Created by PhpStorm.
 * User: Magnus
 * Date: 19.02.19
 * Time: 18:49
 */

//requires the user to be logged into the js application (frontend)
session_start();

require_once "../model/checkAuthentication.php";
require_once "./../model/Administrator.php";
require_once "./../model/DB.php";
require_once "./../model/Resource.php";

//headers to be able to send data to the frontend
$http_origin = $_SERVER['HTTP_ORIGIN'];
header("Access-Control-Allow-Origin: $http_origin");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json");

$admin = new Administrator(DB::getAccountsDBConnection());
$util = new Resource();

//Verify that the user is an admin and is logged in
if ($isLoggedIn && $isAdmin) {
//If the form with the checkboxes is submitted
    if (isset($_POST['submit_checkbox'])) {
        //loop through the key and value pair of the checkboxes
        foreach ($_POST['checkbox'] as $useremail => $key) {
            //the checkbox is checked
            if ($key) {
                //Set the users status to teacher
                $admin->setUserAsTeacher($useremail);

                //reset the request for the users that are not accepted as teachers
            } else {
                $admin->setUserAsStudent($useremail);
            }
        }
    } else if (isset($_GET['getteacherreq'])) {
        // display the normal page
        $users = $admin->getUsersThatWantsToBeTeacher();
        $res = array("names" => $users);
        echo json_encode($res);
    }
}