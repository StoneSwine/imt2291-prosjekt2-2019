<?php
/**
 * Created by PhpStorm.
 * User: Joakim
 * Date: 05/09/2019
 * Time: 7:02 PM
 */
/**
 * UpdateVideoComments
 *
 * Gir mulighet for å slette en kommentar på video. Bare admin kan gjøre dette.
 * Det vil også være mulig å lage andre tjenester på denne
 *
 */
session_start();
require_once "../model/checkAuthentication.php";
include_once realpath(dirname(__FILE__)) . "/../model/Video.php";
if($isLoggedIn) {

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST["videoid"])) {
            $http_origin = $_SERVER['HTTP_ORIGIN'];
            header("Access-Control-Allow-Origin: $http_origin");
            header("Access-Control-Allow-Methods: POST, OPTIONS");
            header("Access-Control-Allow-Headers: Origin");
            header("Access-Control-Allow-Credentials: true");
            header("Content-Type: application/json; charset=utf-8");



            $videoid = $_POST["videoid"];
            $theVideo = Video::withVideoId($videoid);


            // SLETTE KOMMENTAR PÅ VIDEO - Admin kan slette og eieren sin kommentar kan slette
            if(
                isset($_POST["deletecomment"]) &&
                isset($_POST["commentid"])
            ){
                $commentid = $_POST["commentid"];
                if($isAdmin || $isTeacher)
                {
                    try {
                        if($theVideo->deleteComment($commentid)){
                            echo json_encode(array("status"=>"SUCCESS", "message"=>"Kommentar ble sletta"));
                            die();
                        }
                    }catch (Exception $e){
                        echo json_encode(array("status"=>"FAILURE", "message"=>"Kunne ikke slette kommentar"));
                        die();
                    }
                }else {
                    try{
                        if($theVideo->deleteCommentWithAuthorizationCheck($commentid, $_SESSION["sessionid"])){
                            //Successfully deleted
                            echo json_encode(array("status"=>"SUCCESS", "message"=>"Kommentar ble sletta"));
                            die();
                        }else{
                            // Failed deleting
                            echo json_encode(array("status"=>"FAILURE", "message"=>"Kunne ikke slette kommentar"));
                        }
                    }catch(PDOException $e){
                        // Deletion could not be done - the comment might not exist or authorization failed
                        echo json_encode(array("status"=>"FAILURE", "message"=>"Kunne ikke slette kommentar"));
                        die();
                    }
                }
                echo json_encode(array("status"=>"FAILURE", "message"=>"Kunne ikke slette kommentar"));
                die();
            }


            // ADD COMMENT
            if(
                isset($_POST["addcomment"]) &&
                isset($_POST["comment"])
            ){
                $comment = $_POST["comment"];
                try {
                    if($theVideo->addComment($comment, $_SESSION["sessionid"])){
                        echo json_encode(array("status"=>"SUCCESS", "message"=>"Kommentar ble lagt til"));
                        die();
                    }else{
                        echo json_encode(array("status"=>"FAILURE", "message"=>"Kunne ikke legge til kommentar"));
                        die();
                    }
                }catch (Exception $e){
                    echo json_encode(array("status"=>"FAILURE", "message"=>"Kunne ikke legge til kommentar"));
                    die();
                }
            }

            echo json_encode(array("status"=>"FAILURE","message"=>"Ingen handling ble utført"));
            die();

        } else {
            echo json_encode(array("status"=>"FAILURE", "message"=>"Må ha videoid"));
            die();
        }


    }else{
        // GET has no power here
    }
}
