<?php
/**
 * Created by PhpStorm.
 * User: stone
 * Date: 24.02.19
 * Time: 08:08
 */

//get the seession cookie from the user
session_start();

//requirments
require_once "../model/checkAuthentication.php";
require_once "../model/Resource.php";
require_once "../model/Hawkeye.php";
require_once "../model/DB.php";

$util = new Resource();

//Headers to be able to send data to frontend
$http_origin = $_SERVER['HTTP_ORIGIN'];
header("Access-Control-Allow-Origin: $http_origin");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=utf-8");

//if the form is submitted or the get request is made
if (isset($_POST['submit']) || isset($_GET['query'])) {

    //get the data from the parameter
    $query = isset($_POST['submit']) ? $_POST['query'] : $_GET['query'];

    //create new classes
    $searchclass = new Hawkeye(DB::getVideoDBConnection());

    //Try to perform a search
    $result = $searchclass->userSubmittedSearch($query);

    //if there was no result from the query
    if (count($result) == 0) {
        //return message
        $res = array("message" => "No results found with the query \"" . $_REQUEST['query'] . "\"");
        echo json_encode($res);
    }
    else {//There was a result
        $res = array( //return the right format of the results
            "searchresults" => $result,
            "user" => $userdata,
        );
        echo json_encode($res);
    }

} else { // no form was submitted: no action
   
}