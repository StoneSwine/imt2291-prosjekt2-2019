<?php
/**
 * Created by PhpStorm.
 * User: joakimellestad
 * Date: 2019-04-07
 * Time: 16:56
 */
include_once realpath(dirname(__FILE__)) . "/../model/Video.php";
include_once realpath(dirname(__FILE__)) . "/../model/Playlist.php";

if($_SERVER["REQUEST_METHOD"] == "GET") {

      //Get a video's thumbnail image decoded from base64 to a png
    if (isset($_GET["videoid"])) {
        $videoid = $_GET["videoid"];
        $video = Video::withVideoId($videoid);

        try{
            header('Content-Type: image/png');
            $http_origin = $_SERVER['HTTP_ORIGIN'];
            header("Access-Control-Allow-Origin: $http_origin");

            echo base64_decode($video->getThumbnail());
        }catch (Exception $e){

        }

    }
    //Get a playlists thumbnail image decoded from base64 to a png
    else if(isset($_GET["playlistid"])){
        $playlistid = $_GET["playlistid"];
        $playlist = Playlist::setPlaylistId($playlistid);

        try{
            header('Content-Type: image/png');
            $http_origin = $_SERVER['HTTP_ORIGIN'];
            header("Access-Control-Allow-Origin: $http_origin");

            echo base64_decode($playlist->getThumbnail()["thumbnail"]);
        }catch (Exception $e){

        }
    }
}