<?php
/**
 * Created by PhpStorm.
 * User: Joakim
 * Date: 05/09/2019
 * Time: 5:51 PM
 */
/**
 * Kommentarer for video
 * Send med videoid så får du kommentarer tilbake
 */
include_once realpath(dirname(__FILE__)) . "/../model/Video.php";
if($_SERVER["REQUEST_METHOD"] == "GET") {

    if (isset($_GET["videoid"])) {
        $http_origin = $_SERVER['HTTP_ORIGIN'];
        header("Access-Control-Allow-Origin: $http_origin");
        header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
        header("Access-Control-Allow-Headers: Origin");
        header("Access-Control-Allow-Credentials: true");
        header("Content-Type: application/json; charset=utf-8");

        $videoid = $_GET["videoid"];
        try {
            $video = Video::withVideoId($videoid);

            $comments = $video->getComments();
            $res = array("status" => "SUCCESS", "comments" => $comments);
            echo json_encode($res);
            die();
        }catch(Exception $e){
            echo json_encode(array("status"=>"FAILURE", "message"=>"Kunne ikke hente kommentarer for video"));
            die();
        }
    }
}