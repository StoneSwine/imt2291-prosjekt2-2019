<?php
/**
 * Created by PhpStorm.
 * User: Magnus
 * Date: 19.02.19
 * Time: 18:49
 */


 //requires the user to be logged into the js application (frontend)
session_start(); 

require_once "../model/checkAuthentication.php";
require_once "./../model/Administrator.php";
require_once "./../model/DB.php";
require_once "./../model/Resource.php";

$http_origin = $_SERVER['HTTP_ORIGIN'];
header("Access-Control-Allow-Origin: $http_origin");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json");

$admin = new Administrator(DB::getAccountsDBConnection());
$util = new Resource();

//Verify that the user is an admin and is logged in
if ($isLoggedIn && $isAdmin) {

if (isset($_POST['submit_search']) && !empty($_POST['search_text'])) {
        //try to get the full username or email
        $searchtext = $_POST['search_text'];

        //try to get the full email
        if ($result = $admin->getUserByUsername($searchtext)) {
            $res = array("searchResults" => $result);
            echo json_encode($res);
        }//try to get the full first- and lastname of the user
        else if ($result = $admin->getUserByFirstAndOrLastname($searchtext)) {
            $res = array("searchResults" => $result);
            echo json_encode($res);
        }//cant find the user
        else {
            $string = "Kan ikke finne en bruker med navn: ". $searchtext;
            $res = array("message" => $string);
            echo json_encode($res);
        }
}
}
