<?php


//start session
session_start();

//Requirements
require_once "../model/User.php";
require_once "../model/Resource.php";
require_once "../model/DB.php";
require_once "../model/Playlist.php";

$user = new User(DB::getAccountsDBConnection());

//Check if the user is already logged in (existing session OR valid cookies)
//Get the login-status of the user
require_once "../model/checkAuthentication.php";

$http_origin = $_SERVER['HTTP_ORIGIN'];
header("Access-Control-Allow-Origin: $http_origin");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=utf-8");


//The user is already logged in
if ($isLoggedIn) {
  if (isset($_POST["newplaylist"]) && isset($_POST["playlisttitle"]) && isset($_POST["playlistdescription"])) {
    $title = $_POST["playlisttitle"];
    $description = $_POST["playlistdescription"];

    $course = isset($_POST["course"]) ? $_POST["course"] : null;

    $thumbnail = null;
    if (is_uploaded_file($_FILES["thumbnail"]["tmp_name"])) {
        $thumbnail = base64_encode(file_get_contents($_FILES["thumbnail"]["tmp_name"]));
    }
    try {
        $newPlaylist = Playlist::newPlaylist($title, $description, $_SESSION["sessionid"], $course, $thumbnail);
    } catch (PDOException $e) {
        $error = array("STATUS" =>"FAILURE", "message"=>"Could not create playlist");
        echo json_encode($error);
    }
  }
}
