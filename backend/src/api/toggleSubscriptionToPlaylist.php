<?php
/**
 * Created by PhpStorm.
 * User: joakimellestad
 * Date: 2019-05-07
 * Time: 10:42
 */
/**
 * Subscribe or unsubscribe from playlists
 * Needs to be logged in, because sessionid is required!
 */
session_start();

require_once realpath(dirname(__FILE__)) . "/../model/DB.php";
require_once realpath(dirname(__FILE__)) . "/../model/checkAuthentication.php";
require_once realpath(dirname(__FILE__)) . "/../model/Playlist.php";

$http_origin = $_SERVER['HTTP_ORIGIN'];
header("Access-Control-Allow-Origin: $http_origin");
header("Access-Control-Allow-Methods: POST, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=utf-8");

if($isLoggedIn){

    if (isset($_POST["playlistid"])) {

        $playlistId = $_POST["playlistid"];
        $playlist = Playlist::setPlaylistId($playlistId);
        $isSubscribed = $playlist->isSubscribed($_SESSION["sessionid"]);
        if($isSubscribed){
            $playlist->removeSubscriber($_SESSION['sessionid']);
        }else{
            $playlist->addSubscriber($_SESSION['sessionid']);
        }

        $res = array("status"=>"SUCCESS", "message"=>"Successfully changed the subscription status");
        echo json_encode($res);
    }
}