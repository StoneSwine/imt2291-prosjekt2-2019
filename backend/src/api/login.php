<?php

//start session
session_start();

//Requirements
require_once "../model/User.php";
require_once "../model/Resource.php";
require_once "../model/DB.php";

$user = new User(DB::getAccountsDBConnection());

//Check if the user is already logged in (existing session OR valid cookies)
//Get the login-status of the user
require_once "../model/checkAuthentication.php";

$http_origin = $_SERVER['HTTP_ORIGIN'];
header("Access-Control-Allow-Origin: $http_origin");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=utf-8");


//The user is already logged in
if ($isLoggedIn) {
    //Return success
    //return userdata
    $userdata = $user->getUser();
    $res = array(
        "status"=>"SUCCESS",
        "uuid"=>$_SESSION["sessionid"],
        "email"=>$userdata["email"],
        "firstname"=>$userdata["firstName"],
        "lastName"=>$userdata["lastName"],
        "isTeacher"=>$isTeacher,
        "isStudent"=>$isStudent,
        "isAdmin"=> $isAdmin
    );
    echo json_encode($res);


} else if (isset($_POST['email']) and isset($_POST['password'])) {//The form is submitted

    //Get the credentials from the form
    $credentials = array(
        'username' => $_POST['email'],
        'password' => $_POST['password']
    );

    //try to get the user with the given username from the database
    $currentuser = $user->getUserByUsername($credentials['username']);
    //get the first user
    $currentuser = $currentuser[0];
    //If a user if found in the database and the user is verified
    if ($currentuser && password_verify($credentials['password'], $currentuser["password_hash"])) {

        // Set Cookie in browser if 'Remember Me' is checked
        if (isset($_POST["rememberPassword"])) {

            //Generate new selector token
            $selector = Resource::generateToken(12);

            //Generate new validator token
            $validator = Resource::generateToken(24);

            //Generate hashed-validator for the database according best practice
            $hashedvalidator = hash("sha256", $validator);

            // Generate and set the cookie:
            // Cookie-format:
            //      remember_me=<SELECTOR>:<VALIDATOR>
            $cookie = $selector . ":" . $validator;
            setcookie("remember_me", $cookie, $cookie_expiration_time);

            //TODO: create new cookie and delete the old ones

            //get some data from the session
            $expirationdate = date("Y-m-d H:i:s", $cookie_expiration_time);
            $useragent = $_SERVER['HTTP_USER_AGENT'];
            $userip = Resource::getUserIP();

            //Delete existing token/cookie in the database
            $user->deleteToken($currentuser['userId']);
            //insert a new token in the database
            $user->insertToken($currentuser['userId'], $selector, $hashedvalidator, $expirationdate, $useragent, $userip);
        }

        //set the session ID if the user is logged in correctly AND it is not set already
        if (!isset($_SESSION["sessionid"])) {
            $user->setSessionId($currentuser["userId"]);
        }

        if (isset($_SESSION['teacher'])) {
            $isTeacher = true;
        } else if (isset($_SESSION['admin'])) {
            $isAdmin = true;
        } else {
            $isStudent = true;
        }
        //return userdata
        $userdata = $user->getUser();
        $res = array(
            "status"=>"SUCCESS",
            "uuid"=>$_SESSION["sessionid"],
            "email"=>$userdata["email"],
            "firstname"=>$userdata["firstName"],
            "lastName"=>$userdata["lastName"],
            "isTeacher"=>$isTeacher,
            "isStudent"=>$isStudent,
            "isAdmin"=> $isAdmin
        );
        echo json_encode($res);
    } else {
        $res = array("status"=>"FAILURE","message"=>"Kombinasjonen av brukernavn og passord er ukjent.🤷");
        //Render page with invalid login message
        echo json_encode($res);
    }

//The login-form is not submitted AND the user is not logged in from before
} else {
    $res = array("status"=>"FAILURE","message"=>"Ukjent handling!");
    echo json_encode($res);
}