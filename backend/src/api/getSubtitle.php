<?php
/**
 * Created by PhpStorm.
 * User: joakimellestad
 * Date: 2019-04-09
 * Time: 10:01
 */

/**
 * Takes a videoid and a language code
 * Gets the correct subtitle from the filesystem and sends it back.
 *
 * The subtitles have a naming convention like: <videoid>-subtitle-<lang code>.vtt
 */

include_once realpath(dirname(__FILE__)) . "/../config/video_config.php";




// Get the videoid
if(isset($_GET['id'])){
    $videoid = $_GET['id'];
    $langcode = "en";
    if(isset($_GET["lang"])){
        $langcode = $_GET["lang"];
    }
    // File traversal is possible here probably...
    $subtitlepath = VIDEO_S3_ROOT . $videoid . "-subtitle-" . $langcode . ".vtt";
    if(@file_exists($subtitlepath)){

        $http_origin = $_SERVER['HTTP_ORIGIN'];
        header("Access-Control-Allow-Origin: $http_origin");
        header("Access-Control-Allow-Methods: GET, OPTIONS");
        //header('Content-Description: Subtitles');
        //header("Access-Control-Allow-Headers: X-Requested-With");
        header('Content-Type: text/vtt');
        header('Content-Length: ' . filesize($subtitlepath));

        readfile($subtitlepath);
    }else{
        http_response_code(404);
        echo "Den forespurte filen " . $subtitlepath . " finnes ikke";
        die();
    }

}