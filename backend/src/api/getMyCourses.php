<?php
/**
 * Created by PhpStorm.
 * User: Joakim
 * Date: 04/23/2019
 * Time: 7:58 PM
 */
/**
 * Get the courses the user is attached to.
 */
require_once realpath(dirname(__FILE__)) . "/../model/DB.php";
require_once "../model/checkAuthentication.php";

$http_origin = $_SERVER['HTTP_ORIGIN'];
header("Access-Control-Allow-Origin: $http_origin");
header("Access-Control-Allow-Methods: GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=utf-8");

$res = array();
if($isLoggedIn and $_SERVER["REQUEST_METHOD"] == "GET") {

    /*Get the use courses*/

    $sql = "SELECT `course_title` as name, `course_title` as id, `description` FROM Course WHERE `course_title` IN (SELECT `course_ref` FROM UserWithCourse WHERE `user_ref`=:userid)";
    $conn = DB::getAccountsDBConnection();
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(":userid", $_SESSION["sessionid"]);
    $ifCourses = $stmt->execute();
    if ($ifCourses) {
        $courses = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $res["courses"] = $courses;
        $res["STATUS"] = "SUCCESS";
        echo json_encode($res);
    }else {
        $res["message"] = "Could not get courses";
        $res["STATUS"] = "FAILURE";
        echo json_encode($res);
    }


}else{
    $res["STATUS"] = "FAILURE";
    $res["message"] = "Please login first: ";
    echo json_encode($res);
}