<?php
/**
 * Created by PhpStorm.
 * User: Joakim
 * Date: 04/23/2019
 * Time: 7:58 PM
 */
/**
 * En bruker sine opplastede videoer
 */
session_start();

require_once realpath(dirname(__FILE__)) . "/../model/DB.php";
require_once realpath(dirname(__FILE__)) . "/../model/checkAuthentication.php";
require_once realpath(dirname(__FILE__)) . "/../model/Video.php";

$http_origin = $_SERVER['HTTP_ORIGIN'];
header("Access-Control-Allow-Origin: $http_origin");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=utf-8");


if($isTeacher || $isAdmin){
    if ($_SERVER["REQUEST_METHOD"] == "GET"){
        $res = array();
        $videos = array();
        // Get this users playlist. Only trust the session id
        try {
            $conn = DB::getVideoDBConnection();
            $sql = "SELECT `uuid` AS id, `title`, `description`, `owned_by`, `course_link` FROM VideoMetadata WHERE `owned_by`= :userid";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(":userid", $_SESSION["sessionid"]);
            $stmt->execute();

            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as &$video) {

                array_push($videos, $video);
            }


            $res = array("STATUS" => "SUCCESS", "myvideos" => $videos);
            echo json_encode($res);
            die();

        } catch (Exception $e) {
            $error = array("STATUS" =>"FAILURE", "message"=>"Could not get your videos");
            echo json_encode($error);
            die();
        }

    }
}
