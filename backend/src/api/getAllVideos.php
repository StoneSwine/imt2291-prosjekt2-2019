<?php
/**
 * Created by PhpStorm.
 * User: joakimellestad
 * Date: 2019-04-12
 * Time: 09:25
 */

/**
 * Get all the videos available.
 */
require_once realpath(dirname(__FILE__)) . "/../model/DB.php";

$http_origin = $_SERVER['HTTP_ORIGIN'];
header("Access-Control-Allow-Origin: $http_origin");
header("Access-Control-Allow-Methods: GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=utf-8");

$res = array();
try{
    $conn = DB::getVideoDBConnection();
    $sql = "SELECT `uuid` as id, `title`, `owned_by`, `VideoMetadata`.`description`, `course_link` FROM `VideoMetadata`";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $videos = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $res["STATUS"] = "SUCCESS";
    $res["allVideos"] = $videos;
    echo json_encode($res);
}catch(Exception $e){
    $error = array();
    $error["STATUS"] = "FAILURE";
    $error["message"] = "Database access failed";
    echo json_encode($error);
}