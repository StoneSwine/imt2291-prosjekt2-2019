<?php
/**
 * Created by PhpStorm.
 * User: joakimellestad
 * Date: 2019-04-07
 * Time: 17:50
 */
/**
 * - Endre rekkefølgen på video i spillelister
 * - Endre metadata
 * - slette spilleliste
 * -
 */
session_start();

require_once realpath(dirname(__FILE__)) . "/../model/DB.php";
require_once realpath(dirname(__FILE__)) . "/../model/checkAuthentication.php";
require_once realpath(dirname(__FILE__)) . "/../model/Playlist.php";

$http_origin = $_SERVER['HTTP_ORIGIN'];
header("Access-Control-Allow-Origin: $http_origin");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=utf-8");



if($_SERVER["REQUEST_METHOD"] == "POST") {

    /**
     * Oppdatere/endre metadata om spilleliste
     */
    if(isset($_POST['updatePlaylist'])) {
        try {//Get the variables from the request
            $playlistid = $_POST['playlistid'];
            $playlisttitle = $_POST['playlisttitle'];
            $playlistdescription = $_POST['description'];
            //update the playlist
            $playlist = Playlist::setPlaylistId($playlistid);
            $playlist->updatePlaylist($playlistid, $playlisttitle, $playlistdescription);

            //return sucessfull message
            $successfull = array("status" => "SUCCESS");
            echo json_encode($successfull);
            die();
            //return errormessage
        } catch (PDOException $e) {
            $error = array("status" => "FAILURE", "message" => "Could not update playlist");
            echo json_encode($error);
            die();
        }
    }


    /**
    * Endre rekkefølgen på video i spillelister
    * Motta spilleliste id og videoid + sin rekkefølge
    */
    if (isset($_POST["changeVideoPosition"])){
        if(
            isset($_POST["playlistid"]) &&
            isset($_POST["videoid"]) &&
            isset($_POST["position"])
        ){
            $playlistid = $_POST["playlistid"];
            $videoid = $_POST["videoid"];
            $newPosition = $_POST["position"];

            $playlist = Playlist::setPlaylistId($playlistid);

            try {
                $playlist->setPositionOnVideo($newPosition, $videoid);
            } catch (Exception $e) {
                echo json_encode(array("status"=>"FAILURE", "message"=>"Kunne ikke endre rekkefølge på video"));
                die();
            }
            echo json_encode(array("status"=>"SUCCESS", "message"=>"Endra posisjon på video i spilleliste"));
            die();
        }else{
            // Møter ikke kravene til post data
            $res = array("status"=>"FAILURE", "message"=>"Mangler spillelisteid, videoid eller psotion");
            echo json_encode($res);
            die();
        }
    }

    // Remove from playlist
    if(
        isset($_POST["playlistid"]) &&
        isset($_POST["remove"]) &&
        isset($_POST["videoid"])
    ){
        $playlistid = $_POST["playlistid"];
        $playlist = Playlist::setPlaylistId($playlistid);
        $videoid = $_POST["videoid"];
        $playlist->removeVideo($videoid);
    }

    /**
     * SLETTE SPILLELISTE
     */
    if (isset($_POST["deletePlaylist"])){
        $playlistid = $_POST["playlistid"];
        $playlist = Playlist::setPlaylistId($playlistid);
        try{
            $playlist->deletePlaylist();
        }catch(PDOException $e){
            echo json_encode(array("status"=>"FAILURE", "message"=>"Kunne ikke slette spilleliste"));
            die();
        }
        echo json_encode(array("status"=>"SUCCESS","message"=>"Spilleliste ble slettet"));
        die();
    }



}else{
    // Må ha POST for faaan
    echo json_encode(array("status"=>"FAILURE", "message"=>"Do you even HTTP..."));
    die();
}