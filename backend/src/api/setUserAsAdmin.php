<?php
/**
 * Created by PhpStorm.
 * User: Magnus
 * Date: 19.02.19
 * Time: 18:49
 */


 //requires the user to be logged into the js application (frontend)
session_start(); 

require_once "../model/checkAuthentication.php";
require_once "./../model/Administrator.php";
require_once "./../model/DB.php";
require_once "./../model/Resource.php";

//headers to be able to send data to the frontend
$http_origin = $_SERVER['HTTP_ORIGIN'];
header("Access-Control-Allow-Origin: $http_origin");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json");

$admin = new Administrator(DB::getAccountsDBConnection());
$util = new Resource();

//Verify that the user is an admin and is logged in
if ($isLoggedIn && $isAdmin) {
        //selected user to be promoted as admin
    if ($_GET['setasadmin']) {
        $userid = $_GET['setasadmin'];

        //make sure the user has a role which is not admin
        $role = $admin->getRoleByUserID($userid);
        if($role != "" && $role != "admin") {
            //promote the user to admin (swap the roles)
            $admin->changeUserRole($userid, $admin->getRoleByUserID($userid), "admin");
            echo json_encode(array("STATUS" => "SUCCESS", "MSG" => "Successfully set the user as admin"));
        }
        else { //the user is already an admin
            echo json_encode(array("STATUS" => "ERROR", "MSG" => "The user is already admin"));
        }
    }
    else {//the right get parameter is not set
        echo json_encode(array("STATUS" => "ERROR", "MSG" => "Error. try again"));
    }
}