<?php
/**
 * Created by PhpStorm.
 * User: stone
 * Date: 12.02.19
 * Time: 12:46
 *
 * This function is inspired from:
 *      https://phppot.com/php/secure-remember-me-for-login-using-php-session-and-cookies/
 *
 *
 * Logs out a user and
 */

require_once "../model/User.php";
require_once "../model/DB.php";

//start session
session_start();

$http_origin = $_SERVER['HTTP_ORIGIN'];
header("Access-Control-Allow-Origin: $http_origin");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=utf-8");

$user = new User(DB::getAccountsDBConnection());

//clear cookies: modify headers before sending output
$user->clearCookie();

$res = array("status"=>"SUCCESS");
echo json_encode($res);

//Clear Session
$_SESSION['sessionid'] = "";
session_destroy();
