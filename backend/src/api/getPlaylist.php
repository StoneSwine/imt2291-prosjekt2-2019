<?php
/**
 * Created by PhpStorm.
 * User: Joakim
 * Date: 05/07/2019
 * Time: 1:48 PM
 */

/**
 * GET playlists
 * Om du ønsker dine egne, alle eller bare en spesiell spilleliste kan denne brukes.
 */

session_start();

require_once realpath(dirname(__FILE__)) . "/../model/DB.php";
require_once realpath(dirname(__FILE__)) . "/../model/checkAuthentication.php";
require_once realpath(dirname(__FILE__)) . "/../model/Playlist.php";
require_once realpath(dirname(__FILE__)) . "/../model/Video.php";
/**
 * Serve back json content
 */
$http_origin = $_SERVER['HTTP_ORIGIN'];
header("Access-Control-Allow-Origin: $http_origin");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=utf-8");

/**
 * Get metadata about a specific playlist
 */
if($_SERVER["REQUEST_METHOD"] == "GET"){

    if(isset($_GET["playlistid"])){
        $playlistId = $_GET["playlistid"];
        $playlist = Playlist::setPlaylistId($playlistId);

        try{
            $playlistMeta = $playlist->getMetadata();
            $playlistMeta["subscribed"] = $playlist->isSubscribed($_SESSION['sessionid']);
            $playlistMeta["videos"] = $playlist->getPlaylist();
            foreach ($playlistMeta["videos"] as &$video) {
                $v = Video::withVideoId($video["id"]);
                //$video["thumbnail"] = $v->getThumbnail();
                $video = array_merge($video, $v->getMetadata());
            }
            $res['status'] = "SUCCESS";
            $res["playlist"] = $playlistMeta;
            echo json_encode($res);
            die();
        }catch(PDOException $e){
            // No metadata could be found
            $error = array("STATUS" =>"FAILURE", "message"=>"Could not get playlists");
            echo json_encode($error);
            die();
        }
    }
}


/**
 * Get all playlist
 */
if($_SERVER["REQUEST_METHOD"] == "GET"){
    $playlists = array();

    if($isLoggedIn) {
        //If the user is logged in we can also get the subscribed playlists
        //If not we can just send back the others

        try {
            $conn = DB::getVideoDBConnection();
            $playlistsSQL = " 
                              SELECT pm.`uuid` as id, pm.`title`, pm.`description`, pm.`managed_by`, pm.`course_link`, IFNULL(ps.`subscribed`, false) as subscribed, IFNULL(ps.`last_visited`, '12-12-12 00:00:00') as last_visited
                              FROM PlaylistMeta pm
                              LEFT JOIN PlaylistSubscription as ps ON pm.uuid = ps.playlist_ref AND ps.user_ref = :userid
                             ";

            $stmt = $conn->prepare($playlistsSQL);
            $stmt->bindParam(":userid", $_SESSION["sessionid"]);
            $stmt->execute();
            $playlists = array_merge($playlists, $stmt->fetchAll(PDO::FETCH_ASSOC));
            foreach ($playlists as &$playlist){
                $playlist["subscribed"] = ($playlist["subscribed"] ? true:false);
            }
        } catch (Exception $e) {
            // No metadata could be found
            $error = array("STATUS" => "FAILURE", "message" => "Could not get playlists");
            echo json_encode($error);
            die();
        }

    }else{
        // The user does not need to be logged in to get these playlists
        try{
            $conn = DB::getVideoDBConnection();
            $playlistsSQL = " 
                        SELECT pm.`uuid` as id, pm.`title`, pm.`description`, pm.`managed_by`, pm.`course_link`, FALSE as subscribed, null as last_visited 
                        FROM PlaylistMeta as pm";
            $stmt = $conn->prepare($playlistsSQL);
            $stmt->execute();
            $playlists = array_merge($playlists, $stmt->fetchAll(PDO::FETCH_ASSOC));

        }catch(Exception $e) {
            $error = array("status" => "FAILURE", "message" => "Could not get playlists");
            echo json_encode($error);
            die();
        }
    }

    $res = array("status"=>"SUCCESS","playlists"=>$playlists);
    echo json_encode($res);
    die();


}
