<?php
/**
 * Created by PhpStorm.
 * User: Joakim
 * Date: 05/02/2019
 * Time: 7:30 PM
 */
/**
 * Registrer en bruker i systemet
 */

require_once  realpath(dirname(__FILE__)) . "/../model/User.php";
require_once  realpath(dirname(__FILE__)) . "/../model/Resource.php";
require_once  realpath(dirname(__FILE__)) . "/../model/DB.php";

$http_origin = $_SERVER['HTTP_ORIGIN'];
header("Access-Control-Allow-Origin: $http_origin");
header("Access-Control-Allow-Methods: POST, OPTIONS");
header("Access-Control-Allow-Headers: Origin");
header("Access-Control-Allow-Credentials: true");
header("Content-Type: application/json; charset=utf-8");
session_start();

$user = new User(DB::getAccountsDBConnection());
$util = new Resource();

//Check if the user is already logged in (existing session OR valid cookies)
//Get the login-status of the user
require_once realpath(dirname(__FILE__)) .  "/../model/checkAuthentication.php";

if ($isLoggedIn) {
    $userdata = $user->getUser();
    $res = array(
        "status"=>"SUCCESS",
        "uuid"=>$_SESSION["sessionid"],
        "email"=>$userdata["email"],
        "firstname"=>$userdata["firstName"],
        "lastName"=>$userdata["lastName"],
        "isTeacher"=>$isTeacher,
        "isStudent"=>$isStudent,
        "isAdmin"=> $isAdmin,
        "message"=>"Du er allerede logget inn"
    );
    echo json_encode($res);
    die();
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    //Get the credentials
    $credentials = array(
        'email' => $_POST['email'],
        'fname' => $_POST['firstname'],
        'lname' => $_POST['lastname'],
        'password' => $_POST['password'],
        'confirmPassword' => $_POST['confirmedPassword'],
        'isTeacher' => isset($_POST['teacherCheckbox']) ? 1 : 0,
    );

    /**
     * Make sure that all the fields in the form have content
     * need strlen, because empty() treat '0' as false
     */
    foreach($credentials as $field) {
        if (empty($field) && !strlen($field)){
            $res = array(
                "status"=>"FAILURE",
                "message" => "Alle felt er påkrevd");
            echo json_encode($res);
            die();
        }
    }

    // Verify email
    if (!$user->verifyUniqueEmail($credentials['email'])) {

        $res = array(
            "status"=>"FAILURE",
            "message" => "Ugyldig epost");
        echo json_encode($res);
        die();
    }

    // Verify that the two passwords match
    if (!$user->verifyPasswords($credentials['password'], $credentials['confirmPassword'])) {
        $res = array(
            "status"=>"FAILURE",
            "message" => "Passordene er ikke like");
        echo json_encode($res);
        die();
    }

    // Insert to database
    // Get the last inserted id for the user
    $id = $user->insertUser($credentials['email'], $credentials['password'], $credentials['isTeacher'], $credentials['fname'], $credentials['lname']);

    //The user can not be inserted
    if ($id == 0) {
        $res = array(
            "status"=>"FAILURE",
            "message" => "Something wrong happened, this is on us. Please be patient and try again");
        echo json_encode($res);
        die();
    }

    // Login user --> set session variabels (TODO: make a function which does this all over the place)
    $user->setSessionId($id);

    $userdata = $user->getUser();
    $res = array(
        "status"=>"SUCCESS",
        "uuid"=>$_SESSION["sessionid"],
        "email"=>$userdata["email"],
        "firstname"=>$userdata["firstName"],
        "lastName"=>$userdata["lastName"],
        "isTeacher"=>$isTeacher,
        "isStudent"=>$isStudent,
        "isAdmin"=> $isAdmin,
        "message"=>"Bruker ble registrert og du er logget inn!"
    );
    echo json_encode($res);
    die();

} else {
    echo $twig->render("register.html");
}