<?php
/**
 * Created by PhpStorm.
 * User: Magnus
 * Date: 07.02.19
 * Time: 17:45
 */

/**
 * Class Administrator
 */                     //TODO: kanskje fjerne extends?


class Administrator extends User
{

    public $db;

    public function __construct(PDO $db)
    {
        parent::__construct($db);
        $this->db = $db;
    }

    /**
     * @param $useremail            string the email address of the user
     *
     */
    public function setUserAsTeacher($useremail)
    {
        //search for users with the registered name
        if ($userobject = $this->getUserByUsername($useremail)) {
            //verify that the user is a student
            if ($userobject[0]['request_isTeacher']) {
                //set the users to teacher role and remove the teacher request
                $this->changeUserRole($userobject[0]['userId'], "student", "teacher");
            }
        }
    }

    /**
     * Changes the role of the user from $currentrole to $newrole
     *
     * @param $userId           int the user id
     * @param $currentrole      string the current role of the user (admin, teacher, student)
     * @param $newrole          string the new role of the user (admin, teacher, student)
     */
    public function changeUserRole($userId, $currentrole, $newrole){
        //fjerne den nåværende rollen
        //sette den den nye rollen til brukeren

        $roles = array($currentrole, $newrole);
        $i = 0;

        //loop through the roles and set the currentuser role to 0 and the new one to 1
        foreach ($roles as $role) {
            switch ($role) {
                case "student":
                    $sql = "UPDATE `accounts`.`User` t SET t.`request_isTeacher` = ? WHERE t.`userId` = ?";
                    break;
                case "admin":
                    $sql = "UPDATE `accounts`.`User` t SET t.`isAdmin` = ? WHERE t.`userId` = ?";
                    break;
                case "teacher":
                    $sql = "UPDATE `accounts`.`User` t SET t.`isTeacher` = ? WHERE t.`userId` = ?";
                    break;
            }
            $stmnt = $this->db->prepare($sql);
            $stmnt->execute(array($i, $userId));
            $i++;
        }
    }

    /**
     * Get a list of all the usernames that wants to be a teacher
     * @return array a list of all the usernames or blank
     */
    public function getUsersThatWantsToBeTeacher()
    {
        $sql = "SELECT firstName, lastName, email FROM accounts.User WHERE request_isTeacher = 1";
        $stmnt = $this->db->prepare($sql);
        $stmnt->execute();
        return $stmnt->fetchAll(PDO::FETCH_ASSOC);
    }

    /*
     * @param $searchtext string either the first or lastname OR first and lastname
     */
    public function getUserByFirstAndOrLastname($searchtext)
    {
        //the search item has spaces, but not at beginning or end
        if ($searchtext == trim($searchtext) && strpos($searchtext, ' ') !== false) {
            //get the first and lastname in the search string
            list($firstname, $lastname) = explode(" ", $searchtext);
            //search for both
            $sql = "SELECT userId, firstName, lastName FROM accounts.User WHERE (firstName like ?) AND (lastName like  ?)";
            $stmnt = $this->db->prepare($sql);
            $stmnt->execute(array($firstname, $lastname));
        } else {
            //search text for either last or firstname
            $sql = "SELECT userId, firstName, lastName FROM accounts.User WHERE (firstName like ?) OR (lastName like  ? )";
            $stmnt = $this->db->prepare($sql);
            $stmnt->execute(array($searchtext, $searchtext));
        }

        return $stmnt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Removes all the prermissions of the user
     * @param $trim string the email of the user
     */
    public function setUserAsStudent($email){
        $userobject = $this->getUserByUsername($email);
        $sql = "UPDATE `accounts`.`User` t SET t.`request_isTeacher` = 0, t.`isTeacher` = 0, t.`isAdmin` = 0 WHERE t.`userId` = ?";
        $stmnt = $this->db->prepare($sql);
        $stmnt->execute(array($userobject[0]["userId"]));
    }
}