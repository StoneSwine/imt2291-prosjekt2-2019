<?php
/**
 * Created by PhpStorm.
 * User: joakimellestad
 * Date: 2019-04-07
 * Time: 16:38
 */
require_once realpath(dirname(__FILE__)) . "/DB.php";
require_once realpath(dirname(__FILE__)) . "/Resource.php";
require_once realpath(dirname(__FILE__)) . "/../config/video_config.php";

/*
 * Video
 * Contains all the necessary logic to do work on a video
 * 1. Video::Create new video
 * 2. Video::Get existing video
 * Notice tha you should not use the constructor, use the two provided static functions which
 * does either 1 or 2 ^.
 *
 *
 * TODO: Future work
 * This class does not hold information about the video in memory. Instead it tries to fetch it all from the database, when it is needed.
 * Future work will involve making a cache to reduce database lookup. Look into using redis. Maybe frequently accessed
 * video could be cached
 *
 * */
class Video {

    // The id for the video object.
    // The id is the uuid of the video in the database
    //  64-bit unsigned
    private $videoID;

    /**
     * getVideoId
     * Get the id of the video object.
     * Will always be avaiable, a instance of this class
     * should not exist without a videoid.
     * @return short_UUID  64-bit unsigned
     */
    public function getVideoID()
    {
        return $this->videoID;
    }

    /**
     * setVideoID
     * Only used by itself, when creating an instance.
     * Should not be needed by anyone else
     * @param $id 64-bit unsigned
     */
    public function setVideoID($id){
        $this->videoID = $id;
    }


    /**
     * Video constructor.
     * Empty - Does nothing
     */
    public function __construct()
    {

    }

    /**
     * withVideoId
     * Returns a video object aware of a video id
     * @param $videoId 64-bit unsigned
     */
    public static function withVideoId($videoId){
        $instance = new self();
        $instance->setVideoID($videoId);
        return $instance;
    }

    public static function Video($ownerid){
        $instance = new Self();
        $videoUUID = NULL;

        try {
            $conn = DB::getVideoDBConnection();
            $sql = '
                INSERT INTO VideoMetadata
                (`uuid`, `owned_by`) VALUES
                (LAST_INSERT_ID(uuid_short()), :ownedby)';
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(":ownedby", $ownerid);
            $res = $stmt->execute();
        }catch(PDOException $e){
            $error = new Exception("Could not create new video id " . $e->getMessage(), 0, $e);
            throw $error;
        }
        if(!$res){
            //Failed - abort mission
            $error = new Exception("Database execution failed",0, NULL);
            throw $error;
        }
        $videoUUID = $conn->lastInsertId();
        //print_r($videoUUID);
        $instance->setVideoID($videoUUID);
        return $instance;
    }

    /**
     * newVideo
     *
     * Moves the file to permanent storage
     * Creates a metadata record for it
     *
     *
     * @param $fileDP - e.g. $_FILES['filename']
     * @param $ownedBy - the user that uploads the file. It's ID has gived by the database
     * @param $course - the course for which the video should belong
     * @return Vidoe instance on success, false if an error occured.
     * @throws Exception - failure from database execute or moving file on the filesystem
//     */
//    public static function newVideo($fileDP, $title, $description, $ownedBy, $course="")
//    {
//        $instance = new Self();
//        $videoUUID = NULL;
//
//        $ext = strtolower(pathinfo($fileDP["name"],PATHINFO_EXTENSION));
//        $mime = $fileDP['type'];
//
//        try {
//
//            // Create metadata record - No need to store the video if metadata can't be created
//            // Let's go
//            $conn = DB::getVideoDBConnection();
//            if($course != ""){
//                /* Link with course*/
//                $sql = '
//                INSERT INTO VideoMetadata
//                (`uuid`, `title`, `description`, `owned_by`, `course_link`, `extension`, `mime` ) VALUES
//                (LAST_INSERT_ID(uuid_short()), :title, :description, :ownedby, :course, :ext, :mime)';
//                $stmt = $conn->prepare($sql);
//                $stmt->bindParam(':course', $course);
//
//            }else{
//                $sql = '
//                INSERT INTO VideoMetadata
//                (`uuid`,`title`, `description`, `owned_by`, `extension`, `mime`) VALUES
//                (LAST_INSERT_ID(uuid_short()), :title, :description, :ownedby, :ext, :mime)';
//                $stmt = $conn->prepare($sql);
//            }
//
//            $stmt->bindParam(':title', $title);
//            $stmt->bindParam(':description', $description);
//            $stmt->bindParam(':ownedby', $ownedBy);
//            $stmt->bindParam(':ext', $ext);
//            $stmt->bindParam(':mime', $mime);
//            $res = $stmt->execute();
//            if(!$res){
//                //Failed - abort mission
//                $error = new Exception("Database execution failed",0, NULL);
//                throw $error;
//            }
//            //get uuid of metadata
//            $videoUUID = $conn->lastInsertId();
//            //print_r($videoUUID);
//            $instance->setVideoID($videoUUID);
//
//        }catch(PDOException $e){
//            $error = new Exception("Metadata creation failed: " . $e->getMessage(), 0, $e);
//            throw $error;
//        }
//
//        // Move file to permanent location
//        // Text evaluates to true, I think
//        if($videoUUID){
//            $ext = strtolower(pathinfo($fileDP["name"],PATHINFO_EXTENSION));
//
//            /* mp4 and mkv is supported by HTML5 streaming - we could use Handbrake or ffmpeg to convert everything. Another project maybe */
//            if(($ext !== 'mp4') && ($ext !== 'mkv')){
//                // Wrong format
//                $error = new Exception("Wrong format, got: " . $ext . ", expected: mp4, mkv",0, NULL);
//                throw $error;
//            }
//            $from = $fileDP["tmp_name"];
//            $destination = VIDEO_S3_ROOT . $videoUUID . "." . $ext;
//
//            $mv = @move_uploaded_file($from, $destination);
//
//            if($mv == false){
//                $error = new Exception("Moving file from " . $from . " to " . $destination . " failed",0, NULL);
//                throw $error;
//            }
//        }
//        try {
//            $instance->createThumbnail();
//        }catch (Exception $e){
//            //Not much we can do
//            //We don't want to throw this. It's better to return the instance than fail to generate thumbnail
//        }
//        return $instance;
//    }

    /**
     * deleteMe
     * Delete this video from the filesystem
     * Deletes it's metadata from the database
     * Delete it's references from playlists and updates the position for the other videos
     */
    public function deleteMe(){

        //Delete file from disc
        $deleteFile = VIDEO_S3_ROOT . $this->videoID . "." . $this->getExtension();
        if(!unlink($deleteFile)){
            throw new Exception("Could not delete file from filesystem.");
        }

        try {
            Playlist::removeFromPlaylists($this->videoID);
        }catch(PDOException $e){
            throw $e;
        }
        try{
            $conn = DB::getVideoDBConnection();
            $delete = "DELETE FROM VideoMetadata WHERE `uuid`=:id";
            $stmt = $conn->prepare($delete);
            $stmt->bindParam(":id", $this->videoID);
            $deleted = $stmt->execute();
            if(!$deleted){
                throw Exception("Could not delete the video.");
            }


        }catch(PDOException $e){
            throw $e;
        }

    }

    /**
     * getMime()
     * Gets the mimetype of the video
     * video/mp4
     * @return string - the mime type of the video
     */
    public function getMime(){
        $conn = DB::getVideoDBConnection();
        $sql = "SELECT `mime` FROM VideoMetadata WHERE `uuid` = :id";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(":id", $this->videoID);
        $stmt->execute();
        $mime = $stmt->fetchAll(PDO::FETCH_COLUMN);
        return $mime[0];
    }

    /**
     * getExtension
     * Gets the extension for the video.
     * Video usually have mp4, but could have mkv.
     * Is used to build the filepath for the video
     * @return string - the extension e.g. mp4 for the video
     */
    public function getExtension(){
        $conn = DB::getVideoDBConnection();
        $sql = "SELECT `extension` FROM VideoMetadata WHERE `uuid` = :id";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(":id", $this->videoID);
        $stmt->execute();
        $mime = $stmt->fetchAll(PDO::FETCH_COLUMN);
        return $mime[0];
    }

    /**
     * Takes video that is on disk and creates a thumbnail of it
     * Uses ffmpeg binary that is installed on the system
     * exec() is used to execute the command
     * See https://stackoverflow.com/questions/27145238/create-thumbnail-from-video-using-ffmpeg
     *
     * Takes a frame from the 5 second into the video.
     * TODO: If the length of the video is shorter than 5 sec, then this fails. The timestamp should be derived from the length of the video
     *
     * @throws Exception - if database query fails
     */
    public function createThumbnail()
    {
        $ext = $this->getExtension();

        $file = VIDEO_S3_ROOT . $this->videoID . "." . $ext;
        $outTemp = VIDEO_S3_ROOT . $this->videoID . "_thumbnail" . ".jpg";

        if (!file_exists($file)) {
            $error = new Exception("File not found: " . $file, 0, NULL);
            throw $error;
        }
        $base64 = "";

        // Lets hope the video is longer than 5 seconds
        exec("ffmpeg -i " . $file . " -ss 00:00:05.000 -s 286x180 " . $outTemp);
        if(file_exists($outTemp)){
            $data = @file_get_contents($outTemp);
            if($data === false){
                //file_get_contents failed. Probably because of windows. Linux seems to work fine.
                return;
            }
            $base64 = base64_encode($data);
        }

        /*
        try {
            $ffmpeg = FFMpeg\FFMpeg::create();
            $video = $ffmpeg->open($file);
            $frame = $video->frame(FFMpeg\Coordinate\TimeCode::fromSeconds(1)); // Are all videos more than 10 sec? :cross-fingers:
            $frame->save($tmpThumbnail, false, false); // This might work
            if (file_exists($tmpThumbnail)) {
                $data = readfile($tmpThumbnail);
                $base64 = base64_encode($data);
            }
        } catch (RuntimeException $e) {
            echo "Creating thumbnail failed: " . $e->getMessage();
        }*/
        if ($base64 !== "") {

            try {
                $conn = DB::getVideoDBConnection();
                $sql = "UPDATE VideoMetadata SET `thumbnail` = :thumbnail WHERE `uuid`=:id";
                $stmt = $conn->prepare($sql);
                $stmt->bindParam(":thumbnail", $base64);
                $stmt->bindParam(":id", $this->videoID);
                $res = $stmt->execute();

            } catch (PDOException $sqlerr) {
                throw new Exeption("SQL and thumbnail failed: " . $sqlerr->getMessage());
            }
        }
    }

    /**
     * getThumbnail
     * Get's the thumbnail that is stored in the database.
     *
     * @throws Exception - if the database query fails.
     */
    public function getThumbnail(){
        try {
            $conn = DB::getVideoDBConnection();
            $sql = "
            SELECT `thumbnail` FROM VideoMetadata 
            WHERE `uuid` = :id
        ";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':id', $this->videoID);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC)[0]["thumbnail"];
        }catch(PDOException $e){
            throw $e;
        }
    }

    public function setVideo($fileDP){
        $ext = strtolower(pathinfo($fileDP["name"],PATHINFO_EXTENSION));
        $mime = $fileDP['type'];
        try{
            $conn = DB::getVideoDBConnection();
            $sql = 'UPDATE `VideoMetadata` SET `extension`=:ext, `mime`=:mime WHERE uuid=:videoid';
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':ext', $ext);
            $stmt->bindParam(':mime', $mime);
            $stmt->bindParam(':videoid', $this->videoID);
            $res = $stmt->execute();
            if(!$res){
                $error = new Exception("Database execution failed",0, NULL);
                throw $error;
            }
            /* mp4 and mkv is supported by HTML5 streaming - we could use Handbrake or ffmpeg to convert everything. Another project maybe */
            if(($ext !== 'mp4') && ($ext !== 'mkv')){
                // Wrong format
                $error = new Exception("Wrong format, got: " . $ext . ", expected: mp4, mkv",0, NULL);
                throw $error;
            }
            $from = $fileDP["tmp_name"];
            $destination = VIDEO_S3_ROOT . $this->videoID . "." . $ext;

            $mv = @move_uploaded_file($from, $destination);

            if($mv == false){
                $error = new Exception("Moving file from " . $from . " to " . $destination . " failed",0, NULL);
                throw $error;
            }
        }catch(PDOException $e){
            throw new Exception("Could not set video");
        }
    }

    /**
     * setThumbnail
     * Expect a base64 string ready to be stored in the databnase
     * No checks is made to check for validity
     * TODO: Check for valid base64
     * TODO: Check if execute is successfull
     * @param $thumbnail
     * @throws PDOException - If the database query fails
     */
    public function setThumbnail($thumbnail){

        try {
            $conn = DB::getVideoDBConnection();
            $sql = "
            UPDATE VideoMetadata 
            SET `thumbnail`=:thumbnail
            WHERE `uuid` = :id
        ";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':thumbnail', $thumbnail);
            $stmt->bindParam(':id', $this->videoID);
            $stmt->execute();
        }catch(PDOException $e){
            throw $e;
        }
    }

    /**
     * getMetadata
     * Get's the metadata for a video file
     * @return array - returns an array with: array("id"=>,"")
     * @throws Exception - If the database query fails
     */
    public function getMetadata(){

        try{

            $conn = DB::getVideoDBConnection();
            $sql = "
                SELECT `title`, `description`, `owned_by`, `course_link` 
                FROM VideoMetadata 
                WHERE `uuid` = :id
            ";
            $stmt = $conn->prepare($sql);

            $stmt->bindParam(':id', $this->videoID);

            $stmt->execute();
            if(!$stmt){
                // No metadata found with that id
                throw new Exception("No video with that id");
            }
            $videoMeta = $stmt->fetchAll();
            // TODO: Fetch only column name

            //Resource::debug($videoMeta);
            return array(
                "title" => $videoMeta[0]["title"],
                "description"=>$videoMeta[0]["description"],
                "owner"=>$videoMeta[0]["owned_by"],
                "course_link"=>$videoMeta[0]["course_link"],
                "id"=>$this->videoID
            );
        }catch(PDOException $e){
            $error = new Exception("Fetcing metadata failed: " . $e->getMessage(),0,$e);
            throw $error;
        }
    }



    /*
     * Individual functions for updatng columns in database for the video
     * */

    /**
     * Update the title for the video
     * @param $title
     * @return boolean - true if sql executed successfully
     */
    public function setTitle($title){
        try{
            $conn = DB::getVideoDBConnection();
            $sql = "
                UPDATE VideoMetadata SET `title`=:new
                WHERE `uuid` = :id
            ";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':id', $this->videoID);
            $stmt->bindParam(':new', $title);
            return $stmt->execute();
        }catch (PDOException $e){
            throw $e;
        }
    }

    /**
     * Update the description for the vide
     * @param $description
     * @return mixed
     */
    public function setDescription($description){
        try{
            $conn = DB::getVideoDBConnection();
            $sql = "
                UPDATE VideoMetadata SET `description`=:new
                WHERE `uuid` = :id
            ";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':id', $this->videoID);
            $stmt->bindParam(':new', $description);
            return $stmt->execute();
        }catch (PDOException $e){
            throw $e;
        }
    }

    /**
     * Update the owner of the video
     * @param $ownerId
     * @return mixed
     */
    public function setOwner($ownerId){
        try{
            $conn = DB::getVideoDBConnection();
            $sql = "
                UPDATE VideoMetadata SET `owned_by`=:new
                WHERE `uuid` = :id
            ";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':id', $this->videoID);
            $stmt->bindParam(':new', $ownerId);
            return $stmt->execute();
        }catch (PDOException $e){
            throw $e;
        }
    }

    /**
     * Update the linked course for the video
     * @param $courseName
     * @return mixed
     */
    public function setCourse($courseName){
        try{
            $conn = DB::getVideoDBConnection();
            $sql = "
                UPDATE VideoMetadata SET `course_link`=:new
                WHERE `uuid` = :id
            ";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':id', $this->videoID);
            $stmt->bindParam(':new', $courseName);
            return $stmt->execute();
        }catch (PDOException $e){
            throw $e;
        }
    }

    /**
     * Set the subtitle for the video
     */
    public function setSubtitle($fileDP, $lang = "en"){


        $from = $fileDP["tmp_name"];
        $destination = VIDEO_S3_ROOT . $this->videoID . "-subtitle-" . $lang . ".vtt";

        $mv = @move_uploaded_file($from, $destination);

        if($mv == false){
            $error = new Exception("Moving file from " . $from . " to " . $destination . " failed",0, NULL);
            throw $error;
        }
    }

    /**
     * getRanking
     * Get's the ranking for the video
     * If user is provided the ranking details left by that user is returned
     * @param string $user
     */
    public function getRanking($user = ""){

        $conn = DB::getVideoDBConnection();
        try{

            $restrict = "SELECT `uuid`, `rankValue`  
                             FROM VideoRank 
                             WHERE 
                             `video_ref` = :videoid AND
                             `made_by` = :madeby";
            $stmt = $conn->prepare($restrict);
            $stmt->bindParam(':videoid', $this->videoID);
            $stmt->bindParam(':madeby', $user);
            $stmt->execute();
            if($stmt->rowCount > 0){
                $result = $stmt->fetchAll(PDO::FETCH_COLUMN);
                return $result[0];
            }
        }catch (PDOException $e){
            throw $e;
        }
    }

    /**
     * getRank
     *
     * Calculates the rank and returns it
     * Uses sql statement with ROUND(AVG(rank)) with 1 in decimal precision
     *
     */
    public function getRank(){
        $conn = DB::getVideoDBConnection();
        try{

            $ranksql = "SELECT ROUND(AVG(`rankValue`),1)  as average, COUNT(*) AS submissions 
                             FROM VideoRank 
                             WHERE 
                             `video_ref` = :videoid";
            $stmt = $conn->prepare($ranksql);

            $stmt->bindParam(':videoid', $this->videoID);

            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $result[0]["best"] = "5"; // Define the top score
            return $result[0];
        }catch (PDOException $e){
            throw $e;
        }
        throw new Exception("No avarage could be calculated on video: " . $this->videoID,0,null);

    }


    /**
     * Add or updates a ranking
     * !!!! There are UNIQUE key constraints on video_ref+made_by
     * @param string $VideoRankingUUID -
     * @param $made_by
     * @param int $rankValue default to 0
     */
    public function addRanking($made_by, $rankValue = 0){
        try {
            $conn = DB::getVideoDBConnection();

            $sqlRank = "INSERT INTO VideoRank
                                (`rankValue`, `video_ref`, `made_by`)
                                VALUES
                                (:rank, :videoref, :madeby)
                                ON DUPLICATE KEY UPDATE
                                `rankValue`=:newRank
                                ";

            $newRank = $conn->prepare($sqlRank);
            $newRank->bindParam(':rank', $rankValue);
            $newRank->bindParam(':videoref', $this->videoID);
            $newRank->bindParam(':madeby', $made_by);
            $newRank->bindParam(':newRank', $rankValue);
            $newRank->execute();

        } catch (PDOException $e) {
            throw $e;
        }
    }


    /**
     * deleteComment
     *
     * Proper authorization checks must be done prior to running this
     * This function deletes the comment with the id passed in as parameter
     * NO AUTHORIZATION IS PERFORMED BY THIS FUNCTION
     *
     * @param $commentid
     * @return Boolean success status
     */
    public function deleteComment($commentid){
        try {
            $conn = DB::getVideoDBConnection();

            $addComment = "DELETE FROM VideoComment WHERE `uuid` = :id";

            $stmt = $conn->prepare($addComment);
            $stmt->bindParam(':id', $commentid);
            return $stmt->execute();
        }catch(PDOException $e){
            throw $e;
        }
    }

    /**
     * deleteCommentWithAuthorizationCheck
     * Delete a comment that the user owns. Admins must use the deleteComment function.
     * TODO: Make single way of deleting a video. Involves checking account DB for admin credentials
     * @param $commentid
     * @param $userid
     * @return boolean - true if successfully deleted
     */
    public function deleteCommentWithAuthorizationCheck($commentid, $userid){
        try {
            $conn = DB::getVideoDBConnection();

            $deleteComment = "DELETE FROM VideoComment WHERE `uuid` = :id and `made_by` = :userid";

            $stmt = $conn->prepare($deleteComment);
            $stmt->bindParam(':id', $commentid);
            $stmt->bindParam(':userid', $userid);
            $deleted = $stmt->execute();
            return $deleted;
        }catch(PDOException $e){
            throw $e;
        }
    }


    /**
     * addComment
     *
     * Adds a new comment to the video
     *
     * @param $comment
     * @param $made_by
     */
    public function addComment($comment, $made_by){

        try {
            $conn = DB::getVideoDBConnection();

            $addComment = "INSERT INTO VideoComment
                          (`comment`, `video_ref`, `dTime`, `made_by`) VALUES
                          (:comment, :videoref, CURRENT_TIMESTAMP(), :madeby)
                           ";

            $stmt = $conn->prepare($addComment);
            $stmt->bindParam(':comment', $comment);
            $stmt->bindParam(':videoref', $this->videoID);
            $stmt->bindParam(':madeby', $made_by);
            return $stmt->execute();

        }catch(PDOException $e){
            throw $e;
        }
    }

    /**
     * getComments
     *
     * Future work: Save the cursor in the session. Makes it possible to retrieve batches of comments instead
     * of all at once, I think. Must have some ajax on client side though. :thinking:
     *
     * returns the comments as an array of array(comment)
     * [["comment"=>"dfafew", "madeby"="user1"], ["comment"=>"dfafew", "madeby"="user1"]]
     *
     */
    public function getComments(){
        try {
            $conn = DB::getVideoDBConnection();

            $addComment = "SELECT `uuid` AS id,`comment`, `dTime`, `made_by` AS madeby 
                           FROM VideoComment 
                           WHERE `video_ref`=:videoid 
                           ORDER BY `dTime` DESC";

            $stmt = $conn->prepare($addComment);
            $stmt->bindParam(':videoid', $this->videoID);

            $stmt->execute();

            $comments = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if($stmt->rowCount() > 0){
                $users = array(); //string array with id
                foreach($comments as $comment){
                    // get all the users to make a batch wuery agains accounts database
                    array_push($users, $comment['madeby']);
                }

                // Crates ?, ?, ? for getting users - should not be harmful
                $in = str_repeat('?,', count($users) - 1) . '?';

                $accountsConn = DB::getAccountsDBConnection();
                $sqlInfo = "SELECT `userId`, `firstName`, `lastName`, `isTeacher` FROM User WHERE `userId` IN ($in)";
                $prep = $accountsConn->prepare($sqlInfo);


                $prep->execute($users);
                $usersInfo = $prep->fetchAll(PDO::FETCH_ASSOC);
                $result = array_column($usersInfo, null, "userId");


                foreach($comments as &$comment){

                    $comment["firstname"] = $result[$comment["madeby"]]["firstName"];
                    $comment["lastname"] = $result[$comment["madeby"]]["lastName"];
                    $comment["isTeacher"] = $result[$comment["madeby"]]["isTeacher"];
                    $dt = new DateTime($comment["dTime"]);
                    $dtf = date_format($dt,'Y-m-d H:i');
                    $comment["dTime"] = $dtf;
                }
                return $comments;
            }

        }catch(PDOException $e){
            throw $e;
        }
    }


    /**
     * addTag
     * Adds a tag to a video.
     * @param $tag
     */
    public function addTag($tag){

        try {
            $conn = DB::getVideoDBConnection();

            $addTag = "INSERT INTO TagOnVideo (`tag`, `video_ref`) VALUES(:tag, :videoid)";

            $stmt = $conn->prepare($addTag);
            $stmt->bindParam(':tag', $tag);
            $stmt->bindParam(':videoid', $this->videoID);

            $stmt->execute();

        }catch(PDOException $e){
            throw $e;
        }
    }
    public function removeAllTags(){
        try {
            $conn = DB::getVideoDBConnection();

            $removeTagsSQL = "DELETE FROM TagOnVideo WHERE `video_ref`=:videoid";
            $stmt = $conn->prepare($removeTagsSQL);
            $stmt->bindParam(':videoid', $this->videoID);
            $stmt->execute();

        }catch(PDOException $e){
            throw $e;
        }
    }

    public function getTags(){
        try {
            $conn = DB::getVideoDBConnection();

            $addTag = "SELECT tag FROM TagOnVideo WHERE `video_ref` = :videoid";

            $stmt = $conn->prepare($addTag);
            $stmt->bindParam(':videoid', $this->videoID);

            $stmt->execute();
            $tags = array();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach($result as $tag){
                array_push($tags, $tag['tag']);
            }
            return $tags;
        }catch(PDOException $e){
            throw $e;
        }
    }

    /**
     * removeTag
     * Remove a tag from the video
     * @param $tag
     */
    public function removeTag($tag){
        try {
            $conn = DB::getVideoDBConnection();

            $addTag = "DELETE FROM TagOnVideo WHERE `video_ref`=:videoid and `tag_ref`=:tag";

            $stmt = $conn->prepare($addTag);
            $stmt->bindParam(':tag', $tag);
            $stmt->bindParam(':videoid', $this->videoID);

            $stmt->execute();

        }catch(PDOException $e){
            throw $e;
        }
    }

    /**
     * get the owner for the video
     * @return integer 6 bit
     */
    public function getOwner(){
        try {
            $conn = DB::getVideoDBConnection();

            $ownerSQL = "SELECT `owned_by` AS owner FROM VideoMetadata WHERE `uuid`=:videoid ";

            $stmt = $conn->prepare($ownerSQL);
            $stmt->bindParam(':videoid', $this->videoID);
            if($stmt->execute()){

                $userid = $stmt->fetch(PDO::FETCH_COLUMN);
                return $userid;
            }

        }catch(PDOException $e){
            throw $e;
        }
    }
}