<?php
/**
 * Created by PhpStorm.
 * User: joakimellestad
 * Date: 2019-04-06
 * Time: 18:02
 */

class DB
{

    private static $db = null;
    //private $dbh = null;

    /*private $connections = array(
        "accounts"=>NULL,
        "video"=> NULL
    );*/
    private $accountsConn = NULL;
    private $videoConn = NULL;

    private function __construct() {
        /*
         * require statement is inspired by:
         * https://stackoverflow.com/questions/266688/how-do-you-know-the-correct-path-to-use-in-a-php-require-once-statement
         */

        // Array with configuration values
        $dbConfig = require_once(realpath(dirname(__FILE__)) . "/../config/db.php");

        // Creating connection for accounts
        if(is_null($this->accountsConn)){
            // No connection to accounts is made by yet. Let's make one
            $dsn = 'mysql:dbname=' . $dbConfig['dbname']['accounts'] . ';host=' . $dbConfig['host'];

            try {
                $this->accountsConn = new PDO($dsn, $dbConfig['username'], $dbConfig['password']);
            } catch (PDOException $e) {
                // NOTE IKKE BRUK DETTE I PRODUKSJON
                echo 'Connection failed: ' . $e->getMessage();
            }
        }

        // Create connection for video
        if(is_null($this->videoConn)){
            // No connection to video is made by yet. Let's make one
            $dsn = 'mysql:dbname=' . $dbConfig['dbname']['video'] . ';host=' . $dbConfig['host'];

            try {
                $this->videoConn  = new PDO($dsn, $dbConfig['username'], $dbConfig['password']);
            } catch (PDOException $e) {
                // NOTE IKKE BRUK DETTE I PRODUKSJON
                print('Connection failed: ' . $e->getMessage());
            }
        }
    }

    /**
     * Get a connection to the database accounts
     * If no connection is made it creates a new one and returns
     * @return mixed PDO object for the database connection
     */
    public static function getAccountsDBConnection() {
        if (DB::$db==null) {
            DB::$db = new self();
        }
        return DB::$db->accountsConn;
    }

    /**
     *
     * @return Database connection for video database / PDO object
     */
    public static function getVideoDBConnection(){
        if (DB::$db==null) {
            DB::$db = new self();
        }
        return DB::$db->videoConn;
    }
}