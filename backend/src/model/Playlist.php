<?php
/**
 * Created by PhpStorm.
 * User: joakimellestad
 * Date: 2019-04-07
 * Time: 18:15
 */

class Playlist {

    private $playlistId;

    /**
     * @return mixed
     */
    public function getPlaylistId()
    {
        return $this->playlistId;
    }

    /**
     * @param mixed $playlistId
     */
    public static function setPlaylistId($playlistId)
    {
        $instance = new self();
        $instance->playlistId = $playlistId;
        return $instance;
    }

    /**
     *
     * Playlist constructor.
     * I don't like constructors in php. Java is better
     */
    public function __construct()
    {

    }

    /**
     * newPlaylist
     *
     * Lager en ny spilleliste.
     * Videoer legges til i etterkant med addVideo()
     *
     * @param $title
     * @param $description
     * @param $managed_by
     * @param $course_link
     * @param $thumbnail
     * @return Playlist
     */
    public static function newPlaylist($title, $description, $managed_by, $course_link, $thumbnail){
        try {

            $conn = DB::getVideoDBConnection();
            if(is_null($course_link) && is_null($thumbnail)) {
                // If thumbnail or course is NOT provided
                $sql = "INSERT INTO PlaylistMeta
              (`uuid`, `title`, `description`, `managed_by`)
              VALUES (LAST_INSERT_ID(uuid_short()), :title, :description, :managedby)
              ";
                $stmt = $conn->prepare($sql);

            }else if(is_null($course_link) && !is_null($thumbnail)) {
                //If thumbnail is provided
                $sql = "INSERT INTO PlaylistMeta
              (`uuid`, `title`, `description`, `managed_by`, `thumbnail`)
              VALUES (LAST_INSERT_ID(uuid_short()), :title, :description, :managedby, :thumbnail)
              ";
                $stmt = $conn->prepare($sql);
                $stmt->bindParam(":thumbnail", $thumbnail);
            }else if(!is_null($course_link) && is_null($thumbnail)){
                //If course is provided
                $sql = "INSERT INTO PlaylistMeta
              (`uuid`, `title`, `description`, `managed_by`, `thumbnail`, `course`)
              VALUES (LAST_INSERT_ID(uuid_short()), :title, :description, :managedby, :course)
              ";
                $stmt = $conn->prepare($sql);
                $stmt->bindParam(":course", $course_link);
            }else{
                // if both are provided
                $sql = "INSERT INTO PlaylistMeta
              (`uuid`, `title`, `description`, `managed_by`, `course_link`, `thumbnail`)
              VALUES (LAST_INSERT_ID(uuid_short()), :title, :description, :managedby, :course, :thumbnail)
              ";
                $stmt = $conn->prepare($sql);
                $stmt->bindParam(':course', $course_link);
                $stmt->bindParam(":thumbnail", $thumbnail);
            }

            $stmt->bindParam(':title', $title);
            $stmt->bindParam(':description', $description);
            $stmt->bindParam(':managedby', $managed_by);
            $success = $stmt->execute();
            if($success){
                $instance = Playlist::setPlaylistId($conn->lastInsertId());
            }else{
                throw new Exception("Could not create new playlist");
            }

        }catch(PDOException $e){
            throw $e;
        }
        return $instance;
    }

    /**
     * Delete a playlist
     */
    public function deletePlaylist(){
        try{
            $conn = DB::getVideoDBConnection();
            $sql = "DELETE FROM PlaylistMeta WHERE `uuid` = :playlistid";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(":playlistid", $this->playlistId);
            if(!$stmt->execute()){
                throw Exception("Could not delete playlist");
            }
        }catch (PDOException $e){
            throw $e;
        }
    }

    /***
     *
     * update playlist title and description
     *
     * @param $id int
     * @param $title string
     * @param $desc string
     */
    public function updatePlaylist($id, $title, $desc){
        try{
            $conn = DB::getVideoDBConnection();
            $sql = "UPDATE PlaylistMeta SET title = :sqltitle, description = :sqldesc WHERE uuid = :sqluuid";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(":sqltitle", $title);
            $stmt->bindParam(":sqldesc", $desc);
            $stmt->bindParam(":sqluuid", $id);
            $stmt->execute();

            //The update was not sucessfull
            //OBS: returns false when the row was already exactly how you're updating it
            if (!$stmt->rowCount()){
               throw new PDOException();
            }

        }catch (PDOException $e){
            throw $e;
        }
    }

    /**
     * addVideo
     * Video is added at the end. Position can be changed later using updatePosition()
     *
     * @param $videoid
     */
    public function addVideo($videoid){
        try {
            $position = $this->getEndPosition();
            $position = $position+1; // New endposition
            $conn = DB::getVideoDBConnection();
            $sql = "INSERT INTO PlaylistVideo
              (`playlist_ref`, `video_ref`, `position`)
              VALUES (:playlist, :videoid, :pos)
              ";

            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':playlist', $this->playlistId);
            $stmt->bindParam(':videoid', $videoid);
            $stmt->bindParam(':pos', $position);

            $stmt->execute();
        }catch(PDOException $e){
            throw $e;
        }
    }

    /**
     * RemoveVideo
     * Sets the position of the video to the end
     * When removing the video we first
     * get the original position of the video
     * then the last+1 position
     * then we call the updatePosition that will move the "delete video" to the end and
     * fix the position for all the other videos.
     * lastly the video is deleted
     *
     * @param $videoid
     * @throws Exception
     */
    public function removeVideo($videoid){
        try{
            $conn = DB::getVideoDBConnection();
            $sql = "
                SELECT `position` FROM PlaylistVideo WHERE `video_ref` = :videoref AND `playlist_ref` = :playlistref INTO @OP;
                SELECT MAX(`position`)+1 FROM PlaylistVideo WHERE `playlist_ref` = :playlistref INTO @MAX;
                SET @PLAYLISTREF = :playlistref;
                CALL updatePosition(@OP, @PLAYLISTREF, @MAX);
                DELETE FROM PlaylistVideo WHERE `video_ref` = :videoref AND `playlist_ref` = :playlistref;
        ";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(":playlistref", $this->playlistId);
            $stmt->bindParam(":videoref", $videoid);
            $res = $stmt->execute();
            if(!$res){
                throw new Exception("Could not update position");
            }
        }catch (PDOException $e){
            throw $e;
        }

    }

    /**
     * Deletes the video from all playlists
     *
     * @param $videoid
     */
    public static function removeFromPlaylists($videoid){
        try{
            $conn = DB::getVideoDBConnection();
            $sql = "SELECT `playlist_ref` as playlist FROM PlaylistVideo WHERE `video_ref`=:videoid";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(":videoid",$videoid);
            if($stmt->execute() && $stmt->rowCount() > 0){
                foreach ($stmt->fetch(PDO::FETCH_ASSOC) as $playlist){
                    $p = Playlist::setPlaylistId($playlist);
                    $p->removeVideo($videoid);
                }
            }else{
                throw new Exception("Could not get playlists");
            }

        } catch (PDOException $e){
            throw $e;
        }

    }

    private function getEndPosition(){
        $conn = DB::getVideoDBConnection();
        $endPosition = "SELECT MAX(`position`) AS position FROM `PlaylistVideo` WHERE `playlist_ref` = :playlistref";
        $stmt = $conn->prepare($endPosition);
        $stmt->bindParam(":playlistref", $this->playlistId);
        $stmt->execute();
        $endPos = $stmt->fetch(PDO::FETCH_ASSOC);
        return $endPos["position"];
    }

    /**
     * getMetadata
     * Get the title, description, owner/managed_by and course link from the playlist
     * @return mixed
     */
    public function getMetadata(){
        try{
            $conn = DB::getVideoDBConnection();
            $sql = "SELECT `uuid` AS id, `title`, `description`, `managed_by`, `course_link` FROM PlaylistMeta WHERE `uuid`=:id";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(":id",$this->playlistId);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC)[0];
        }catch(PDOException $e){
            throw $e;
        }
    }
    /**
     * gives back the playlist metadata:
     * array(
     * "playlist"=>"title goes here",
     * "videos" => array(array("video_ref"=>"124ty81723t487","pos"=>"0"), array("video_ref"=>"23r4t", "pos"=>"1"))
     * )
     *
     */
    public function getPlaylist(){

        try{

            $conn = DB::getVideoDBConnection();
            $sql = "SELECT `video_ref` AS id, `position` 
                    FROM PlaylistVideo 
                    WHERE `playlist_ref` = :playlist 
                    ORDER BY `position`";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(":playlist",$this->playlistId);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }catch(PDOException $e){
            throw $e;
        }

    }

    /**
     * getThumbnail
     *
     * if thumbnail field for the playlist is empty a thumbnail from one of the videos is selected
     * @returns "thumbnail"=>"base64";
     */
    public function getThumbnail(){
        try{

            $conn = DB::getVideoDBConnection();
            $sql = "SELECT `thumbnail` FROM PlaylistMeta 
                    WHERE `uuid` = :playlist AND `thumbnail` IS NOT NULL";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(":playlist", $this->playlistId);
            $res = $stmt->execute();
            if($stmt->rowCount() == 1){
                $thumbnail = $stmt->fetch(PDO::FETCH_ASSOC);
                return $thumbnail;
            }else{
                /* Need thubmnail from one of the videos */
                $thumbnailFromVideo = "SELECT `thumbnail` 
                                        FROM VideoMetadata 
                                        WHERE `uuid` IN 
                                        (SELECT `video_ref` 
                                        FROM PlaylistVideo 
                                        WHERE `playlist_ref`= :playlistid) LIMIT 1";
                $stmt = $conn->prepare($thumbnailFromVideo);
                $stmt->bindParam(":playlistid", $this->playlistId);
                $stmt->execute();
                if($stmt->rowCount() == 1){
                    return $stmt->fetch(PDO::FETCH_ASSOC);
                }
            }
            /* if there are no images to be displayed, then this might work*/
            return array("thumbnail"=>"iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z/C/HgAGgwJ/lK3Q6wAAAABJRU5ErkJggg==");

        }catch(PDOException $e){
            throw $e;
        }
    }

    /*
     *
     * TODO: Check if playlist is public.
     * */
    public function addSubscriber($user){
        try {
            $conn = DB::getVideoDBConnection();
            $sql = "INSERT INTO PlaylistSubscription (`playlist_ref`, `user_ref`) VALUES (:playlist, :user)";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(":playlist", $this->playlistId);
            $stmt->bindParam(":user", $user);
            if ($stmt->execute()) {
                return true;
            } else {
                return false;
            }

        } catch (PDOException $e) {
            throw $e;
        }
    }


    public function removeSubscriber($user){
        try{
            $conn = DB::getVideoDBConnection();
            $sql = " DELETE FROM PlaylistSubscription WHERE (`playlist_ref` = :playlist AND `user_ref` = :user)";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(":playlist", $this->playlistId);
            $stmt->bindParam(":user", $user);
            if($stmt->execute()){
                return true;
            }else{
                return false;
            }
        }catch (PDOException $e){
            throw $e;
        }
    }

    /**
     *
     * SELECT CASE WHEN (count(*)=1) THEN 'TRUE' else 'FALSE' end FROM PlaylistSubscription WHERE (`playlist_ref` = '98075232509624327' AND `user_ref` = '2')
     * @param $user
     * @return bool true or false if user is subscribed to this playlist
     */

    public function isSubscribed($user){
        try{
            $conn = DB::getVideoDBConnection();
            //$sql = "SELECT CASE WHEN (count(*)=1) THEN \'TRUE\' ELSE \'FALSE\' END AS subscribed FROM PlaylistSubscription WHERE (`playlist_ref` = :playlist AND `user_ref` = :user)";
            $sql = "SELECT CASE WHEN (count(*)=1) THEN TRUE else FALSE end as subscribed FROM PlaylistSubscription WHERE (`playlist_ref` = :playlist AND `user_ref` = :user)";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(":playlist", $this->playlistId);
            $stmt->bindParam(":user", $user);
            $stmt->execute();
            $res = $stmt->fetch(PDO::FETCH_COLUMN);

            return $res;

        }catch (PDOException $e){
            throw $e;
        }
    }

    /**
     * setPositionOnVideo
     *
     * When a video is set to e.g. 4 from 8,
     * the video is put on position 4 and
     * the video that was on 4 is pushed down with all
     * the rest of the videos
     *
     * Den heftigste sql querien noensinne....Nå skal jeg legge meg
     * SET @NEWPOSITION=3;
     * SET @MVIDEO="98073586866061313";
     * set @PLAYLIST="98075232509624327";
     * @param $newPosition - The new position for the video
     * @param $video - the video to set the position for
     */
    public function setPositionOnVideo($newPosition, $video){

        try{
            $conn = DB::getVideoDBConnection();
            $sql = "
                SELECT `position` FROM PlaylistVideo WHERE `video_ref` = :videoref AND `playlist_ref` = :playlistref INTO @OP;
                SET @NEWPOSITION = :newposition;
                SET @PLAYLISTREF = :playlistref;
                CALL updatePosition(@OP, @PLAYLISTREF, @NEWPOSITION);
                UPDATE `PlaylistVideo` SET `position` = :newposition WHERE `playlist_ref` = :playlistref and `video_ref` = :videoref;
        ";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(":playlistref", $this->playlistId);
            $stmt->bindParam(":newposition", $newPosition);
            $stmt->bindParam(":videoref", $video);
            $res = $stmt->execute();
            if(!$res){
                throw new Exception("Could not update position");
            }
        }catch (PDOException $e){
            throw $e;
        }

    }
}