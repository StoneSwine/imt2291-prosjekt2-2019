<?php
/**
 * Created by PhpStorm.
 * User: joakimellestad
 * Date: 2019-04-06
 * Time: 18:02
 */

class User
{
    public $db;

    /**
     * User constructor.
     * @param $db PDO object to the database
     */
    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * select the member in the database for the current username
     * @param $username     string username/email to look up
     * @return bool|mixed   returns the row of the selected user || false
     */
    public function getUserByUsername($username)
    {
        $sql = "SELECT * FROM accounts.User WHERE email = ?";
        $sth = $this->db->prepare($sql);
        $sth->execute(array($username));

        if ($sth->rowCount() > 0) {
            return $sth->fetchAll(PDO::FETCH_ASSOC);
        }else{
            return false;
        }
    }   

    /**
     * Clear the tokens in the browser (clientside)
     */
    public function clearCookie()
    {
        if (isset($_COOKIE["remember_me"])) {
            setcookie("remember_me", "");
        }
    }

    /**
     * Inserts cookie into the database to make the "remember me" functionality work
     *
     * @param $userid           int identifier of the user
     * @param $selector         string selector part of the cookie
     * @param $hashedvalidator   string validator part of the coookie
     * @param $expirationdate   string expiration date of the cookie
     * @param $useragent        string users current user-agent string
     * @param $userip           string last IP of the user
     *
     * @return bool             if the insertion is successful or not
     */
    public function insertToken($userid, $selector, $hashedvalidator, $expirationdate, $useragent, $userip)
    {
        $sql = "INSERT INTO `accounts`.`auth_tokens` (`selector`, `hashedValidator`, `userId_ref`, `expires`, `last_login_agent`, `last_login_ip`) VALUES (?, ?, ?, ?, ?, ?)";
        $sth = $this->db->prepare($sql);

        //preapare the parameters
        $sth->bindParam(1, $selector);
        $sth->bindParam(2, $hashedvalidator);
        $sth->bindParam(3, $userid);
        $sth->bindParam(4, $expirationdate);
        $sth->bindParam(5, $useragent);
        $sth->bindParam(6, $userip);
        $sth->execute();

        if ($sth->rowCount() == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * Validates the cookie from the user with the user in the database according to best practice
     *
     * @param $clientselector   string the selector from the user
     * @param $clientvalidator  string the non-hashed validator from the user
     * @param $currentdate      string the current date
     * @return bool             true or false based on status of validation
     */
    public function validateCookie($clientselector, $clientvalidator, $currentdate)
    {

        $sql = "SELECT * FROM accounts.auth_tokens WHERE selector = ?";
        $sth = $this->db->prepare($sql);
        $sth->execute(array($clientselector));

        //Grab the row in auth_tokens for the given selector. If none is found, abort.
        if ($result = $sth->fetch(PDO::FETCH_ASSOC)) {

            //Hash the validator provided by the user's cookie with SHA-256
            $clienthashedvalidator = hash("sha256", $clientvalidator);

            //Compare the SHA-256 hash we generated with the hash stored in the database, using hash_equals().
            //hash_equals — Timing attack safe string comparison
            if (hash_equals($result["hashedValidator"], $clienthashedvalidator)) {

                //Make sure the cookie has not expired

                //Make sure that the database cookie has not expired
                if ($result["expires"] >= $currentdate) {
                    // DBTIME               the current time

                    //associate the current session with the appropriate user ID.
                    //set the session ID if all of the cookies are correct
                    $this->setSessionId($result["userId_ref"]);

                    //validation is correct, return
                    return true;

                } else { //The cookie has expired
                    return false;
                }

            } else { //The hashes doesn't match
                return false;
            }

        } else { //The a matching selector cannot be found in the database
            return false;
        }
    }

    /**
     * @param $userId_ref int the identifier of the user
     * @return string the role of the user
     */
    public function getRoleByUserID($userId_ref)
    {

        $sql = "SELECT isAdmin, isTeacher FROM accounts.User where userId = ?";
        $sth = $this->db->prepare($sql);
        $sth->execute(array($userId_ref));

        if ($result = $sth->fetch(PDO::FETCH_ASSOC)) {

            //the user is registered as a teacher
            if ($result["isTeacher"] == 1) {
                return "teacher";
                //the user is registered as an admin
            } else if ($result["isAdmin"] == 1) {
                return "admin";
                //the user is a student
            } else {
                return "student";
            }
        } else {
            return "";
        }

    }

    /**
     *
     * Delete the existing user tokens in the database
     * when the user logs in again
     *
     * @param $userId int
     */
    public function deleteToken($userId)
    {
        $sql = "DELETE FROM `accounts`.`auth_tokens` WHERE userId_ref = ?";
        $sth = $this->db->prepare($sql);
        $sth->execute(array($userId));
    }

    /**
     * Set the appropriate session variables for the given user
     * @param $userId int unique user identier
     */
    public function setSessionId($userId)
    {
        $_SESSION['sessionid'] = $userId;
        //get the role of the user
        $role = $this->getRoleByUserID($userId);
        //set the role of the user to the session cookie
        $_SESSION[$role] = "1";
    }

    /**
     * @param $email            string the email address of the new user
     * @return bool             if the email is unique
     */
    public function verifyUniqueEmail($email)
    {

        //Check if the email is a valid email address
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            //not valid email
            return false;
        }

        $sql = "SELECT * FROM accounts.User where email = ?";
        $stmnt = $this->db->prepare($sql);
        $stmnt->execute(array($email));

        //Check if the email address already exists
        if ($stmnt->rowCount() > 0) {
            //not valid email
            return false;
        } else {
            //the email is valid
            return true;
        }

    }

    /**
     * @param $password         string The original passwrord
     * @param $confirmPassword  string The password to confirm...
     * @return bool             if the passwords match or not
     */
    public function verifyPasswords($password, $confirmPassword){
        //Case sensitive and binary safe string comparison
        if (strcmp($password, $confirmPassword) == 0) {
            //the passwords match
            return true;
        } else {
            //the passwords don't match
            return false;
        }
    }

    /**
     *
     * @param $email            string a unique email address of the new user
     * @param $password         string the password of the new user
     * @param $isTeacher        bool the checkbox if the new user wants to be a teacher or not
     *
     * @param $firstname        string firstname
     * @param $lastname         string lastname
     * @return int              the id of the inserted user or 0, if the SQL failes
     *
     */
    public function insertUser($email, $password, $isTeacher, $firstname, $lastname){

        //Generate password hash
        $hashedpassword = password_hash($password, PASSWORD_DEFAULT);

        $default = 0;

        // Insert the user
        $sql = "INSERT INTO User (`firstName` , `lastName` , `email`, `password_hash`, `request_isTeacher`, `isTeacher`, `isAdmin`) VALUES (?,?,?,?,?,?,?)";
        $stmnt = $this->db->prepare($sql);
        $stmnt->bindParam(1, $firstname);
        $stmnt->bindParam(2, $lastname);
        $stmnt->bindParam(3, $email);
        $stmnt->bindParam(4, $hashedpassword);
        $stmnt->bindParam(5, $isTeacher);
        $stmnt->bindParam(6, $default);
        $stmnt->bindParam(7, $default);
        //try to insert user
        if ($stmnt->execute()) {
            return $this->db->lastInsertId();
        } else {
            return 0;
        }
    }

    /**
     * Delete a user
     * @param $userId int the unique user identifier
     */
    public function deleteUser($userId){
        $sql = "DELETE FROM `accounts`.`User` WHERE userId = ?";
        $sth = $this->db->prepare($sql);
        $sth->execute(array($userId));
    }

    /**
     * Returns the user information
     *
     * @return array - firstName, lastName, email
     * @throws Exception
     */
    public function getUser(){
        try{
            $sql = "SELECT `firstName`, `lastName`, `email` FROM User WHERE `userId` = :userid";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(":userid", $_SESSION["sessionid"]);
            if($stmt->execute()){
                return $stmt->fetchAll(PDO::FETCH_ASSOC)[0];
            }else{
                throw new Exception("Could not execute sql statement");
            }
        }catch (PDOException $e){
            throw $e;
        }
    }

    /**
     * getMyPlaylists()
     *
     * Get's the users playlists, returns an array. array("playlistid", "playlistid");
     */
    public function getMyPlaylists(){
        try {
            $conn = DB::getVideoDBConnection();
            $sql = "SELECT `uuid` AS id FROM PlaylistMeta WHERE `managed_by` = :userid";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(":userid", $_SESSION["sessionid"]);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
            //print_r($result);
            //return array_column($result, null, "uuid");
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }

}