<?php
/**
 * Created by PhpStorm.
 * User: Joakim
 * Date: 02/16/2019
 * Time: 8:33 PM
 * _____ _____ _ _ _ _____ _____ __ __ _____
 * |  |  |  _  | | | |  |  |   __|  |  |   __|
 * |     |     | | | |    -|   __|_   _|   __|
 * |__|__|__|__|_____|__|__|_____| |_| |_____|
 *
 * Video:
 *      lecturer
 *      title
 *      tags
 *      course
 *
 * Playlist:
 *      lecturer
 *      title
 *      tags
 *      course
 *
 */
class Hawkeye
{

    //all the types and categories avaliable
    private $types = array("video", "playlist");
    private $categories = array("lecturer", "title", "tags", "course");

    //the current selected type and category (if selected)
    private $currenttype; //video / playlist
    // $currentcategory:$searchcategory ---> lecturer:Gunnar
    private $currentcategory = []; //"lecturer", "title", "tags", "course"
    private $searchcategory = []; //text to search for in a categroy

    //user defined text
    private $searchterm;

    //database connection
    private $db;

    //all the results:
    private $results = [];

    //Bool to see if the user have provived a string in quotes or not
    private $searchwholeword = false;

    /**
     * Hawkeye constructor
     * @param $db               object the PDO object to the database connection
     */
    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * Parse the search string and returns possible finds
     * @param $query            string the searchterm of the user
     * @return array            two dimentional array that contains all of the items found
     */
    public function userSubmittedSearch($query){
        //get the type of search from the user (type and category)

        //the string is surrounded with quotes and the user wants to search for this specific string
        if ($this->isSurroundedWithQoutes($query)) {
            $this->searchterm = trim($query, "\"");
            $this->searchwholeword = True;
        } //parse the search query, if the user have filters in it
        else if (substr_count($query, ':') > 0) {
            $this->parseSearchTerm(trim($query));
        } else {
            $this->searchterm = $query;
        }

        //if the user wants to see a specific type with no category:
        if ($this->currenttype && !$this->currentcategory) {
            $this->searchWithType();
        }
        //if the user wants to see a specific category (and maybe a type?)
        if ($this->currentcategory) {
            $this->searchCategory();
        }
        //search everything at the end
        $this->searchEverything();

        //return the result
        return $this->results;

    }

    /**
     * Find the right type and category from the filters in the search string
     * @param $query            string trimmed searchterm
     */
    private function parseSearchTerm($query){
        //split the text on space ' '
        $subpartsarray = explode(" ", $query);

        foreach ($subpartsarray as $part) {
            //check if the substring contains exactly one delimiter
            if (substr_count($part, ':') == 1) {
                //find out if the first part is type etc.
                list($firstpart, $secondpart) = explode(":", $part);
                //parse the categories + lowercase the search string
                $this->putStringsIntoCorrectCategory(strtolower($firstpart), strtolower($secondpart));
            } else {
                $this->searchterm = $this->searchterm . " " . $part;
            }
        }
    }

    /**
     * From the first and the second part of the "filterstring" --> firstpart:secondpart
     * find the appropriate category and type
     * @param $firstpart        string
     * @param $secondpart       string
     */
    private function putStringsIntoCorrectCategory($firstpart, $secondpart){

        if ($firstpart == "type" && in_array($secondpart, $this->types)) {
            $this->currenttype = $secondpart;

        } else if (in_array($firstpart, $this->categories)) {
            $this->currentcategory[] = $firstpart;
            $this->searchcategory[] = str_replace(str_split('-_'), " ", $secondpart);
        }
    }

    /**
     * search with a specific type
     */
    private function searchWithType(){
        //select from the right database, depending on the type
        $type = ($this->currenttype != $this->types[1]) ? "schooltube.VideoMetadata" : "schooltube.PlaylistMeta";
        $sql = "SELECT uuid,title,description FROM " . $type . " LIMIT 500";
        $stmnt = $this->db->prepare($sql);
        $stmnt->execute();

        //add the elements only if they are not there from before,      if the result is a playlist or a video
        $this->addToResults($stmnt->fetchAll(PDO::FETCH_ASSOC), $this->currenttype == $this->types[1] ? 1 : 0 );
    }

    /**
     * search with a specific category/categories and maybe a type
     */
    private function searchCategory(){

        //if a type is set search for that specific type, else search in both
        if(isset($this->currenttype)){
            $searchtypes = array($this->currenttype);
        }else{
            $searchtypes = $this->types;
        }

        //loop through all the types
        foreach ($searchtypes as $type){
            //translate the type to database and construct the sql statement from the categories and the type
            if($type == $this->types[1]){//The selected type is playlist
                $sql = $this->createSqlStatementFromCategory("schooltube.PlaylistMeta", "playlist_ref", "schooltube.TagOnPlaylist", "managed_by");
            }else if ($type != $this->types[1]){//the selected type is video
                $sql = $this->createSqlStatementFromCategory("schooltube.VideoMetadata", "video_ref", "schooltube.TagOnVideo", "owned_by");
            }
            $stmnt = $this->db->prepare($sql);

            //bind all the prameters into the constructed sql statement
            //number of category-filters
            $arraysize = count($this->searchcategory);
            //loop all of them
            for ($i = 0; $i < $arraysize; $i++){
                //get the right value for the filter
                $bindvalue = $this->searchcategory[$i];
                //special actions for the "lecturer" category:
                if($this->currentcategory[$i] == "lecturer"){
                    //get lecturer id from another database
                    $id = $this->getLecturerIdFromName($this->searchcategory[$i]);
                    $bindvalue = $id["userId"] ?: "";
                }//prevent sql injection:
                $stmnt->bindValue($i+1, $bindvalue);
            }
            //run the query
            $stmnt->execute();
            //add the elements only if they are not there from before       //if the result is a playlist (1) or not
            $this->addToResults($stmnt->fetchAll(PDO::FETCH_ASSOC), $type == $this->types[1] ? 1 : 0);
        }
    }

    /**
     * search for everything(video and playlist) for the $this->searchterm string
     */
    private function searchEverything(){
        //if the user has provided a searchterm in quotes: don't split the string
        if ($this->searchterm) {
            if ($this->searchwholeword) {
                $allthewords = array($this->searchterm);
            } else {//split the string
                $allthewords = explode(" ", $this->searchterm);
            }

            //Get all videos from playlists that match the searchword
            foreach ($allthewords as $currentword) {
                //try to get an userId from the userdatabase
                $id = $this->getLecturerIdFromName($currentword);
                //add wildcards to the sql statement
                $currentword = "%" . $currentword . "%";
                //search all the videos and playlists for items (lecturer, title, tags, course) containing  $currentword
                $sql = "SELECT uuid,title,description FROM schooltube.PlaylistMeta sp LEFT JOIN schooltube.TagOnPlaylist st ON sp.uuid = st.playlist_ref 
                        WHERE sp.title LIKE ?  
                           or st.tag LIKE ? 
                           or sp.course_link LIKE ?
                           or sp.managed_by LIKE ?
                        GROUP BY sp.uuid LIMIT 500";
                $stmnt = $this->db->prepare($sql);
                //prevent sql injection
                $stmnt->execute(array($currentword, $currentword, $currentword, $id["userId"] ?: ""));

                //add the elements only if they are not there from before
                $this->addToResults($stmnt->fetchAll(PDO::FETCH_ASSOC), 1); //playlist
            }

            //do the same thing for the video database
            //get all videoes from videos that match
            foreach ($allthewords as $currentword) {
                $id = $this->getLecturerIdFromName($currentword);
                $currentword = "%" . $currentword . "%";
                //search all the videos and playlists for items (lecturer, title, tags, course) containing  $currentword
                $sql = "SELECT uuid,title,description FROM schooltube.VideoMetadata sv LEFT JOIN schooltube.TagOnVideo st ON sv.uuid = st.video_ref
                        WHERE sv.title LIKE ?
                          or st.tag LIKE ?
                          or sv.course_link LIKE ?
                          or sv.owned_by LIKE ?
                        GROUP BY sv.uuid LIMIT 500";
                $stmnt = $this->db->prepare($sql);
                $stmnt->execute(array($currentword, $currentword, $currentword, $id["userId"] ?: ""));

                //add the elements only if they are not there from before
                $this->addToResults($stmnt->fetchAll(PDO::FETCH_ASSOC) ,0); //video

            }

        }
    }

    /**
     * Finds out if the text is surrounded with qoutes " / '
     * @return true             if the text is surrounded with qoutes
     */
    private function isSurroundedWithQoutes($query){
        if (preg_match('/^(["\']).*\1$/m', $query)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * get the lecturer id from either the first or the last name
     * the person also needs to have a teacher or admin role
     */
    private function getLecturerIdFromName($name){
        $sql = "SELECT userId FROM accounts.User au WHERE (au.isTeacher LIKE 1 or au.isAdmin = 1) and (au.firstName like ? or au.lastName like ?) LIMIT 500;";
        $stmnt = $this->db->prepare($sql);
        $stmnt->execute(array($name, $name));
        return $stmnt->fetch();
    }

    //add the elements in the $fetchall array to the results[] array only if they are not there from before
    private function addToResults($fetchAll, $playlist){
        foreach ($fetchAll as $oneresult) {

            $oneresult["playlist"] = $playlist == 1 ? 1 : 0;

            if (!in_array($oneresult, $this->results)) {
                $this->results[] = $oneresult;
            }
        }
    }

    /**
     * creates the sql statement for the filter function
     *
     * @param $type             string the database name
     * @param $foreignkeyname   string the name of the foreign key constraint
     * @param $foreignkeydb     string the foreign key database name
     * @param $ownername        string the owner name of the video/playlist
     * @return string           the completed sql statement
     */
    private function createSqlStatementFromCategory($type, $foreignkeyname, $foreignkeydb, $ownername){
        //construct the base sql statement
        $sqlstmnt = "SELECT uuid,title,description FROM " . $type . " sdb ";
        $sqlstmnt = $sqlstmnt . "LEFT JOIN " . $foreignkeydb . " fdb ON sdb.uuid = fdb." . $foreignkeyname . " WHERE";

        $loopcounter = 0;
        //loop all of the categories/category
        foreach ($this->currentcategory as $category){
            //add an "AND" between all the filters except for the first time
            if($loopcounter != 0){
                $sqlstmnt = $sqlstmnt . " AND ";
            }

            //construct sql statement for each of the filter types and provide a wildcard
            //the paramters are put in later to prevent sql injection
            switch ($category){
                case "lecturer":
                    $sqlstmnt = $sqlstmnt . " sdb." . $ownername . " LIKE " . "?";
                    break;
                case "title":
                    $sqlstmnt = $sqlstmnt . " sdb.title " . " LIKE " . "?";
                    $this->searchcategory[$loopcounter] = "%" . $this->searchcategory[$loopcounter] . "%";
                    break;
                case "tags":
                    $sqlstmnt = $sqlstmnt . " fdb.tag " . " LIKE " . "?";
                    $this->searchcategory[$loopcounter] = "%" . $this->searchcategory[$loopcounter] . "%";
                    break;
                case "course":
                    $sqlstmnt = $sqlstmnt . " sdb.course_link " . " LIKE " . "?";
                    $this->searchcategory[$loopcounter] = "%" . $this->searchcategory[$loopcounter] . "%";
                    break;
            }

            $loopcounter++;
        }
        //construct the last part of the statement and return it
        $sqlstmnt = $sqlstmnt . " GROUP BY sdb.uuid LIMIT 500";
        return $sqlstmnt;
    }
}