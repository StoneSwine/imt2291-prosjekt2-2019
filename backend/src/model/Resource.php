<?php
/**
 * Created by PhpStorm.
 * User: stone
 * Date: 12.02.19
 * Time: 13:08
 */

/**
 * Class Resource
 * This class is a collection of helper functions or other utilities that one might need
 */
class Resource
{
    /**
     * Generates a token of a certain length
     * @param int $length the length of the token
     * @return string generated token
     */
    public static function generateToken($length = 20)
    {
        try {
            return bin2hex(random_bytes($length / 2));
        } catch (Exception $e) {
        }
    }

    /**
     *
     * Finds the IP address of the connecting user
     * @return mixed the ip address of the user
     * This snippet is taken from:
     *      https://stackoverflow.com/questions/13646690/how-to-get-real-ip-from-visitor
     *
     */
    public static function getUserIP()
    {
        // Get real visitor IP behind CloudFlare network
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }
        $client = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote = $_SERVER['REMOTE_ADDR'];

        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }

        return $ip;
    }

    /**
     * @param $string
     */
    public static function returnFailure($message="There was an error...🤦"){
        $http_origin = $_SERVER['HTTP_ORIGIN'];

        if ($http_origin == "http://www" || $http_origin == "http://localhost:8080" || $http_origin == "http://127.0.0.1:8080") {
            header("Access-Control-Allow-Origin: $http_origin");
        }
        header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
        header("Access-Control-Allow-Headers: Origin");
        header("Access-Control-Allow-Credentials: true");
        header("Content-Type: application/json; charset=utf-8");
        $res = array("status"=>"FAILURE", "message"=>$message);
        echo $res;
        die();
    }

    /**
     * Output the debug messsage to console
     * @param $debugMessage
     */
    public static function debug($debugMessage){
        echo  "<script> console.log('" . json_encode($debugMessage)  . "')</script>";

    }

}