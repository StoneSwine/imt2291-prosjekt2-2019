<?php
/**
 *
 * This file is included in the beginning of each page where the user needs to be authenticated
 *
 * "First, the remembered login is checked with the PHP session. If it returns false, then the code will search for
 * the authentication keys stored in the cookies. If the keys are not empty then they will be hashed compared with
 * the database. Once the match found then the expiration date is validated with the current date and time.
 * Once the code passes through with all the validation, the user will be redirected to the dashboard."
 *
 * <CREDITS>
 *      https://phppot.com/php/secure-remember-me-for-login-using-php-session-and-cookies/
 * </CREDITS>
 *
 */
require_once realpath(dirname(__FILE__)) . "/User.php";
require_once realpath(dirname(__FILE__)) . "/Resource.php";
require_once realpath(dirname(__FILE__)) . "/DB.php";

//create objects
$user = new User(DB::getAccountsDBConnection());
$userdata = array();

// Get Current date, time
$current_time = time();
$currentdate = date("Y-m-d H:i:s", $current_time);

// Set Cookie expiration for 1 month
$cookie_expiration_time = $current_time + (30 * 24 * 60 * 60);  // 1 month

//the user is by default not logged in
$isLoggedIn = false;
$isStudent = false;
$isTeacher = false;
$isAdmin = false;


// Check if SESSION exists first
if (isset($_SESSION['sessionid'])) {
    $isLoggedIn = true;

    //get the status of the user (admin / teacher etc..)
    if (isset($_SESSION['teacher'])) {
        $isTeacher = true;
    } else if (isset($_SESSION['admin'])) {
        $isAdmin = true;
    } else {
        $isStudent = true;
    }


    /*
     * Get some user credentials
     * */
    try{
        $userdata = $user->getUser();
        //print_r($userdata);
    }catch (PDOException $e){
        //Could not get userdata - what to do

    }

} // ELSE IF: check if COOKIE(S) exists and generate session
else if (!empty($_COOKIE["remember_me"])) {
// Verify all the cookie values with the database tables selector etc..
    $clientcookie = trim($_COOKIE["remember_me"]);

    //split the cookie into selector and validator strings
    list($clientselector, $clientvalidator) = explode(':', $clientcookie);

    //validate the cookie
    if ($user->validateCookie($clientselector, $clientvalidator, $currentdate)) {

        //the user has a valid cookie: set the appropriate session cookies
        $isLoggedIn = true;
        if (isset($_SESSION['teacher'])) {
            $isTeacher = true;
        } else if (isset($_SESSION['admin'])) {
            $isAdmin = true;
        } else {
            $isStudent = true;
        }
    } else {//The user cannot be verified
        // clear the users cookis if they have them
        $user->clearCookie();
        //redirect to login page
        Resource::returnFailure("User could no be verified");
    }
} // Else: cookies does not exist for the user AND the user does not have the session-identifier
else {
    //mark the token as expired in the database /  clear cookies?
    //redirect?

}


