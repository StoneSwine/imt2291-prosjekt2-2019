# Schooltube

## Deltagere
* Joakim N. Ellestad (141245)
* Magnus Lien Lilja (473150)

### Usage

Make sure you do not have any conflicting volumes, run : docker volume prune

First time usage, rebuild everything : docker-compose up --build

After that, start with : docker-compose up

**Install npm dependencies**
Jump into the container `www` with `docker exec -it www_1 bash` and execute `npm install`. This will install the necessary packages/dependencies listed in package.json.

A restart of the container might be necessary.


Go to : http://localhost:8080 to view the page **Veldig viktig å bruke localhost og ikke 127.0.0.1 på Chromium basert nettleser, noe rart med video strømming som ikke går med 127.0.0.1 🤷‍♂️**

## Få medie filer over i docker volum
** Sjekk filbane og navn på container først. Skal i api der php kjører. Dvs. Dockerfile.api**

`docker cp ./media/production/. imt2291-prosjekt2-2019_api_1:/storage/video`

## Eksisterende brukere på systemet (dummy.sql blir kjørt automatisk inn i databasen)

`kyrre@docker.com`, passord: `admin` - Har 2 spillelister med respektive 1 og 2 videoer i. 

`pepe@frog.com`, passord: `admin` - Er en student og abonerer på en spilleliste

`example@mail.com`, passord: `admin` - Er lærer og har 1 video.

## NB

Merk at på enkelte views så blir du stående der etter du f.eks. har logget inn, registrert deg, lastet opp video. Dette er fordi vi ikke har fått til å bruke `iron-pages` i stater-templaten helt riktig, fungerer fint for navigering. 
Benytt heller navigering på venstre side for å komme videre.

Det har vært litt trøblete å få subtitles til å fungere i Mozilla. Det skal hvertfall gå fint på chromium baserte nettlesere(Chrome, Vivaldi etc.). 


## Dokumentasjon
All dokumentasjon finnes i **[wikien](https://bitbucket.org/StoneSwine/imt2291-prosjekt2-2019/wiki/Home)** til dette repositoriet