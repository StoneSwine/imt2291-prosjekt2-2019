
CREATE DATABASE IF NOT EXISTS accounts
  CHARACTER SET = 'latin1'
  COLLATE = 'latin1_swedish_ci';

/* need this line becuase of: https://github.com/docker-library/mariadb/issues/15  */
GRANT ALL PRIVILEGES ON accounts.* TO 'admin' identified by 'admin';

USE  accounts;
/**
* Tabell for brukere
* identifiser med email + password_hash
*
*/
CREATE TABLE User(
userId int(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL,
firstName VARCHAR(70) NOT NULL,
lastName VARCHAR(100) NOT NULL,
email VARCHAR(320) NOT NULL,
password_hash VARCHAR(255) NOT NULL,
request_isTeacher BOOLEAN DEFAULT 0,
isTeacher BOOLEAN DEFAULT 0,
isAdmin BOOLEAN DEFAULT 0
);

/**
* auth_tokens
*/
CREATE TABLE auth_tokens (
   id INTEGER(11) UNSIGNED NOT NULL PRIMARY KEY  AUTO_INCREMENT,
   selector char(12),
   hashedValidator char(64),
   userId_ref INTEGER(11) UNSIGNED NOT NULL,
   expires datetime,
   last_login_agent VARCHAR(250),
   last_login_ip VARCHAR(40),
   FOREIGN KEY(userId_ref) REFERENCES User(userId)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);


/**
* Emne/Course
*  Det finnes emner, brukere/lærere er tilnyttet emner.
*/
CREATE TABLE Course (
course_title VARCHAR(255) NOT NULL PRIMARY KEY,
description VARCHAR(255)
);

CREATE TABLE UserWithCourse(
user_ref int(6) UNSIGNED,
course_ref VARCHAR(255),
FOREIGN KEY (course_ref) REFERENCES Course(course_title)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
FOREIGN KEY (user_ref) REFERENCES User(userId)
  ON DELETE CASCADE
  ON UPDATE CASCADE
);

