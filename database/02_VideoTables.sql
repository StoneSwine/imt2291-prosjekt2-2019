/*Tables for video*/

CREATE DATABASE IF NOT EXISTS schooltube
  CHARACTER SET = 'latin1'
  COLLATE = 'latin1_swedish_ci';

/* need this line becuase of: https://github.com/docker-library/mariadb/issues/15  */
GRANT ALL PRIVILEGES ON schooltube.* TO 'admin' identified by 'admin';

USE schooltube;
/**
* Metadata for video
* uuid: Bruk UUID_SHORT() når ny video lages. Denne brukes for å lagre fil i filsystemet også
* thumbnail: base64 før lagres i databasen. Kan lagre binary, men skal vises på nettside som base64 uansett så?
* owned_by: er id til brukeren som oppretter vidoen
* course_link: Hvilket emne er videoen tilnyttet
*/
CREATE TABLE VideoMetadata(
uuid BIGINT UNSIGNED NOT NULL PRIMARY KEY DEFAULT UUID_SHORT(),
title VARCHAR(255),
description VARCHAR(255),
extension varchar(10),
mime varchar(50),
thumbnail MEDIUMTEXT,
owned_by INTEGER(6),
course_link VARCHAR(255)
);


/**
* Rangering av video
* made_by: Brukeren som har laget raden
* Refererer video med video_ref til video sin uuid.
*/
CREATE TABLE VideoRank(
uuid BIGINT UNSIGNED NOT NULL PRIMARY KEY DEFAULT UUID_SHORT(),
rankValue INT(1),
video_ref BIGINT UNSIGNED,
made_by INTEGER(6),
FOREIGN KEY(video_ref) REFERENCES VideoMetadata(uuid)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
UNIQUE KEY (video_ref, made_by)
);

/**
* Kommentar på video
* made_by: Brukeren som har laget raden
* Refererer video med video_ref til video sin uuid.
*/
CREATE TABLE VideoComment(
uuid BIGINT UNSIGNED NOT NULL PRIMARY KEY DEFAULT UUID_SHORT(),
comment VARCHAR(255),
video_ref BIGINT UNSIGNED,
made_by INTEGER(6),
dTime DATETIME(6),
FOREIGN KEY(video_ref) REFERENCES VideoMetadata(uuid)
  ON DELETE CASCADE
  ON UPDATE CASCADE
);

/**
* Spillelister
* Spillelister med like navn kan lages. Derfor uuid istedet for å bare bruke title
* managed_by noen identifisert med integer(6), siden dette bukes som id hos alle brukere
* course_link: Hvilket emne er videoen spillelisten tilknyttet om den er linket i det hele tatt
 */
CREATE TABLE PlaylistMeta(
uuid BIGINT UNSIGNED NOT NULL PRIMARY KEY DEFAULT UUID_SHORT(),
title VARCHAR(255) NOT NULL,
description VARCHAR(255),
managed_by INTEGER(6) UNSIGNED,
course_link VARCHAR(255),
thumbnail MEDIUMTEXT
);

/**
* PlaylistVideo - Hvilke videoer som er i spillelisten
* Hvis video eller spilleliste slettes så fjernes raden her også. aka CASCADE
* REF: https://dev.mysql.com/doc/refman/8.0/en/create-table-foreign-keys.html
*/
CREATE TABLE PlaylistVideo(
  playlist_ref BIGINT UNSIGNED,
  video_ref BIGINT UNSIGNED,
  position int,
  date_added DATETIME default(NOW()),
  FOREIGN KEY (video_ref) REFERENCES VideoMetadata(uuid)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  FOREIGN KEY (playlist_ref) REFERENCES PlaylistMeta(uuid)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    UNIQUE KEY (playlist_ref, video_ref)
);

/*
* Brukere skal kunne abbonere på spillelister
last_visited - oppdateres hver gang brukeren går inn på spilleliste, på den måten kan vi tracke om nye videoer for brukeren er lagt i spillelista
*/
CREATE TABLE PlaylistSubscription(
playlist_ref BIGINT UNSIGNED,
user_ref INTEGER(6) UNSIGNED,
last_visited DATETIME default(NOW()),
subscribed Boolean NOT NULL DEFAULT True,
 FOREIGN KEY (playlist_ref) REFERENCES PlaylistMeta(uuid)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
UNIQUE KEY (playlist_ref, user_ref)
);


/**
* Tags
*/

/*CREATE TABLE Tags(
  tagName VARCHAR(50) NOT NULL PRIMARY KEY UNIQUE
);*/

/*
* TagsOnVideo - Tagger på videoer
*
*/

CREATE TABLE TagOnVideo(
  tag VARCHAR(50),
  video_ref BIGINT UNSIGNED,
  FOREIGN KEY(video_ref) REFERENCES  VideoMetadata(uuid)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    UNIQUE KEY (tag, video_ref)
);


CREATE TABLE TagOnPlaylist(
  tag VARCHAR(50),
  playlist_ref BIGINT UNSIGNED,
  FOREIGN KEY(playlist_ref) REFERENCES PlaylistMeta(uuid)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    UNIQUE KEY (tag, playlist_ref)
);