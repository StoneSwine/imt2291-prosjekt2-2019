import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import { setPassiveTouchGestures, setRootPath } from '@polymer/polymer/lib/utils/settings.js';
import '@polymer/app-layout/app-drawer/app-drawer.js';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
import '@polymer/app-layout/app-header/app-header.js';
import '@polymer/app-layout/app-header-layout/app-header-layout.js';
import '@polymer/app-layout/app-scroll-effects/app-scroll-effects.js';
import '@polymer/app-layout/app-toolbar/app-toolbar.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/paper-toast/paper-toast.js';


//import './my-icons.js';
import './components/api-status-element.js';
import './components/user-status.js';
import store from './store.js';

// Gesture events like tap and track generated from touch will not be
// preventable, allowing for better scrolling performance.
setPassiveTouchGestures(true);

// Set Polymer's root path to the same value we passed to our service worker
// in `index.html`.
setRootPath(MyAppGlobals.rootPath);

class Schooltube extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          --app-primary-color: #4285f4;
          --app-secondary-color: black;

          display: block;
        }

        app-drawer-layout:not([narrow]) [drawer-toggle] {
          display: none;
        }

        app-header {
          color: #fff;
          background-color: var(--app-primary-color);
        }

        app-header paper-icon-button {
          --paper-icon-button-ink-color: white;
        }

        .drawer-list {
          margin: 0 20px;
        }

        .drawer-list a {
          display: block;
          padding: 0 16px;
          text-decoration: none;
          color: var(--app-secondary-color);
          line-height: 40px;
        }

        .drawer-list a.iron-selected {
          color: black;
          font-weight: bold;
        }
        .logo{
          max-height: 100%;
          max-width: 100px;
        }
      </style>

      <app-location route="{{route}}" url-space-regex="^[[rootPath]]">
      </app-location>

      <app-route 
        route="{{route}}" 
        pattern="/:page" 
        data="{{routeData}}" 
        tail="{{subroute}}">
      </app-route>

      <!-- Rute for video med videoid som hengende etter-->
      <app-route 
      route="{{route}}" 
      pattern="/watch"
      tail={{watchTail}} 
      active="{{videoPageActive}}">
    </app-route>

    <!-- Rute for endring av spillelsite, med id hengene etter (/editPlaylist/playlistid)-->
    <app-route 
    route="{{route}}" 
    pattern="/editPlaylist"
    tail={{playlistidtail}} 
    active="{{editPlaylistActive}}">
  </app-route>
  <!-- Rute for endring av video, videoid hengende etter. /editVideo/<videoid> -->
  <app-route
  route="{{route}}"
  pattern="/editVideo"
  tail={{editVideoTail}}
  active="{{editVideoActive}}">
  </app-route>

      <app-drawer-layout fullbleed="" narrow="{{narrow}}">
        <!-- Drawer content -->
        <app-drawer id="drawer" slot="drawer" swipe-open="[[narrow]]">
          <app-toolbar>Meny</app-toolbar>
          <iron-selector selected="[[page]]" attr-for-selected="name" class="drawer-list" role="navigation">
            <a name="home" href="[[rootPath]]home">Hjem</a>
            <a name="search" href="[[rootPath]]search">Søk</a>
            <a name="showAllVideos" href="[[rootPath]]showAllVideos">Vis alle videoer</a>
            <a name="showAllPlaylists" href="[[rootPath]]showAllPlaylists">Vis alle spillelister</a>
            <!--START Teacher or admins can see this START-->
            <template is="dom-if" if="{{user.isTeacher}}">
              <hr>
            <a name="uploadVideo" href="[[rootPath]]uploadVideo">Last opp video</a>
            <a name="myVideos" href="[[rootPath]]myVideos">Mine videoer</a>
            <a name="managePlaylists" href="[[rootPath]]managePlaylists">Mine spillelister</a>
            </template>
            <!--END Teacher or admins can see this END-->
            <!-- START Admin view START -->
              <template is="dom-if" if="{{user.isAdmin}}">
                <hr>
                <a name="adminPage" href="[[rootPath]]adminPage">Administrer</a>
                <hr>
              </template>
            <!-- END Admin view END -->
          </iron-selector>
        </app-drawer>

        <!-- Main content -->
        <app-header-layout has-scrolling-region="">

          <app-header slot="header" condenses="" reveals="" effects="waterfall">
            <app-toolbar>
              <!-- paper icon vises på smale skjermer.-->
              <paper-icon-button icon="icons:menu" drawer-toggle=""></paper-icon-button>
              <img class="logo" src="./images/logo_ntnu_u-slagord.png"></img>
              <div main-title="">Schooltube</div>
              <user-status></user-status>
              <api-status-element></api-status-element>
            </app-toolbar>
          </app-header>

          <iron-pages selected="[[page]]" attr-for-selected="name" role="main">
            <recent-videos-view name="home"></recent-videos-view>
            <show-all-videos-view name="showAllVideos" route="{{subroute}}"></show-all-videos-view>
            <show-all-playlists-view name="showAllPlaylists" route="{{subroute}}"></show-all-playlists-view>
            <upload-video-view name="uploadVideo" toast="{{toast}}" route="{{subroute}}"></upload-video-view>
            <my-videos-view name="myVideos" route="{{subroute}}"></my-videos-view>
            <manage-playlists-view name="managePlaylists" route="{{subroute}}"></manage-playlists-view>
            <administration-view name="adminPage" toast="{{toast}}" route="{{subroute}}"></administration-view>
            <watch-video-view name="watch" toast="{{toast}}" videoid="[[videoid]]" route="{{watchTail}}"></watch-video-view>
            <edit-playlist name="editPlaylist" playlistid="[[playlistid]]" route="{{playlistidtail}}"></edit-playlist>
            <edit-video-view name="editVideo" toast="{{toast}}" videoid="[[editvideoid]]" route="{{editVideoTail}}"></edit-video-view>
            <search-result-view name="search" toast="{{toast}}" route="{{subroute}}"></search-result-view>
            <login-view name="loginView" isloggedin="[[this.user.isLoggedIn]]" toast="{{toast}}"></login-view>
            <register-view name="registerView" toast="{{toast}}"></register-view>
            <my-view404 name="view404"></my-view404>
          </iron-pages>
        </app-header-layout>
      </app-drawer-layout>
      <paper-toast id="errorToast" text="No error..yet! "></paper-toast>
    `;
  }

  static get properties() {
    return {
      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged'
      },
      routeData: Object,
      subroute: Object,
      user:{
        type: Object,
        value: {student: false, teacher: false, admin: false, isLoggedIn: false}
      },
      videoid:{
        type: String,
        reflectToAttribute: true
      },
      watchTail: {type: Object, notify: true, reflectToAttribute: true},
      videoPageActive: {
        type: Boolean,
        reflectToAttribute: true,
        observer: '_videoPageActiveChanged'
      },
      playlistid: {type: String, reflectToAttribute: true},
      playlistidtail: {type: Object, notify: true, reflectToAttribute: true},
      editPlaylistActive: {
        type: Boolean,
        reflectToAttribute: true,
        observer: '_editPlaylistChanged'
      },
      editvideoid:{
        type: String,
        reflectToAttribute: true
      },
      editVideoTail:{type: Object, notify: true, reflectToAttribute: true},
      editVideoActive:{
        type: Boolean,
        reflectToAttribute: true,
        observer: '_editVideoChanged'
      },
      toast: Object /* The error that appears in the lower left. A reference to the DOM element */
    };
  }

  constructor(){
    super();
    //console.log(this.page);
    //this.page = "/";
    //console.log(this.page);
    const data = store.getState();
    this.user = data.userstatus;
    store.subscribe(()=>{
      const state = store.getState();
      this.user = state.userstatus;
      //console.log(state.changedPage);
      //console.log(this.page);
      if(!this.page){
     // if(this.page !== state.changedPage){  //This breaks if page is undefined
        //console.log(state.changedPage);
        //window.history.pushState({}, '', state.changedPage.page);
        this.set('route.path',state.changedPage.page);
        //this.routeData.page = state.changedPage.page;
      }
      
    });
  }
  
  ready(){
    super.ready();
    this.toast = this.shadowRoot.getElementById("errorToast");
    //this.toast.text = "Edited on ready";
  }

  static get observers() {
    return [
      '_routePageChanged(routeData.page)'
    ];
  }

  _routePageChanged(page) {
     // Show the corresponding page according to the route.
     //
     // If no page was found in the route data, page will be an empty string.
     // Show 'view1' in that case. And if the page doesn't exist, show 'view404'.
    if (!page) {
      this.page = 'home';
    } else if (['watch', 'home', 'showAllVideos', 'search', 'showAllPlaylists', 'uploadVideo','myVideos', 'managePlaylists', 'adminPage','editPlaylist', 'editVideo', 'loginView', 'registerView'].indexOf(page) !== -1) {
      this.page = page;
    } else {
      this.page = 'view404';
    }

    // Close a non-persistent drawer when the page & route are changed.
    if (!this.$.drawer.persistent) {
      this.$.drawer.close();
    }
  }

  _pageChanged(page) {
    // Import the page component on demand.
    //
    // Note: `polymer build` doesn't like string concatenation in the import
    // statement, so break it up.
    switch (page) {
      case 'home':
        import('./components/recent-videos-view.js');
        break;
      case 'showAllVideos':
        import('./components/show-all-videos-view.js');
        break;
      case 'showAllPlaylists':
        import('./components/show-all-playlists-view.js');
        break;
      case 'uploadVideo':
        import('./components/upload-video-view.js');
       break;
      case 'myVideos':
        import('./components/my-videos-view.js');
        break;
      case 'managePlaylists':
        import('./components/manage-playlists-view.js');
        break;
      case 'adminPage':
        import('./components/administration-view.js');
        break;
      case 'loginView':
        import('./components/login-view/login-view.js');
        break;
      case 'registerView':
        import('./components/register-view/register-view.js');
        break;
        /*case 'watch':
        import('./components/watch-video-view.js');
        console.log(this.routeData);
        this.videoid = this.routeData.id;
        //console.log(this.videoid);
        break;*/
      case 'search':
        import("./components/search-result-view.js");
        break;
      case 'view404':
        import('./my-view404.js');
        break;
    }
  }
  _videoPageActiveChanged(data){
    if(data){
      import('./components/watch-video-view.js');
      console.log("videoPageActiveChanged");
      this.videoid = this.watchTail.path.slice(1); // removes leading "/12345"
      //console.log(this.watchTail);
      //console.log(this.videoid);
     //this.videoid = this.routeData.id;
    }
  }
    _editPlaylistChanged(data){
      if(data){
        import('./components/edit-playlist.js');
        this.playlistid = this.playlistidtail.path.slice(1);
        console.log(this.playlistid);
      }
    }
    _editVideoChanged(data){
      if(data){
        import('./components/edit-video-view.js');
        this.editvideoid = this.editVideoTail.path.slice(1);
        console.log(this.editvideoid);
      }
    }
  
}

window.customElements.define('schooltube-app', Schooltube);
