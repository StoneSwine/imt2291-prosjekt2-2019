import thunkMiddleware from 'redux-thunk';
import { createStore, applyMiddleware  } from "redux";

import schooltubeApp from "./reducers/index";

const store = createStore(schooltubeApp, applyMiddleware(thunkMiddleware));


export default store;
