/** 
* Actions are payloads of information that send data from your application to your store. 
* They are the only source of information for the store. You send them to the store using store.dispatch().
* Actions are plain JavaScript objects. Actions must have a type property that indicates the type of action being performed. 
* Types should typically be defined as string constants. 
*/

// Action type: to see the availability status of the api/backend/server
export const UPDATE_API_STATUS = "UPDATE_API_STATUS";

//Action types: User login and logout
export const LOG_IN = "LOG_IN";
export const LOG_OUT = "LOG_OUT";

/*
 * Other constants
 */

export const API_Availability = {
    MAINTANANCE: 'MAINTANANCE',
    LOW: 'LOW',
    MEDIUM: 'MEDIUM',
    HIGH: 'HIGH'
}

export const CHANGE_PAGE = "CHANGE_PAGE";

export const PLAYLISTS_CHANGED = "PLAYLISTS_CHANGED";
