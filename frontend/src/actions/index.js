//import fetch from 'cross-fetch'
import {UPDATE_API_STATUS, LOG_IN, LOG_OUT, CHANGE_PAGE, PLAYLISTS_CHANGED} from "./../constants/action-types";
/**
 * Action creators are exactly that—functions that create actions. 
 * It's easy to conflate the terms “action” and “action creator”, so do your best to use the proper term.
 */

 // Action creator: to update the status received from the server
export function updateAPIStatus(status){
    return{ type: UPDATE_API_STATUS, status: status }
}

 // Action creator: To login and logout the user.
export function login(userdata){
    return {type: LOG_IN, details: userdata}
}
export function logout(){
    return {type: LOG_OUT}
}

// Call when to change page
export function changePage(page){
    return {type: CHANGE_PAGE, details: page}
}


/**
 * ACTIONS nedenfor tilhører familien av playlist handlinger
 * 
 */

// Intern action som skal hente spillelister
export const REQUEST_PLAYLISTS = 'REQUEST_PLAYLISTS'; 
function requestPlaylists(){
    return {type: REQUEST_PLAYLISTS}
}
// Intern action som mottar spillelister 
export const RECEIVE_PLAYLISTS = 'RECEIVE_PLAYLISTS';
function receivePlaylists(json){
    return {
        type: RECEIVE_PLAYLISTS,
        details: json
    }
    
}

// Eksportert funksjon som henter spillelister
export const GET_PLAYLISTS = 'GET_PLAYLISTS'
export function getPlaylists(){
    return function(dispatch){

        // Si ifra til interessenter at jeg holder på å hente ned spillelister
        dispatch(requestPlaylists())

        // Hent ned spillelister fra api
        return fetch(`${window.MyAppGlobals.apiURL}api/getPlaylist.php`, {
            method: 'GET',
            credentials: "include"
        })
        .then( 
            res => res.json(),
            error => console.log('Feil i getPlaylist', error)
        )
        .then(res =>
            // Si ifra til interessenter at spillelister nå er hentet
            dispatch(receivePlaylists(res)))
    }

}

/**
 * Starter en kjede med actions som subscriber/unsubscriber for en spilleliste
 * Gjør en nettverksforespørsel for å få oppdatert endringer - ikke det mest optimale... :/
 * @param {*} playlistid 
 */
export const TOGGLE_PLAYLIST_SUBSCRIPTION = 'TOGGLE_PLAYLIST_SUBSCRIPTION';
export function toggleSubscription(playlistid){

    return function(dispatch){
        var data = new FormData();
        data.append('playlistid',playlistid);
        return fetch(`${window.MyAppGlobals.apiURL}api/toggleSubscriptionToPlaylist.php`,{
            method: "POST",
            credentials: "include",
            body: data
        }).then(
            res=>res.json(),
            error => console.log('Kunne ikke endre subscribe/unsubscribe', error)
        )
        .then(res=>{
            if(res.status === 'SUCCESS'){
                dispatch(getPlaylists())
            }
        })
    }
}
/**
 * Fjerner video fra spilleliste. Dispatcher en action for å oppdatere den lokale spilleliste dataen
 */
export const INVALIDATE_PLAYLIST = 'INVALIDATE_PLAYLIST';
function invalidatePlaylist(playlistid){
    return {type: INVALIDATE_PLAYLIST, playlistid: playlistid}
}
export const REQUEST_PLAYLIST = 'REQUEST_PLAYLIST';
function requestPlaylist(playlistid){
    return {type: REQUEST_PLAYLIST, playlistid: playlistid}
}
export const RECEIVE_PLAYLIST = "RECEIVE_PLAYLIST";
export function receivePlaylist(playlist){
    return {type: RECEIVE_PLAYLIST, details: playlist}
}

export const REMOVE_VIDEO_IN_PLAYLIST_FINISHED = 'REMOVE_VIDEO_IN_PLAYLIST_FINISHED';
export const REMOVE_VIDEO_IN_PLAYLIST = 'REMOVE_VIDEO_IN_PLAYLIST';
export function removeVideoInPlaylist(playlistid, videoid){

    return function (dispatch){

        // Si ifra at dataen i denne spillelista er ugyldig
        dispatch(invalidatePlaylist(playlistid))
        
        var data = new FormData();
        data.append('remove', 'yesplease');
        data.append('playlistid',playlistid);
        data.append('videoid', videoid);
        
        return fetch(`${window.MyAppGlobals.apiURL}api/updatePlaylist.php`,{
            method: "POST",
            credentials: "include",
            body: data
        }).then(
            res=>res.json(),
            error => console.log('Kunne ikke fjerne video i spilleliste', error)
        )
        .then( res=>{
            if(res.status === 'SUCCESS'){
                dispatch(getPlaylist(playlistid))
            }

        })
    }
}

// Henter en spilleliste
export const GET_PLAYLIST = "GET_PLAYLIST"
export function getPlaylist(playlistid){
    return function (dispatch){

        // La interesser få vite at du arbeider med å hente ned fresh data på spillelista
        dispatch(requestPlaylist(playlistid))
        
        return fetch(`${window.MyAppGlobals.apiURL}api/getPlaylist.php?playlistid=${playlistid}`,{
            method: 'GET',
            credentials: "include",
        })
        .then(
            res=> res.json(),
            error=> console.log('Kunne ikke hente metadata for spilleliste', error)
        )
        .then(res=>{
            // Si ifra at du har henta ned spillelista
            // TODO: Håndtere status=>FAILURE
            console.log(res)
            dispatch(receivePlaylist(res.playlist))
            
        });
    }

}
export const UPDATE_POSITION_FOR_VIDEO_IN_PLAYLIST = 'UPDATE_POSITION_FOR_VIDEO_IN_PLAYLIST';
export function updatePositionForVideoInPlaylist(playlistid, videoid, newPosition){

    return function(dispatch){

        // Si ifra til interessenter at spillelista sin data muligens er ugyldig
        dispatch(invalidatePlaylist(playlistid));

        var data = new FormData();
        data.append('changeVideoPosition', 'yesplease');
        data.append('playlistid',playlistid);
        data.append('videoid', videoid);
        data.append('position', newPosition);

        return fetch(`${window.MyAppGlobals.apiURL}api/updatePlaylist.php`,{
            method: "POST",
            credentials: "include",
            body: data
        }).then(
            res=>res.json(),
            error => console.log('Kunne ikke endre rekkefølge i spilleliste', error)
        )
        .then( res=>{
            if(res.status === 'SUCCESS'){
                dispatch(getPlaylist(playlistid))
            }

        }) 

    }
} 

/**
* Actions nedenfor hører til familien for èn video, dvs. når man arbeider med èn video kan disse brukes
*/

export const ADD_COMMENT_ON_VIDEO = 'ADD_COMMENT_ON_VIDEO';
export const DELETE_COMMENT_ON_VIDEO = 'DELETE_COMMENT_ON_VIDEO';

export const ADD_RATING_ON_VIDEO = 'ADD_RATING_ON_VIDEO';

export const GET_METADATA_FOR_VIDEO = 'GET_METADATA_FOR_VIDEO';
export const GET_COMMENTS_FOR_VIDEO = 'GET_COMMENTS_FOR_VIDEO';

//export const GET_RATING_FOR_VIDEO = 'GET_RATING_FOR_VIDEO';

export const INVALIDATE_VIDEO_METADATA = 'INVALIDATE_VIDEO_METADATA';

export const REQUEST_VIDEO_METADATA = 'REQUEST_VIDEO_METADATA';
export const RECEIVE_VIDEO_METADATA = 'RECEIVE_VIDEO_METADATA';

export const REQUEST_VIDEO_COMMENTS = 'REQUEST_VIDEO_COMMENTS';
export const RECEIVE_VIDEO_COMMENTS = 'RECEIVE_VIDEO_COMMENTS';


function requestVideoMetadata(videoid){
    return{type: REQUEST_VIDEO_METADATA, details: videoid}
}
function receiveVideoMetadata(videoMetadata){
    return{type: RECEIVE_VIDEO_METADATA, details: videoMetadata}
}
function requestVideoComments(videoid){
    return{type: REQUEST_VIDEO_COMMENTS, details: videoid}
}
function receiveVideoComments(videoComments){
    return{type: RECEIVE_VIDEO_COMMENTS, details: videoComments}
}
// Henter metadata for en video
export function getMetadataForVideo(videoid){

    return function(dispatch){
        
        // Forteller interessenter om at ny video er forespurt
        dispatch(requestVideoMetadata(videoid));

        return fetch(`${window.MyAppGlobals.apiURL}api/getVideoMetadata.php?videoid=${videoid}`,{
            method: "GET",
            credentials: "include"
        }).then(
            res=> res.json(),
            error=>console.log('Kunne ikke hente metadata for video',error)
        ).then(res=>{
            
            if(res.status === 'SUCCESS'){
                dispatch(receiveVideoMetadata(res.metadata))
            }else{
                // God forbid
                // TODO: Håndtere error
            }
        })
    }
}

export function getCommentsForVideo(videoid){
    return function(dispatch){
        
        // Forteller interessenter om at ny video er forespurt
        dispatch(requestVideoComments(videoid));

        return fetch(`${window.MyAppGlobals.apiURL}api/getVideoComments.php?videoid=${videoid}`,{
            method: "GET",
            credentials: "include"
        }).then(
            res=> res.json(),
            error=>console.log('Kunne ikke hente kommentarer for video',error)
        ).then(res=>{
            
            if(res.status === 'SUCCESS'){
                dispatch(receiveVideoComments(res.comments))
            }else{
                // God forbid
                // TODO: Håndtere error
            }
        })
    }
}

export function deleteCommentOnVideo(videoid, commentid){
    return function(dispatch){
        var data = new FormData();
        data.append('videoid', videoid);
        data.append('deletecomment', 'yesplease');
        data.append('commentid', commentid);

        return fetch(`${window.MyAppGlobals.apiURL}api/updateVideoComments.php`,{
            method: "POST",
            credentials: "include",
            body: data
        })
        .then(
            res=> res.json(),
            error=> console.log("Kunne ikke slette kommentar", error) 
        )
        .then(res=>{
            if(res.status === "SUCCESS"){
                dispatch(getCommentsForVideo(videoid))
            }else{
                // TODO: håndtere error
            }
        })
    }
}

// Legg til en kommentar i kommentarfeltet på video
// Sørger selv for å oppdatere store
export function addCommentOnVideo(videoid, comment){
    return function(dispatch){
        var data = new FormData();
        data.append('videoid', videoid);
        data.append('addcomment', 'yesplease');
        data.append('comment', comment);

        return fetch(`${window.MyAppGlobals.apiURL}api/updateVideoComments.php`,{
            method: "POST",
            credentials: "include",
            body: data
        })
        .then(
            res=> res.json(),
            error=> console.log("Kunne ikke legge til kommentar", error) 
        )
        .then(res=>{
            if(res.status === "SUCCESS"){
                dispatch(getCommentsForVideo(videoid))
            }else{
                // TODO: håndtere error
            }
        })

    }
}