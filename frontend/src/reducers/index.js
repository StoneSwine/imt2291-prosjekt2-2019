import { UPDATE_API_STATUS, API_Availability, LOG_IN, LOG_OUT, CHANGE_PAGE, PLAYLISTS_CHANGED} from "./../constants/action-types";
import { GET_PLAYLISTS, REQUEST_PLAYLISTS, RECEIVE_PLAYLISTS, INVALIDATE_PLAYLIST, REMOVE_VIDEO_IN_PLAYLIST, RECEIVE_PLAYLIST, REQUEST_PLAYLIST, REQUEST_VIDEO_METADATA, RECEIVE_VIDEO_METADATA, REQUEST_VIDEO_COMMENTS, RECEIVE_VIDEO_COMMENTS} from "./../actions/index"
import { combineReducers } from "redux";
/**
 * Reducers specify how the application's state changes in response to actions sent to the store. 
 * Remember that actions only describe what happened, but don't describe how the application's state changes.
 */

 /**
  * isActive: either true or false
  * apiAvailability: MAINTANANCE means the server api is down.
  */
const apiStatusInitialState = {
    isActive: false, 
    apiAvailability: API_Availability.MAINTANANCE
};

// A reducer to support the api status
function apiStatusReducer(state = apiStatusInitialState, action){
    switch(action.type){
        case UPDATE_API_STATUS:
            return Object.assign({}, state, {
                isActive: action.status.isActive,
                apiAvailability: action.status.apiAvailability
               
            });
        default:
            return state;
    }
}

const userInitialState = {
    isLoggedIn: false,
    isStudent: false, 
    isTeacher: false, 
    isAdmin: false,
    email: "",
    uuid: ""
}
function userStatusReducer(state = userInitialState, action){
    switch(action.type){
        case LOG_IN:
            return Object.assign({}, state, action.details);
        case LOG_OUT:
            return Object.assign({}, state, userInitialState);
        default: return state;
    }
}


function changePageReducer(state={page:""}, action){
    if(action.type === CHANGE_PAGE){
        return {"page": action.details}
    }
    return state;
}

/**
 * Reducers nedenfor tilhører familien av spillelister
 */

 function getPlaylists(action){
    switch(action.type){
        case GET_PLAYLISTS:
        return;
        default: return;
    }
 }

 function playlistsReducer(state={isFetching: false, didInvalidate: true, playlists:[]}, action){
    switch(action.type){
        case REQUEST_PLAYLISTS:
            return Object.assign({}, state, {
                isFetching: true,
                didInvalidate: false
            })
        case RECEIVE_PLAYLISTS:
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                status: (action.details.status==='SUCCESS'? true:false),
                playlists: action.details.playlists,
                errorMessage: (action.details.message?action.details.message:'')
            })
        default:
            return state
    }
 }

 export function playlistReducer(state={playlistid: '', isFetching: false, didInvalidate: true, playlist: {}}, action){
     switch(action.type){
        case INVALIDATE_PLAYLIST:
            //Returner state for når playlist er ugyldig
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: true/*,
                playlist: {'playlistid': action.playlistid}*/
            })
        case REQUEST_PLAYLIST:
            return Object.assign({}, state, {
                isFetching: true,
                didInvalidate: true/*,
                playlist: {'playlistid': action.playlistid}*/
            })
        case RECEIVE_PLAYLIST:
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                playlist: action.details
            })        
        default: return state;
     }

 }

 export function videoDataReducer(state={
    videoid:'', 
    metadata:{}, 
    comments:[], 
    isFetching: false, 
    didInvalidate: true                
    }, action){
        switch(action.type){
            case REQUEST_VIDEO_METADATA:
            return Object.assign({}, state,{
                isFetching: true,
                didInvalidate: true
            })
            case RECEIVE_VIDEO_METADATA:
            return Object.assign({}, state,{
                isFetching: false,
                didInvalidate: false,
                metadata: action.details
            })
            case REQUEST_VIDEO_COMMENTS:
            return Object.assign({}, state,{
                isFetching: true,
                didInvalidate: true
            })
            case RECEIVE_VIDEO_COMMENTS:
            return Object.assign({}, state,{
                isFetching: false,
                didInvalidate: false,
                comments: action.details
            })
            default:
            return state
        }
    }

//Combine the reducers and export just one!
const schooltubeApp = combineReducers({
    apistatus: apiStatusReducer, 
    userstatus: userStatusReducer, 
    changedPage: changePageReducer, 
    playlistsData: playlistsReducer,/* Alle spillelistene lagres her */
    playlistData: playlistReducer, /*Når du endrer en spilleliste så lagres den her - arbeider kun med en spilleliste om gangen tror jeg */
    videoData: videoDataReducer
})

export default schooltubeApp;