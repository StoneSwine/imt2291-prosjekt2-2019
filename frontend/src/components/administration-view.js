import { LitElement, html, css } from "lit-element";
import "@polymer/paper-checkbox/paper-checkbox.js";
import "@polymer/paper-button/paper-button.js";
import "./check-teacher-request.js";

/**
 * Search for users to give admin-rights
 * @class AdministrationView
 * @extends {LitElement}
 */
class AdministrationView extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  /**
   * @readonly
   * @static
   * @memberof AdministrationView
   */
  static get properties() {
    return {
      result: {
        type: Object
      },
      message: {
        type: Object
      },
      toast: Object,
    };
  }

  /**
   * Creates an instance of AdministrationView.
   * @memberof AdministrationView
   */
  constructor() {
    super();
    this.message = "";
    this.result = false;
  }

  /**
   * @returns
   * @memberof AdministrationView
   */
  render() {
    return html`
      <h2>Search for users to give admin role</h2>
      <form onsubmit="javascript: return false;">
        <input
          type="search"
          name="search_text"
          id="search_text"
          placeholder="Enter email or name"
        />
        <input type="hidden" name="submit_search" value="1" />
        <button @click="${this.sendSearchForm}">Søk</button>
      </form>

      ${this.result
        ? html`
            ${this.result.searchResults.map(
              i => html`
                <label for=${i.userId}>Gjør til Admin</label>
                <button
                  id="${i.userId}"
                  value="${i.userId}"
                  @click="${this.setUserToAdmin}"
                >
                  ${i.firstName} ${i.lastName}</button
                ><br />
              `
            )}
          `
        : html`
            ${this.message
              ? html`
                  <p>${this.message}</p>
                `
              : html``}
          `}
      <check-teacher-request></check-teacher-request>
    `;
  }

  /**
   * @param {*} event
   * @memberof AdministrationView
   */
  sendSearchForm(event) {
    let data = new FormData(event.target.form);
    fetch(`${window.MyAppGlobals.apiURL}api/adminSearchUsers.php`, {
      method: "post",
      credentials: "include",
      body: data
    })
      .then(res => res.json())
      .then(res => {
        console.log(res);
        this.displayResults(res)
      });
  }

  /**
   * @param {*} result
   * @memberof AdministrationView
   */
  displayResults(result) {
    if (result.message) {
      this.toast.text = result.message;
      this.toast.open();
    } else {
      this.result = result;
    }
  }

  /**
   * @param {*} e
   * @memberof AdministrationView
   */
  setUserToAdmin(e) {
    fetch(
      `${window.MyAppGlobals.apiURL}api/setUserAsAdmin.php?setasadmin=` +
        e.target.value,
      {
        method: "get",
        credentials: "include"
      }
    ).then(res => res.json())
     .then(res=> {
       if(res.STATUS == "SUCCESS"){
         //remove the search results
         this.result = false;
       }//fix the toast to display the message
       this.toast.text = res.MSG;
       this.toast.open();
    });
  }
}
customElements.define("administration-view", AdministrationView);
