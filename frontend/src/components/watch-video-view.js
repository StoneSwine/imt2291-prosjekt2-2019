import { LitElement, html, css } from 'lit-element';
import '@polymer/paper-button/paper-button.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/iron-icons/av-icons.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/paper-slider/paper-slider.js';
import './video-comments-element/video-comments-element';
import store from './../store';
import {getMetadataForVideo} from './../actions/index';
//import './../shared-styles.js';

class WatchVideoView extends LitElement {

    static get properties () {
        return {
            videoid: {
                type: String,
                reflect: true
            },
            title: {
                type: String,
                reflect: true
            },
            videotitle:{
              type: String,
              reflect: true
            },
            videoDescription:{
              type: String,
              reflect: true
            },
            route:{
              type: Object
            },
            subtitle:{type: Array}, /* One subtitle split into cues/array */
            activecues: {type: Array}, /*The cues/subtitle that is "displayed on the video at the video time" */
            isPlaying: {type: Boolean},
            isFullscreen:{type: Boolean}/*Keeps track if the user is in fullscreen */

      }
    }

    static get styles(){
      return css`
      :host{
          display: block;
          height: 100%;
          margin-top: 20px;
      }
      .container{
          display: grid;
          height: auto;
          width: 100%;
          grid-template-columns: minmax(600, 1200), minmax(300,648); /* video subtitles*/
          grid-template-rows: auto;
          grid-template-areas: "video subs";
      }
      #videoandcontrols{
        grid-area: video;
        min-width: 600px;
      }
      #video-controls{
        /*position:absolute;
        bottom: 0;
	      left: 0;
	      right: 0;*/
	      padding: 5px;
        /*width:100%;
        height: auto;*/
        background-color: lightgray;
      }
 
      video{
        width: 100%;
        /*height: auto;*/
      }
      #subs{
          background: lightblue;
          grid-area: subs;
          overflow-y: hidden;
          min-width:300px;
          max-width: 648px;

      }
      #subs ul{
        list-style-type: none;
        width: 100%;
        height: 100%;
        padding: 0;
        margin: 0;
        overflow-y: auto;
        padding: 10px;
      }
      #subs li {
        padding: 3px 6px;
      }

      #subs li.active {
        background: #ddd;
      }

     `;
    }

    render() {
        return html`
              <!--START container START-->
              <div class="container">
                <div id="videoandcontrols"><!-- Video and controls container -->
                              
                    <video class="embed-responsive" preload="auto" xmlns="" controls crossorigin>
                    <source src="${window.MyAppGlobals.apiURL}api/stream.php?id=${this.videoid}" type="video/mp4">
                    <track kind="subtitles" label="English subtitles" src="${window.MyAppGlobals.apiURL}api/getSubtitle.php?id=${this.videoid}" srclang="en" default></track>
                    Your browser does not support the video tag.
                    </video><!-- END video END -->
                    <div id="video-controls"> 
                    <paper-button @click="${this.setPlaybackSpeed}" playback-speed="0.5">0.5x hastighet</paper-button><!--Slower-->
                    <paper-button @click="${this.setPlaybackSpeed}" playback-speed="1">Normal</paper-button><!--Normal-->
                    <paper-button @click="${this.setPlaybackSpeed}" playback-speed="2.0">2x hastighet</paper-button><!--Fast-->
                  </div>  
              
                </div><!-- Video and controls container -->

                
              <div id="subs"><!-- START subtitles START-->
                <!-- Subs are built using a unordered list-->
                <ul>
                ${this.subtitle.map(cue=>{
                  return html`<li data-id="${cue.id}" data-starttime="${cue.startTime}">${cue.text}</li>`;
                })}
                </ul>  
              </div>
              <!-- END subtitles END-->
            </div><!--END container END-->
            
            
            <div class="video-metadata">
              <h2>${this.videotitle}</h2>
              <br>
              <p>${this.videoDescription}</p>
              <hr>
              Here comes rating
            </div>
            <!-- Here comes comment section-->
                <video-comments-element videoid="${this.videoid}"></video-comments-element>
            `;
    }

    constructor(){
      super();
      //console.log("constructor: " + this.videoid);
      this.subtitle = [];
      this.isFullscreen = false;

      const unsubscribe = store.subscribe(()=>{

        let change = store.getState();
        // Håndtere endring i metadata
        if(change.videoData.isFetching === false && change.videoData.didInvalidate === false){
          // Fresh data kan oppdateres
          // Antageligvis ingen endring i metadata, men kan forekomme
          this.videotitle = change.videoData.metadata.title;
          this.videoDescription = change.videoData.metadata.description;

        }
      })

    }

    firstUpdated(){
      //console.log("firstUpdated - watch-video-view:  " + this.videoid);
      /*if(!this.videotitle){
        // If the title doesn't exists then we must fetch it. 
        // This is encountered when link sharing.
        this.getVideoMetadata();

      }*/

      const track = this.shadowRoot.querySelector('video track'); // Subs are included in the video tag and track is there
      track.addEventListener('load', (e)=>this.cueDidLoad(e));
      this.shadowRoot.querySelector('ul').addEventListener('click', (e)=>this.userClickedOnCue(e));

      /* Listener for fullscree */
      this.shadowRoot.querySelector('video').addEventListener('webkitfullscreenchange', this.userEnteredFullscreen)
      this.shadowRoot.querySelector('video').addEventListener('mozfullscreenchange', this.userEnteredFullscreen)
      this.shadowRoot.querySelector('video').addEventListener('fullscreenchange', this.userEnteredFullscreen)

    }

  

    cueDidLoad(e){
      console.log(e);
      this.subtitle = []; //Empty array

      if(e.path){
        var trackCues = e.path[0].track.cues; // Chromium based
      }else{
         var trackCues = e.target.track.cues; //Safari specific
      }


      for(let i=0; i<trackCues.length; i++){
        this.subtitle.push(
          {
          text: trackCues[i].text,
          id: trackCues[i].id,
          startTime: trackCues[i].startTime
        }
        );
      }
      //console.log(this.subtitle);
      //console.log(this.shadowRoot.querySelector('video').textTracks[0]);
      this.shadowRoot.querySelector('video').textTracks[0].mode = 'hidden';
      //console.log(this.shadowRoot.querySelector('video track'))
      this.shadowRoot.querySelector('video').textTracks[0].addEventListener('cuechange', (e)=>this.cueDidChange(e));
    }

    /**
     * Få tak i de aktive "cues" eller undertekstene for at disse skal kunne utheves på høyresiden
     * @param {*} e 
     */
    cueDidChange(e){
      //console.log("cueDidChange");
      //console.log(e.target.activeCues);
      const startTimes = [];
      for(let i=0; i<e.target.activeCues.length;i++){
        startTimes.push(e.target.activeCues[i].startTime);
      }
      //console.log("startTimes: " + startTimes);
      this.activecues = startTimes;
    }

    /**
     * Sørger for at endringer i for eksempel videoid blir reflektert over i metadataen.
     * 
     * @param {*} changedProperties 
     */
    updated(changedProperties){
      //console.log(changedProperties);
      
      store.dispatch(getMetadataForVideo(this.videoid)); // Alltid se etter fresh metadata

      changedProperties.forEach((oldValue, propName) => {
        if(propName === "videoid" && oldValue !== this.videoid){
          //Video som skal strømmes endra, hente ny
          this.shadowRoot.querySelector('video').load();
        }
        
        if(propName == "activecues")
        {
          // First remove the active list elements
          this.shadowRoot.querySelectorAll('li').forEach(listElement=>{
            listElement.classList.remove('active');
          })
          //Apply class active for the new active li element
          this.activecues.forEach(startTime=>{
             this.shadowRoot.querySelector(`[data-starttime="${startTime}"]`).classList.add('active');
          
            /* Center the cue in the middle */
            const li = this.shadowRoot.querySelector(`[data-starttime="${startTime}"]`);
            const ul = li.parentElement;
            const heightofUl = ul.clientHeight;
            const top = li.offsetTop - ul.offsetTop;
            if(top<heightofUl/2){
              ul.scrollTop = 0;
            }else{
              ul.scrollTop = top - heightofUl/2;
            }
          })
        }
      });
    }

  userClickedOnCue(event){
    if(event.path[0].tagName=='LI'){
      //console.log(event.path[0].dataset);
      this.shadowRoot.querySelector('video').currentTime = event.path[0].dataset.starttime;
    }
  }



  /**
   * Change the playback speed of the video
   * 
   * @param {*} speed - Hastigheten for playback  
   */
  setPlaybackSpeed(event){
    //The speed is set using a dropdown with available speeds; 0,5x 2x, 4x
    console.log(event);
    console.log(event.target.getAttribute("playback-speed"));
    let playbackSpeed = event.target.getAttribute("playback-speed");
    const videoPlayer = this.shadowRoot.querySelector('video');
    videoPlayer.playbackRate = playbackSpeed;
  }


  userEnteredFullscreen(event){
    console.log("User entered fullscreen");
    //console.log(this);
    // At this point 'this' refers to the video element 🤷‍♂️
    if(this.isFullscreen){
      this.textTracks[0].mode ='hidden';
      this.isFullscreen = false;
    }else{
      this.textTracks[0].mode ='showing';
      this.isFullscreen = true;
    }
    

  }

}




customElements.define('watch-video-view', WatchVideoView);