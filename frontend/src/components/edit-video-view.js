import { LitElement, html, css } from 'lit-element';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-input/paper-textarea.js';
import '@polymer/paper-dropdown-menu/paper-dropdown-menu.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/paper-listbox/paper-listbox.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/iron-icons/iron-icons.js';
import './video-metadata-element.js';
/**
 * A view to make editing of video metadata intuitive.
 */
class EditVideoView extends LitElement {

    static get properties(){
        return {
            videoid:{
                type: String,
                reflect: true
            }
        }
    }
    static get styles(){
        return css`
            :host{
                width:100%;
            }
            #edit-video{
                padding-left: 50px;
                padding-right: 50px;
                padding-bottom: 50px;
                background: whitesmoke;
            }
        `;
    }
    render() {
        return html`
        <h1>Gjør endringer på video med id ${this.videoid}</h1>
        <hr>
        <video-metadata-element videoid="${this.videoid}"></video-metadata-element>
        `;
    }

    constructor(){
        super();

    }

    firstUpdated(){

    }

}
customElements.define('edit-video-view', EditVideoView);