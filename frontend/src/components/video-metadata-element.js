import { LitElement, html, css } from 'lit-element';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-input/paper-textarea.js';
import '@polymer/paper-dropdown-menu/paper-dropdown-menu.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/paper-listbox/paper-listbox.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/iron-icons/iron-icons.js';
import {ifDefined} from 'lit-html/directives/if-defined';
import store from './../store';
import { getPlaylists } from '../actions';
//import {} from './../actions/index';
/**
 * A universal element for creating, updating and viewing metadata about a video
 * 
 * If a videoid is present the html elements have the placeholder filled in with the original values. Meaning that when the 
 * user edits one of the properties we can know what properties that changed because they will be the only one
 * with value in them. 
 * 
 * If no video element blank values are displayed.
 */
class VideoMetadataElement extends LitElement {

    static get properties(){
        return {
            videoid:{
                type: String,
                reflect: true,
            },
            allowediting:{ /* When set to true the video metadata is can be changed in UI */
                type: String, /* TODO: Actually make html tags disabled */
                reflect: true
            },
            videotitle:{
                type: String,
                reflect: true
            },
            videoDescription:{
                type: String,
                reflect: true
            },
            courses:{
                type: Array,
                reflect: true
            },
            playlists:{
                type: Array,
                reflect: true
            },
            tags:{
                type: Array,
                reflect: true
            },
            actionName:{
                type: String,
                reflect: true
            },
            userid:{
                type: String,
                reflect: true
            },
            videoThumbnailURL: {
                type: String,
                reflect: true
            },
            thumbnailBlob:{
                type: Blob
            },
            toast: Object,
            errorMessage:{
                type: String,
                reflect: true
            }
        };
    }

    static get styles(){
        return css`
        :host{
            width: 100%;
        }
        #metadata-container{
            padding-left: 50px;
            padding-right: 50px;
            padding-bottom: 50px;
            background: whitesmoke;
        }
        button {
            -webkit-appearance: none;
            border: 0;
            padding: 0;
            background: transparent;
          }
          .column{
              flex: 50%;
          }
          .row{
              display: flex;
          }
          @media screen and (max-width: 600px) {
            .column {
              width: 100%;
            }
        }
        video{
            width: 50%;
        }
         
        `;
    }
    // ${((true) ? html`placeholder="${this.videotitle}" `: html`value="${this.videotitle}"`)}
    render() {
        return html`
            <div id="metadata-container">
                <paper-input id="titleInput" name="title" label="Video tittel" 
                value="${(this.videoid ? '' : this.videotitle)}" 
                placeholder="${(this.videoid ? this.videotitle : '')}"> 
            </paper-input>   <!-- Video title-->
            <paper-textarea id="descriptionInput" name="description" label="Beskrivelse"
                value="${(this.videoid ? '' : this.videoDescription)}" 
                placeholder="${(this.videoid ? this.videoDescription : '')}">
            </paper-textarea><!--Video description-->
            <paper-dropdown-menu id="courseDropdown" name="course" label="Velg emne"><!-- Dropdown with course -->
                <paper-listbox slot="dropdown-content" class="dropdown-content">
                ${this.courses.map(course => html`
                <paper-item
                    selected="${ifDefined(course.selected)}"
                    value="${course.id}">
                ${course.name}
                </paper-item>
                `)}
                <paper-item value="none">None</paper-item>
                </paper-listbox>
            </paper-dropdown-menu> <!--Course dropdown-->
            <paper-dropdown-menu id="playlistDropdown" name="playlist" label="Legg i spilleliste"><!-- Dropdown with playlists -->
                <paper-listbox slot="dropdown-content" class="dropdown-content">
                ${this.playlists.map(playlist=> html`
                <paper-item 
                selected="${ifDefined(playlist.selected)}"
                value="${playlist.id}">${playlist.title}</paper-item>
                `)}
                <paper-item value="none">None</paper-item>
                </paper-listbox>
            </paper-dropdown-menu><!--Playlist dropwdown-->
            <paper-input id= "tagsInput" name="tags" label="Legg til emneknagger på video(kommaseparert)"
            value="${(this.videoid ? '' : this.tags)}" 
            placeholder="${(this.videoid ? this.tags : '')}">
            </paper-input>
            <!-- På venstre side er input fileer og høyre minibildet-->
            <div class="row">
                <div class="column">
                <paper-input 
                    id="videoFile" 
                    type="file" 
                    name="video" 
                    label="Velg en video"></paper-input><!--Video file input-->

                <!--If there is a video we can put it below-->
                <paper-input id="subtitleFile" type="file" name="subtitles" label="Legg til en WebVTT fil for teksting av video"></paper-input><!--Subtitles file input-->
                <paper-input id="thumbnailFile" type="file" name="thumbnail" label="Legg til et minibilde"></paper-input><!--Thumbnail file input-->
                <!--Updating video-->
                <button  tabindex="-1" @click="${this.submit}">
                    <paper-button id="submitNewVideo" raised><iron-icon icon="icons:file-upload"></iron-icon>${this.actionName}</paper-button>
                </button>
                </div>
                <div class="column">
                    <video></video>
                    ${(this.videoThumbnailURL ? html`<img width="640" height="360" src="${this.videoThumbnailURL}">`: html`<img width="640" height="360" id="previewThumbnail">`)}
                </div>
            </div>
           
            
            </div>
        `;
    }

    constructor(){
        super();
        this.videotitle = "";
        this.videoDescription = "";
        this.courses = [];
        this.playlists = [];
        this.tags = [];

        const unsubscribe = store.subscribe(()=>{
            console.log("video-metadata-element subscribes to store");
            let change = store.getState();
            
            this.updateDisplayedPlaylists(change.playlistsData);
            this.performUpdate();
        });

        
        
    }
    /**
     * Hvis
     */
    firstUpdated(){
        if(this.videoid){
            this.getMetadata();
            this.videoThumbnailURL = `${window.MyAppGlobals.apiURL}api/getThumbnail.php?videoid=${this.videoid}`;
            // Make changes for a update of video
            this.actionName = "Lagre endringer";
        }else{
            // Make changes for a new video
            this.actionName = "Ny video";
        }
        this.getCoursesForUser();

        let change = store.getState();
        
        this.userid = change.userstatus.uuid;
        this.updateDisplayedPlaylists(change.playlistsData);

        this.shadowRoot.querySelector('#videoFile').addEventListener('input',(event)=>{
            console.log("Via event listener");
            this.newInputVideo(event);
        })

        this.shadowRoot.querySelector('#thumbnailFile').addEventListener('input',(event)=>{
            console.log("Via event listener");
            const thumbnail = (this.shadowRoot.querySelector("#thumbnailFile")).inputElement.inputElement.files[0];
            var img = this.shadowRoot.querySelector('#previewThumbnail');
            const reader = new FileReader();
            reader.onload = (function(aImg) { return function(e) { aImg.src = e.target.result; }; })(img);
            reader.readAsDataURL(thumbnail);
            //img.src = (this.shadowRoot.querySelector("#thumbnailFile")).inputElement.inputElement.files[0];
        })
    }

    updateDisplayedPlaylists(playlistsData){
        //console.log(playlistsData);
        if(playlistsData.didInvalidate){store.dispatch(getPlaylists());}

        playlistsData.playlists.forEach(playlist => {
            //console.log(playlist);
            if(playlist.managed_by === this.userid){
                let pl = {'id':playlist.id, 'title':playlist.title}
                this.playlists.push(pl);
            }
            
        });
    }
    /**
     * Takes filled in properties and send them off to the server.
     * If no videoid is present it is assumed we are creating a new video
     * @param {*} event 
     */
    submit(event){
        const data = new FormData();
        if(this.videoid){
            data.append("videoid", this.videoid);
        }
        const title = (this.shadowRoot.querySelector("#titleInput")).value;
        if(title){
            data.append("title", title);
        }
        const description = (this.shadowRoot.querySelector("#descriptionInput")).value;
        if(description){
            data.append("description", description);
        }
        const tags = (this.shadowRoot.querySelector("#tagsInput")).value;
        if(tags){
            data.append("tags", tags);
        }
        const course = (this.shadowRoot.querySelector("#courseDropdown")).value;
        if(course){
            data.append("course", course);
        }
        const playlist = (this.shadowRoot.querySelector("#playlistDropdown")).value;
        if(playlist){
            data.append("playlist", playlist);
        }
        const video = (this.shadowRoot.querySelector("#videoFile")).inputElement.inputElement.files[0];
        if(video){
            data.append("video", video);
        }
        const subtitle = (this.shadowRoot.querySelector("#subtitleFile")).inputElement.inputElement.files[0];
        if(subtitle){
            data.append("subtitle", subtitle);

        }
        const thumbnail = (this.shadowRoot.querySelector("#thumbnailFile")).inputElement.inputElement.files[0];
        if(thumbnail){
            data.append("thumbnail", thumbnail);
        }else if(this.thumbnailBlob){ // Ja, det vil være mulig å input en ny video fil og bruke den som thumbnail. Selvom videoen ikke blir bytta ut.
            data.append("thumbnailBlob", this.thumbnailBlob);
        }
        //console.log("Formdata: " + title + ", " + description);
        //console.log(data);
        fetch(`${window.MyAppGlobals.apiURL}api/uploadVideo.php`,{
            method: 'POST',
            credentials: "include",
            body: data
        })
        .then(res=>res.json())
        .then(res=>{
            console.log(res);
            if(res.status === "SUCCESS"){
                console.log("File successfully uploaded");
                const videoLocation = "/watch/" + res.videoid;
                store.dispatch(changePage(videoLocation));
            }else{
                console.log("Failed uploading file");
                this.showError(res.message);
            }
        })
    }
    
    showError(msg){
        this.toast.text = msg;
        this.toast.open();
    }
    
    /**
     * Trigges ved valg av video fil
     * Spiller av fila i en video tag
     * 
     * Vil også velge et tilfeldig thumbnail som brukeren kan bruke hvis et thumbnail ikke lastes opp.
     * INSPIRERT FRA: https://codepen.io/aertmann/pen/mAVaPx
     * @param {} event 
     */
    newInputVideo(event){
        console.log(event.target);
        const video = (this.shadowRoot.querySelector("#videoFile")).inputElement.inputElement.files[0];
        //console.log(video);
        let videoelement = this.shadowRoot.querySelector('video');
        //var type = video.type
        var fileURL = URL.createObjectURL(video)

        videoelement.src = fileURL;
        videoelement.muted = true;
        videoelement.playsInline = true;
        videoelement.play();

        // Lager thumbnail
        var hiddenvideo = document.createElement('video');
        hiddenvideo.width = '640';
        hiddenvideo.height = '360';
        hiddenvideo.src = fileURL;
        hiddenvideo.preload = 'metadata';
        hiddenvideo.muted = true;
        hiddenvideo.playsInline = true;
        //hiddenvideo.currentTime = 21;

        hiddenvideo.addEventListener('loadedmetadata',()=>{
            console.log('hiddenvideo loadedmetadata');
            var seconds = hiddenvideo.duration;
            var randomtime = Math.floor(Math.random() * seconds) + 1;
            console.log("random time: " + seconds + ", " + randomtime);
            hiddenvideo.currentTime = randomtime;
        })

        hiddenvideo.addEventListener('loadeddata', ()=>{
            console.log('hiddenvideo loadeddata');
            console.log(hiddenvideo);
            //var w = hiddenvideo.getAttribute('width');
            //var h = hiddenvideo.getAttribute('height');
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d')
            ctx.drawImage(hiddenvideo, 0, 0, 640, 360); //Skummelt å anta størrelse
            var image = canvas.toDataURL('image/png');
            
            var success = true;//image.length > 100000;
            if(success){
                console.log("Canvas image success");
                var img = this.shadowRoot.querySelector('#previewThumbnail');
                img.src = image; //"https://static.independent.co.uk/s3fs-public/thumbnails/image/2017/09/12/11/naturo-monkey-selfie.jpg?w968h681";
                //console.log(image);
                this.thumbnailBlob = image.substring(image.indexOf(',') + 1);
            }
        })
        //hiddenvideo.play();
       
    }

    /**
     * When a vieoid is present we need to get the metadata for the video
     * Downloads the metadata for the video.
     * Enables the user to see what info exists for the video 
     * before changing it.
     * 
     * Takes a videoid as parameter.
     */
    getMetadata(){
        fetch(`${window.MyAppGlobals.apiURL}api/getVideoMetadata.php?videoid=${this.videoid}`,{
            method: "GET",
            credentials: "include"
          })
          .then(res=>res.json())
          .then(res=>{
            console.log(res);
            this.videotitle = res.metadata.title;
            this.videoDescription = res.metadata.description;
          })
    }

    /**
     * Course/emne er spesifikk for en bruker.
     * En foreleser har jo et emne. 
     * Videoen kan legges inn i et emne. På en måte som en spilleliste.
     */
    getCoursesForUser(){
        fetch(`${window.MyAppGlobals.apiURL}api/getMyCourses.php`,{
            method: 'GET',
            credentials: "include"
        })
        .then(res=>res.json())
        .then(res=>{
            if(res.STATUS == "SUCCESS"){
                console.log(res.courses);
                this.courses = res.courses;
            }else{
                this.showError("Could not get your courses");
            }
           
        })
    }


    /**
     * Hent spillelister som brukeren har lagd.
     * Brukeren kan velge ved opplasting hvilken spilleliste videoen skal høre til.
     */
    /*getPlaylistsForUser(){
        fetch(`${window.MyAppGlobals.apiURL}api/getMyPlaylists.php`,{
            method: 'GET',
            credentials: "include"
        })
        .then(res=>res.json())
        .then(res=>{
            if(res.STATUS == "SUCCESS"){
                console.log(res.playlists);
                this.playlists = res.ownedplaylists;
            }else{
                this.showError("Could not get your playlists");
            }
        })
    }*/
    showError(msg){
        /*TODO: Finn en måte å få toast til å fungere her */
        console.log(msg);
        //this.toast.text = msg;
        //this.toast.open();
    }
}
customElements.define('video-metadata-element', VideoMetadataElement);