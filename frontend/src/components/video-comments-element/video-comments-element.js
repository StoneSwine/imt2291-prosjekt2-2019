import { LitElement, html, css } from 'lit-element';

import store from './../../store';
import {getCommentsForVideo, deleteCommentOnVideo, addCommentOnVideo} from './../../actions/index';
class VideoCommentsElement extends LitElement {

    static get properties(){
        return {
            videoid:{
                type: String,
                reflect: true
            },
            comments:{
                type: Array,
                reflect: true
            },
            isAdmin:{
                type: Boolean,
                reflect: true
            }
        };
    }

    static get styles(){
        return css`
            :host{
                display: block;
                width: 100%;
            }
        `;
    }

    
    render() {
        return html`
        <h2>Kommentarer</h2>
        <hr>
        <p>velkommen til kommentarfeltet. Vær snille da.</p>
        <hr>
        <input type="textfield" id="commentInput">
        <button @click="${this.addComment}">Send</button>
        <hr>
        ${this.comments.map(comment=>{
            return html`
            >> ${comment.firstname} - ${comment.dTime} <br>
                       > ${comment.comment} <br>
            ${(this.isAdmin ? html`<button @click="${this.deleteComment}" commentid="${comment.id}">Slett kommentar</button>`:html``)}
            <hr>`
        })}

        
        
        `;
    }

    constructor(){
        super();
        //this.comments = [];
        let data = store.getState();
        this.isAdmin = data.userstatus.isAdmin;
        if(!data.videoData.isInvalid){
            this.comments = data.videoData.comments;
        }
        

        const unsubscribe = store.subscribe(()=>{
            let change = store.getState();
            console.log(change);

            if(change.videoData.didInvalidate === true){
                this.comments = [];
            }

            if(change.videoData.isFetching === false && change.videoData.didInvalidate === false){
                // Fresh data kan oppdateres                
                this.comments = change.videoData.comments;
                
            }
            this.isAdmin = data.userstatus.isAdmin;
            //this.performUpdate();
        })
    }

    firstUpdated(){
        //store.dispatch(getCommentsForVideo(this.videoid));
    }

    updated(changedProperties){


       
       changedProperties.forEach((oldValue, propName) => {
            if(propName == 'videoid' && oldValue !== this.videoid){
                store.dispatch(getCommentsForVideo(this.videoid));
            }
       });

    }
    addComment(event){
        console.log("Addcomment");
        let comment = (this.shadowRoot.querySelector('#commentInput')).value
        if(comment){
            store.dispatch(addCommentOnVideo(this.videoid, comment));
        }
       
    }

    deleteComment(event){
        //console.log(event.target.getAttribute('commentid'));
        const commentid = event.target.getAttribute('commentid');
        store.dispatch(deleteCommentOnVideo(this.videoid, commentid));
    }

}
customElements.define('video-comments-element', VideoCommentsElement);