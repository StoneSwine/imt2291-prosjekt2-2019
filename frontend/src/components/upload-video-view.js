import { LitElement, html, css } from 'lit-element';
import '@polymer/paper-input/paper-input.js';
import '@polymer/paper-input/paper-textarea.js';
import '@polymer/paper-dropdown-menu/paper-dropdown-menu.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/paper-listbox/paper-listbox.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/iron-icons/iron-icons.js';
import '@polymer/iron-form/iron-form.js';
import '@polymer/paper-styles/typography.js';
import './video-metadata-element';
import store from './../store.js';
import { changePage } from '../actions/index.js';

class UploadVideoView extends LitElement {

    static get properties(){
        return {
            title:{
                type: String,
                reflect: true
            },
            courses:{
                type: Array,
                reflect: true
            },
            playlists:{
                type: Array,
                reflect: true
            },
            toast: Object,
            errorMessage:{
                type: String,
                reflect: true
            }
        };
    }

    static get styles(){
        return css`
            :host{
                width: 100%;
            }
            #upload-video{
                padding-left: 50px;
                padding-right: 50px;
                padding-bottom: 50px;
                background: whitesmoke;
            }
            button {
                -webkit-appearance: none;
                border: 0;
                padding: 0;
                background: transparent;
              }
              input {
                -webkit-appearance: none;
                border: 0;
                padding: 0;
                background: transparent;
              }
        `;
    }

    render() {
        return html`
            <h1>${this.title}</h1>
            <hr>
            <br>
            <video-metadata-element></video-metadata-element>`;
    }
    constructor(){
        super();
        this.title = "Last opp video";
        this.courses = [];
        this.playlists = [];
        this.errorMessage = "Hello world";
    }

    firstUpdated(){
        
    }

  

    showError(msg){
        this.toast.text = msg;
        this.toast.open();
    }
}
customElements.define('upload-video-view', UploadVideoView);