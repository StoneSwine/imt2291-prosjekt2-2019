import { LitElement, html, css } from "lit-element";
import "./video-teaser-element/video-teaser-element.js";
import "./playlist-teaser-element.js";
/**
 * This element searches for videoes and displays them:
 * TODO: add bootstrap to this
 * @class SearchResultView
 * @extends {LitElement}
 */
class SearchResultView extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
      }
      .container {
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: center;
        height: auto;
        padding: 5px;
      }
    `;
  }

  /**
   * @readonly
   * @static
   * @memberof SearchResultView
   */
  static get properties() {
    return {
      searchquery: {
        type: String
      },
      result: {
        type: String
      },
      toast: Object
    };
  }

  /**
   *Creates an instance of SearchResultView and initialize variables
   * @memberof SearchResultView
   */
  constructor() {
    super();
    this.searchquery = "";
    this.result = {};
  }

  /**
   * Renderes the hmtl based on the result from the searching
   * @returns
   * @memberof SearchResultView
   */
  render() {
    return html`
      <input
        placeholder="Søk"
        type="text"
        .value="${this.searchquery}"
        @input="${this.handleInput}"
        @keypress="${this.handleKeyPress}"
      />
      <button @click="${this.getSearchResults}">Søk</button>

      ${this.result.searchresults
        ? html`
            <div class="container">
              ${this.result.searchresults.map(
                i => html`
                  ${i.playlist
                    ? html`
                        <playlist-teaser-element
                          playlistId="${i.id}"
                          playlistTitle="${i.title}"
                          playlistDescription=${i.description}
                        >
                        </playlist-teaser-element>
                      `
                    : html`
                        <video-teaser-element
                          videoid="${i.uuid}"
                          videotitle="${i.title}"
                          videodescription=${i.description}
                        >
                        </video-teaser-element>
                      `}
                `
              )}
            </div>
          `
        : html``}
    `;
  }

  /**
   * Handles search when the user presses "enter" key
   * @param {*} e
   * @memberof SearchInput
   */
  handleKeyPress(e) {
    if (e.target.value !== "") {
      if (e.key === "Enter") {
        this.getSearchResults();
      }
    }
  }

  /**
   * Saves the input typed in the search field
   * @param {*} e
   * @memberof SearchInput
   */
  handleInput(e) {
    this.searchquery = e.target.value;
  }

  /**
   * Retrieves the searchresult from the backend
   * @memberof SearchInput
   */
  getSearchResults() {
    console.log("searching for: " + this.searchquery);
    let searchquery = encodeURI(this.searchquery);
    fetch(`${window.MyAppGlobals.apiURL}api/search.php?query=` + searchquery, {
      method: "GET",
      credentials: "include"
    })
      .then(res => res.json())
      .then(res => {
        console.log(res);
        if (res.message) {
          this.toast.text = res.message;
          this.toast.open();
        }
        this.result = res;
      });
  }
}

customElements.define("search-result-view", SearchResultView);
