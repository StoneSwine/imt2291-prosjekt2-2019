import { LitElement, html, css } from "lit-element";

class CreateNewPlaylist extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
      cources: {
        type: Object
      }
    };
  }

  constructor() {
    super();
    this.getCources();
  }

  render() {
    return html`
      <h2>Lag ny spilleliste</h2>
      <form onsubmit="javascript: return false;">
        <input type="hidden" name="newplaylist" value="" />
        <label for="playlisttitle">Name your playlist</label>
        <input
          type="text"
          class="form-control"
          name="playlisttitle"
          id="playlisttitle"
          placeholder="Awesome lectures"
        /></br>

        <label for="playlistdescription"
          >Write something descriptive about the playlist</label
        >
        <input
          type="text"
          class="form-control"
          name="playlistdescription"
          id="playlistdescription"
          placeholder="Lectures for dissecting animals"
        /></br>

        <label for="courses" class="col-form-label">Link with course</label>
        <select class="form-control" id="courses">
          <option>None</option>
          ${this.cources ? html`
          ${this.cources.map(
            course => html`
              <option name="course">${course}</option>
            `
          )}
          ` : html``}
        </select></br>

        <input
          type="file"
          name="thumbnail"
          class="custom-file-input"
          id="customFile"
        />
        <label class="custom-file-label" for="customFile"
          >Choose a thumbnail. If none are provided we will generate one.</label
        ></br>

        <button id="newplaylist" @click="${this.sendPlaylistForm}">
          All done! Create playlist
        </button>
      </form>
    `;
  }

  sendPlaylistForm(event) {
    let data = new FormData(event.target.form);
    fetch(`${window.MyAppGlobals.apiURL}api/createPlaylist.php`, {
      method: "post",
      credentials: "include",
      body: data
    })

  }

  getCources() {
    fetch(`${window.MyAppGlobals.apiURL}api/getCources.php`, {
      method: "GET",
      credentials: "include"
    })
      .then(res => res.json())
      .then(res => {
        console.log(res);
        this.cources = res;
      });
  }
}

customElements.define("create-new-playlist", CreateNewPlaylist);
