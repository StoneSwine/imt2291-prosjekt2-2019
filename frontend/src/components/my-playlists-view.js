import { LitElement, html, css } from 'lit-element';
import "./playlist-teaser-element";
import store from './../store.js';
import {getPlaylists} from './../actions/index.js';
class MyPlaylistsView  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
      .container{
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: center;
        height: auto;
        padding: 5px;
    }
    `;
  }

  static get properties() {
    return {
        playlists: {
            type: Object
        },
        message: {
            type: String
        },
        myUserid:{
          type: String,
          reflect: true
        }
    };
  }

  constructor() {
    super();    
    this.playlists = (store.getState()).playlistsData.playlists;
    this.myUserid = (store.getState()).userstatus.uuid;

    const unsubscribe = store.subscribe(()=>{
      console.log("my-playlists-view subscribed to store");
      let changes =  store.getState();
      console.log(changes.playlistsData);
      if(changes.playlistsData.status){
        this.playlists = changes.playlistsData.playlists;
      }else{
        //Ser ut som det skjedde en feil
        console.log("Kunne ikke hente spillelister" + changes.playlistsData.errorMessage);
      }
    });

    // Fetch data
    store.dispatch(getPlaylists());
  }

  render() {
    return html`
    <h2>Mine spillelister</h2>
      ${this.message ? html`<h1>${this.message}</h1>`:html``}
      ${this.playlists ? html`
      <div class="container">
        ${this.playlists.map(playlist => html`
              ${(playlist.managed_by === this.myUserid) ? html`
              <playlist-teaser-element 
                  playlistId="${playlist.id}" 
                  playlistTitle="${playlist.title}" 
                  playlistDescription=${playlist.description}>
                </playlist-teaser-element>
              ` : html``}
                
                `)}
        </div>
        `:html``}
    `;
  }



}

customElements.define('my-playlists-view', MyPlaylistsView);