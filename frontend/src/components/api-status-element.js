import { LitElement, html, css } from 'lit-element';
import '@polymer/iron-icon/iron-icon.js';
import store from './../store.js';
import { updateAPIStatus } from '../actions/index.js';
import { API_Availability } from '../constants/action-types.js';

/**
 * Shows an icon to either red or green indicating the availability of the backend api service.
 * @extends LitElement
 */
class ApiStatusElement extends LitElement {

    static get properties (){
        return {
            apiStatus:{
                type: Boolean,
                value: false,
                reflect: true
            },
            apiAvailability:{
                type: API_Availability,
                reflect: true
            },
            statusColor:{
                type: String,
                reflect: true
            }
        };
    }

    constructor(){
        super();
        const unsubscribe = store.subscribe(()=>this.updateAPIAvailability(store.getState()));
        //this.apiAvailability = API_Availability.MAINTANANCE;
    }
    static get styles(){
        return css`
.circle {                    
    border-radius: 50%;
    width: 24px;
    height: 24px;
    display: inline-block;
}
.red {
    background: Red;
}
.orange{
    background: orange;    
}
.yellow{
    background: yellow;
}
.green{
    background: green;
}
        `;
    }

    render() {
        return html`
            <div class="circle ${this.statusColor}"></div>`;
    }

    firstUpdated(changedProperties){
        //console.log("Api status element added")
        fetch (`${window.MyAppGlobals.apiURL}api/apiStatus.php`)
        .then(res=>res.json())
        .then(res=>{
            //console.log("from server: " + JSON.stringify(res));
            store.dispatch(updateAPIStatus({isActive: res.apiStatus, apiAvailability: res.apiAvailability}))
        })
    }
    updated(changedProperties){
    }

    updateAPIAvailability(update){
        //console.log("updateAPIAvailability: " + update.apistatus.isActive + " " + update.apistatus.apiAvailability);
        //console.log(update);
        this.apiStatus = update.apistatus.isActive;
        this.apiAvailability = update.apistatus.apiAvailability;
        switch(this.apiAvailability){
            case API_Availability.MAINTANANCE:
                this.statusColor = "red";
                break;
            case API_Availability.LOW:
                this.statusColor = "orange";
                break;
            case API_Availability.MEDIUM:
                this.statusColor = "yellow";
                break;
            case API_Availability.HIGH:
                this.statusColor = "green";
        }
    }
}
customElements.define('api-status-element', ApiStatusElement);