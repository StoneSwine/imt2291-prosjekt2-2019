import { LitElement, html, css } from 'lit-element';
import './video-teaser-element/video-teaser-element.js';
class ShowAllVideosView extends LitElement {

    static get properties(){
        return {
            title:{
            type: String,
            reflect: true
            },
            videos:{
                type: Object,
                reflect: true
            }
        };
    }
    static get styles(){
        return css`
        :host{
            display: block;
            /*padding: 50px 50px;*/
            background: seashell;
        }
        .container{
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            height: auto;
        }
`;
    }

    render() {
        return html`
        <h1>${this.title}</h1>
        <div class="container"> 
        ${this.videos.map(video => html`
                <video-teaser-element 
                videoid="${video.id}" 
                videotitle="${video.title}" 
                videodescription=${video.description}>
                </video-teaser-element>
                `)}
        </div>
        `;
    }

    constructor(){
        super();
        this.title = "Her er alle videoene";
        this.videos = [];
        this.getAllVideos();
    }
    /**
     * 
     */
    firstUpdated(){

    }

    /**
     * Get all videos from api
     */
    getAllVideos(){

        fetch(`${window.MyAppGlobals.apiURL}api/getAllVideos.php`, {
            method: "GET",
            credentials: "include"
        })
        .then(res=>res.json())
        .then(res=>{
            console.log(res);
            if(res.STATUS == "SUCCESS"){
                //I think we have some videos here
                this.videos = res.allVideos;
            }else{
                //An error occurred, alert the user
            }
        })
    }
}
customElements.define('show-all-videos-view', ShowAllVideosView);