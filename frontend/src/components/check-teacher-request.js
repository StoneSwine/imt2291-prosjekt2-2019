import { LitElement, html, css } from "lit-element";

/**
 * 
 * Handle requests to be a teacher
 * TODO: legg til design + oppdattering av hvem som vil bli lærer i bakgrunnen
 * @class CheckTeacherRequest
 * @extends {LitElement}
 * 
 */
class CheckTeacherRequest extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
      result: {
        type: Object
      }
    };
  }

  constructor() {
    super();
    this.result = false;
    this.checkForTeacherRequests();
  }

  render() {
    return html`
      <h2>Check the people that are accepted as teachers</h2>

      ${this.result
        ? html`
            <form onsubmit="javascript: return false;">
              ${this.result.names.map(
                (i, index) => html`
                  <input type="hidden" name="checkbox[${i.email}]" value="0" />
                  <input
                    type="checkbox"
                    name="checkbox[${i.email}]"
                    value="1"
                    id="${index}"
                  /><label for="${index}">

                  <span>${i.firstName} ${i.lastName}</span>
                  
                  </label></br>
                `
              )}
              <input type="hidden" name="submit_checkbox" value="1" />
              <button @click="${this.sendTeacherRequestForm}">
                Send
              </button>
            </form>
          `
        : html`
            <h2 class="center">
              There are currently no requests for teacher role
            </h2>
          `}
                
          `;
  }

  sendTeacherRequestForm(event) {
    console.log(event.target);
    let data = new FormData(event.target.form);
    fetch(`${window.MyAppGlobals.apiURL}api/teacherRequests.php`, {
      method: "post",
      credentials: "include",
      body: data
    });
    this.checkForTeacherRequests();
  }

  checkForTeacherRequests() {
    fetch(
      `${window.MyAppGlobals.apiURL}api/teacherRequests.php?getteacherreq=1`,
      {
        method: "get",
        credentials: "include"
      }
    )
      .then(res => res.json())
      .then(res => {
        this.result = res
      });
  }

  getNewTeacherRequestsFromBackend() {
    
  }
}

customElements.define("check-teacher-request", CheckTeacherRequest);
