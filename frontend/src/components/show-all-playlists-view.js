import { LitElement, html, css } from 'lit-element';
import './playlist-teaser-element.js';
import store from'./../store.js';
import {getPlaylists} from './../actions/index.js';

class ShowAllPlaylistsView extends LitElement {

    static get properties(){
        return {
            title:{
                type: String,
                reflect: true
            },
            playlists:{
                type: Array,
                reflect: true
            }
        };
    }

    static get styles(){
        return css`
            :host{
                display: block;
            }
            .container{
                display: flex;
                flex-direction: row;
                flex-wrap: wrap;
                justify-content: center;
                height: auto;
            }
        `;
    }

    render() {
        return html`
        <h1>${this.title}</h1>
        <div class="container">
        ${this.playlists.map(playlist=>html`
        <playlist-teaser-element 
            issubscribedtypeelement="true"
            ?isusersubscribed="${playlist.subscribed}" 
            playlisttitle="${playlist.title}" 
            playlistdescription="${playlist.description}" 
            playlistid="${playlist.id}">
        </playlist-teaser-element>
        `)}
        </div>
        `;
    }

    /**
     * Setter opp grunnleggende
     * 1. Setter tittel på denne view
     * 2. Subscriber og håndterer endringer i relevant data
     * 3. Forespør om fresh data
     */
    constructor(){
        super();
        this.title = "Alle spillelister";
        console.log(store.getState());
        this.playlists = (store.getState()).playlistsData.playlists;

        const unsubscribe = store.subscribe(()=>{
            console.log("recent-video subscribes to store");
            let changes = store.getState();
            //console.log(changes);
            if(changes.playlistsData.status){
                this.playlists = changes.playlistsData.playlists;
            }else{
                //Ser ut som det skjedde en feil
                console.log("Kunne ikke hente spillelister" + changes.playlistsData.errorMessage);
            }
            super.performUpdate();
        });

        //fetch data
        store.dispatch(getPlaylists());        
    }
    
    firstUpdated(){

    }



}
customElements.define('show-all-playlists-view', ShowAllPlaylistsView);