import { LitElement, html, css } from 'lit-element';
import '@polymer/paper-input/paper-input';
import '@polymer/paper-checkbox/paper-checkbox';
import '@polymer/paper-button/paper-button';
import store from './../../store';
import {login, changePage} from './../../actions/index.js';

class LoginView extends LitElement {


    static get properties(){
        return{
            isLoggedIn:{
                type: Boolean,
                reflect: true
            }
        };
    }

    static get styles(){
        return css`
        #login-container{
            padding-left: 50px;
            padding-right: 50px;
            padding-bottom: 50px;
            background: whitesmoke;
        }
        `;
    }
    render() {
        return html`
        ${(this.isLoggedIn ? html`
        <h1>Du er logget inn!</h1>
        ` 
        : 
        html`
        <h1>Logg inn</h1>
        <hr>
        <div id="login-container">
            <paper-input 
                type="input"
                id="email"
                name="email"
                label="Epost">
            </paper-input>
            <paper-input 
                type="input"
                id="password"
                name="password"
                label="Passord">
            </paper-input>
            <paper-checkbox noink id="rememberMe">Husk meg</paper-checkbox>
            <paper-button @click="${this.login}">Logg inn</paper-button>
        </div>
        `)}
        
        `;
    }

    constructor(){
        super();
        this.isLoggedIn = false;
    }

    firstUpdated(){

    }

    login(event){
        console.log("Bruker forsøker logge inn");
        const data = new FormData();
        const email = (this.shadowRoot.querySelector("#email")).value;
        if(email){
            data.append("email", email);
        }
        const password = (this.shadowRoot.querySelector("#password")).value;
        if(password){
            data.append("password", password);
        } 
        const rememberMe = (this.shadowRoot.querySelector("#rememberMe")).value;
        if(rememberMe){
            data.append("rememberPassword", rememberMe);
        }
        fetch(`${window.MyAppGlobals.apiURL}api/login.php`,{
            method: 'POST',
            credentials: "include",
            body: data
        })
        .then(res=>res.json())
        .then(res=>{
            console.log(res);
            if(res.status == 'SUCCESS'){ //Login successfull
                store.dispatch(login({isLoggedIn: true, email: res.email, uuid: res.uuid, isStudent: res.isStudent, isTeacher: res.isTeacher, isAdmin: res.isAdmin}));
                //store.dispatch(changePage("/recent"));
                //console.log(res);
            }else{
                store.dispatch(login({isLoggedIn: false}));
            }
        })
    }
}
customElements.define('login-view', LoginView);