import { LitElement, html, css } from 'lit-element';
import './video-teaser-element/video-teaser-element.js';
import './playlist-teaser-element.js';
import store from './../store.js';
import {getPlaylists} from './../actions/index.js';
/**
 * Get the recent videos from the api and displays them with a pretty video-teaser-element
 */
class RecentVideosView extends LitElement {


    static get properties(){
        return {
            title:{
                type: String,
                reflect: true
            },
            topVideos:{
                type: Array,
                reflect: true
            },
            playlists:{
                type: Array,
                reflect: true
            }
        };
    }

   
    static get styles(){
        return css`:host{
    /*padding: 50px 50px;*/
    background: seashell;
}

.container{
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: center;
    height: auto;
}`;
    }

    render() {
        return html`
        <h1>${this.title}</h1><br>
        <h2>Videoer med flest stjerner</h2>
        <div class="container">
        ${this.topVideos.map(topvideo => html`
                <video-teaser-element 
                    videoid="${topvideo.id}" 
                    videotitle="${topvideo.title}" 
                    videodescription=${topvideo.description}>
                </video-teaser-element>
                `)}
        </div>
        <hr>
        <h2>Spillelister du abonnerer på</h2>
        <div class="container">
        ${this.playlists.map(playlist => html`
            ${((playlist.subscribed === true) ? 
            html`
            <playlist-teaser-element 
                issubscribedtypeelement="true"
                ?isusersubscribed="${playlist.subscribed}" 
                playlisttitle="${playlist.title}" 
                playlistdescription="${playlist.description}" 
                playlistid="${playlist.id}">
            </playlist-teaser-element>
            ` 
            : 
            html``)}
        </div>
        `)}
`;
    }
    constructor(){
        super();
        this.title = "Siste";
        this.topVideos = [];
        this.playlists = [];

        const unsubscribe = store.subscribe(()=>{
            console.log("recent-video subscribes to store");
            let changes = store.getState();
            //console.log(changes);
            if(changes.playlistsData.status){
                this.playlists = changes.playlistsData.playlists;
              }else{
                //Ser ut som det skjedde en feil
                console.log("Kunne ikke hente spillelister" + changes.playlistsData.errorMessage);
              }
            super.performUpdate();
        });
    
         //fetch data
         store.dispatch(getPlaylists());  
    }

    firstUpdated(){        
        this.getRecentVideos();
    }

    getRecentVideos(){
        fetch(`${window.MyAppGlobals.apiURL}api/getTopVideos.php`,{
            method: 'GET',
            credentials: "include",
        }).then(res=>res.json())
        .then(res=>{
            //console.log(res);
            this.topVideos = res.topVideos;
            //console.log(this.topVideos);
        })
    }


}
customElements.define('recent-videos-view', RecentVideosView);