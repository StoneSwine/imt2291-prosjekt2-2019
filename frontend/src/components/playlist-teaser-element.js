import { LitElement, html, css } from "lit-element";
import "@polymer/paper-card/paper-card.js";
import "@polymer/paper-button/paper-button.js";
import store from './../store.js';
import { toggleSubscription } from '../actions/index.js';

/**
 * Et element som viser en spilleliste.
 * To versjoner:
 * 1. Subscribed spillelister
 *  - Gir mulighet for å klikke for å se videoer i spilleliste.
 *  - Gir mulighet for unsubscribe
 *  - Gir mulighet for subscribe
 * 2. Mine spillelister
 *  - Gir mulighet for å klikke for å redigere spilleliste
 *  
 */
class PlaylistTeaserElement extends LitElement {
  static get styles() {
    return css`
    :host{
        display: block;
        width: 250px;
        height: auto;
        margin: auto;
    }
    `;
  }

  static get properties() {
    return {
        playlistTitle: {
            type: String,
            reflected: true
        },
        playlistDescription: {
            type: String, 
            reflected: true
        },
        playlistid: {
            type: String,
            reflected: true
        },
        playlistThumbnailURL: {
            type: String,
            reflected: true
        },
        isusersubscribed:{
          type: Boolean,
          reflected: true
        },
        isSubscribedTypeElement:{
          type: Boolean,
          reflected: true
        }
    };
  }

  static get styles(){
    return css`
    :host{
      display: block;
      width: 250px;
      height: auto;
      margin: auto;
  }
    button {
      -webkit-appearance: none;
      border: 0;
      padding: 0;
      background: transparent;
    }
    `;
  }
  constructor() {
    super();
    this.isSubscribedTypeElement = false;
    this.isusersubscribed = false;

  }

  firstUpdated(){
    this.playlistThumbnailURL = `${window.MyAppGlobals.apiURL}/api/getThumbnail.php?playlistid=${this.playlistid}`;
  }

  render() {
    return html`
      <paper-card
        image="${this.playlistThumbnailURL}">
        <h3>${this.playlistTitle}</h3>
        <div class="card-content">
          <p class="card-text">${this.playlistDescription}</p>
        </div>
        <div class="card-actions">
          ${(this.isSubscribedTypeElement ? html`
          <!-- User has options for subscribe/unsubscribe and watch playlist-->
          <a href="${window.MyAppGlobals.rootPath}watchPlaylist/${this.playlistid}">
            <paper-button>Se videoer</paper-button>
          </a>
          <button tabindex="-1" @click="${this.toggleSubscription}">
            <paper-button>${this.isusersubscribed ? 'Unsubscribe' : 'Subscribe'}</paper-button>
          </button>
          `: html`
          <!-- User has options for editing playlist -->
          <a href="${window.MyAppGlobals.rootPath}editPlaylist/${this.playlistid}">
            <paper-button>Rediger spilleliste</paper-button>
          </a>
          `)}
        </div>
      </paper-card>
    `;
  }

  /**
   * Change the state of the subscription
   */
  toggleSubscription(){
    store.dispatch(toggleSubscription(this.playlistid));
  }

}

customElements.define("playlist-teaser-element", PlaylistTeaserElement);
