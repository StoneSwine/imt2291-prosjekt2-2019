import { LitElement, html, css } from 'lit-element';
import '@polymer/paper-card/paper-card.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/paper-dropdown-menu/paper-dropdown-menu.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/paper-listbox/paper-listbox.js';
import store from './../../store';
import {removeVideoInPlaylist, updatePositionForVideoInPlaylist} from './../../actions/index';
/**
 * Use <video-teaser-element videoid="123456789"> to initialize this element correctly
 * set the showEditButton to true to enable the user to edit the video.
 * That should be dome by ssomething like showEditButton="${((this.userid === video.owned_by) ? 'true':'false')}"
 * 
 * ------------------
 * Når elementet brukes i redigeringsmodus til spilleliste sett variabler til sann/true for å ha med disse tjenestene
 * deleteVideoFromPlaylistFeature=Boolean
 * changePositionForVideoInPlaylistFeature=Boolean
 * 
 */

class VideoTeaserElement extends LitElement {

    static get properties(){
        return {
            videotitle:{
                type: String,
                reflect: true
            },
            videodescription:{
                type: String,
                reflect: true
            },
            videoOwner:{
                type: String,
                reflect: true
            },
            videoid: {
                type:String,
                reflect: true
            },
            videoThumbnailURL: {
                type: String,
                reflect: true
            },
            videoThumbnailAlt: {
                type: String
            },
            showEditButton:{
                type: Boolean,
                reflect: true
            },/** Sett features til sann for å aktivere de */
            deleteVideoFromPlaylistFeature:{
                type: Boolean,
                reflect: true
            },
            changePositionForVideoInPlaylistFeature:{
                type: Boolean,
                reflect: true
            },
            positionForVideoInPlaylist:{
                type: Number,
                reflect: true
            },
            positionForVideoInPlaylistTop:{
                type: Number,
                reflect: true
            },
            playlistId:{
                type: String,
                reflect: true
            }


        };
    }

    static get styles(){
        return css`
            :host{
                display: block;
                width: 480px;/*250px;*/
                height: auto;
                margin: auto;
            }
            paper-button.warning{
                background-color: red;
            }
        `;
    }

    render() {
        return html`
            <paper-card image="${this.videoThumbnailURL}" alt="${this.videoThumbnailAlt}">
            <h3>${this.videotitle}</h3>
                <div class="card-content">
                    <p class="card-text">${this.videodescription}</p>
                </div>
                <div class="card-actions">
                <a href="${window.MyAppGlobals.rootPath}watch/${this.videoid}">
                        <paper-button raised @click="${this.clickWatchvideo}">Se video</paper-button>
                </a>
                    ${(this.showEditButton ? html`
                    <a href="${window.MyAppGlobals.rootPath}editVideo/${this.videoid}">
                    <paper-button raised>Rediger</paper-button>
                    </a>`
                    : null)}
                </div>
                <div class="card-actions">
                ${(this.deleteVideoFromPlaylistFeature ? html`
                    <paper-button
                    raised
                    class="warning"
                    @click="${this.removeVideoFromPlaylist}"
                    >Fjern video fra spilleliste</paper-button>
                `:html``)}
                ${(this.changePositionForVideoInPlaylistFeature ? html`
                <paper-dropdown-menu 
                    label="Endre rekkefølge"
                    id="changePositionDropdown"
                    @iron-select="${(e)=>this.changePositionForVideoInPlaylist(e)}">
                    <paper-listbox 
                        slot="dropdown-content" 
                        class="dropdown-content" 
                        selected="${this.positionForVideoInPlaylist-1}">
                    ${this.getPositionListboxes()}
                    </paper-listbox>
                </paper-dropdown-menu>
                `: html``)}
                </div>
            </paper-card>
        `;
    }

constructor(){
    super();
    //this.videoThumbnailURL = `${window.MyAppGlobals.apiURL}/api/getThumbnail.php?videoid=${this.videoid}`;
    //console.log("constr " + this.videoid)
    //this.positionForVideoInPlaylistTop = 0;
}


firstUpdated(){
    this.videoThumbnailURL = `${window.MyAppGlobals.apiURL}api/getThumbnail.php?videoid=${this.videoid}`;
    //console.log("Position: " + this.positionForVideoInPlaylist + ", top: " + this.positionForVideoInPlaylistTop);
}



removeVideoFromPlaylist(){
    store.dispatch(removeVideoInPlaylist(this.playlistId))
}

/**
 * Endrer posisjonen på video i spilleliste hvis nedtrekkslista sin valgte verdi endrer seg.
 * Denne trigges når elementet lages, så viktig å sjekke om selected har endra seg fra originalen-
 * @param {*} event 
 */
changePositionForVideoInPlaylist(event){
    console.log(event.target.selected); // Gir nummeret til paper-item, den starter å telle på 0. dvs 0=1, 1=2, 2=3
    console.log(this.positionForVideoInPlaylist);
    const selectedId = parseInt(event.target.selected);

    // Hvis posisjonen har endra seg så må vi handle 
    if((selectedId+1) !== this.positionForVideoInPlaylist){
        store.dispatch(updatePositionForVideoInPlaylist(this.playlistId, this.videoid, selectedId+1));
    }
}

getPositionListboxes(){
    const items = [];
    var i;
    for(i = 1; i <= this.positionForVideoInPlaylistTop; i++){
    items.push(html`<paper-item>${i}</paper-item>`)        
    }
    //console.log(listboxes);
    return items;
}


clickWatchvideo(event){
    console.log("clickWatchvideo");

}

}
customElements.define('video-teaser-element', VideoTeaserElement);