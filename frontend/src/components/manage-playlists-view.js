import { LitElement, html } from 'lit-element';
import "./create-new-playlist.js";
import "./my-playlists-view.js";

class ManagePlaylistsView extends LitElement {

    static get properties(){
        return{
            title:{
                type: String,
                reflect: true
            }
        };
    }
    render() {
        return html`
            <create-new-playlist></create-new-playlist>
            </br>
            <hr>
            <my-playlists-view></my-playlists-view>
        `;
    }
    constructor(){
        super();
        this.title = "Mine spillelister!";
    }
}
customElements.define('manage-playlists-view', ManagePlaylistsView);