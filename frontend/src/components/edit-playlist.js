import { LitElement, html, css } from 'lit-element';
import './video-teaser-element/video-teaser-element';
import store from './../store';
import {getPlaylist, updatePositionForVideoInPlaylist, removeVideoInPlaylist} from './../actions/index';

class EditPlaylist  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        playlistid: {
            type: String,
            reflect: true
        },
        playlistmetadata: {
            type: Object,
            reflect: true
        }
    };
  }
  static get styles(){
    return css`
    :host{
      display: block;
      padding: 50px 50px;
      
  }
    .container{
      display: flex;
      flex-direction: row;
      flex-wrap: wrap;
      justify-content: center;
      height: auto;
  }
    `;
  }

  constructor() {
    super();
      
      const unsubscribe = store.subscribe(()=>{
        console.log("edit-playlist started subscribing to store");
        let changes = store.getState();
        console.log(changes);
        if(changes.playlistData.didInvalidate === false && changes.playlistData.isFetching === false){
          // Dataen er gyldig og vi burde oppdatere
          this.playlistmetadata = changes.playlistData.playlist;
          console.log("Spilleliste data endra seg!!");
        }
        //TODO: Håndtere de andre tilfellene av spilleliste actions

        super.performUpdate();
      });

     

  }
  
  firstUpdated(){
     // Fetch data
     store.dispatch(getPlaylist(this.playlistid)); //Er avhengig av spillelisteid og MÅ være her!!!
  }


  render() {
    return html`${this.playlistmetadata ? 
      html`
        <h2>Rediger spillelisten</h2>
        <!-- Form hvor man kan endre tittel og beskrivelse-->
        <form onsubmit="javascript: return false;">
            <input type="text" name="playlistid"  value="${this.playlistid}" hidden>
            <input type="text" name="updatePlaylist" value=1 hidden>
            <label for="playlisttitle">Title </label>
            <input type="text" name="playlisttitle" class="form-control" value="${this.playlistmetadata.title}"><br>
            <label for="description">Description</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description">${this.playlistmetadata.description}</textarea><br>
            <button name="updatePlaylist" @click="${this.sendEditPlaylistForm}">Submit changes</button>
        </form>

        <hr>

        <h2>Forandre rekkefølgen på videoer i spillelisten</h2>
        <!-- Forandre rekkefølgen på alle videoene i spillelisten-->
        <div class="container">
        ${this.playlistmetadata.videos.map(video => html`
          <video-teaser-element
          videoid="${video.id}"
          videotitle="${video.title}"
          videodescription="${video.description}"
          videoowner="${video.owner}"
          ?showeditbutton="${true}"
          ?deleteVideoFromPlaylistFeature="${true}"
          ?changePositionForVideoInPlaylistFeature="${true}"
          positionForVideoInPlaylist="${video.position}"
          positionForVideoInPlaylistTop="${this.playlistmetadata.videos.length}"
          playlistId="${this.playlistid}"
          ></video-teaser-element>
          `)}
        </div>
      `:
      `<!--Ingen playlistmedata-->`}`;
  }

  sendEditPlaylistForm(event){
    let data = new FormData(event.target.form);
    console.log(event);
    fetch(`${window.MyAppGlobals.apiURL}api/updatePlaylist.php`, {
      method: "POST",
      credentials: "include",
      body: data
    })
      .then(res => res.json())
      .then(res => {
        console.log(res);
      });
  }
}

customElements.define('edit-playlist', EditPlaylist);