import { LitElement, html, css } from 'lit-element';
import './video-teaser-element/video-teaser-element';
import store from './../store.js';


class MyVideosView extends LitElement {

    static get properties(){
        return {
            title:{
                type: String,
                reflect: true
            },
            myVideos:{
                type: Array,
                reflect: true
            },
            userid:{
                type: String,
                reflect: true
            }
        };
    }
    static get styles(){
        return css`
        :host{
            display: block;
            padding: 50px 50px;
        }
        .container{
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            height: auto;
        }
        `;
    }
    render() {
        return html`
            <h1>${this.title}</h1>
            <hr>
            <div class="container">
            ${this.myVideos.map(video => 
                html`<video-teaser-element 
                    videoid="${video.id}" 
                    videotitle="${video.title}" 
                    videodescription="${video.description}" 
                    videoOwner="${video.owned_by}"
                    showEditButton="${((this.userid === video.owned_by) ? 'true':'false')}">
                    </video-teaser-element>`
            )}
        </div>`;
    }
    constructor(){
        super();
        this.title = "Mine videoer";
        this.myVideos = [];
        /*Find out who the user is so we can display a edit button on the video*/
        

    }
    firstUpdated(){
        let state = store.getState();
        this.myUUID = state.userstatus.uuid;
        this.getMyVideos();
    }

    getMyVideos(){
        fetch(`${window.MyAppGlobals.apiURL}api/getMyVideos.php`,{
            method: 'GET',
            credentials: "include"
        })
        .then(res=>res.json())
        .then(res=>{
            if(res.STATUS == "SUCCESS"){
                console.log(res.myvideos);
                this.myVideos = res.myvideos;

            }else{
                this.showError("Could not get your playlists");
            }
        })
    }
}
customElements.define('my-videos-view', MyVideosView);