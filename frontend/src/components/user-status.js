import { LitElement, html, css } from 'lit-element';
import '@polymer/paper-button/paper-button';
import '@polymer/app-route/app-route';
import store from './../store.js';
import {login, changePage} from './../actions/index.js';

class UserStatus extends LitElement {

    static get properties(){
        return{
            isLoggedIn:{
                type: Boolean,
                value: false
            },
            isTeacher:{
                type: Boolean,
                value: false
            },
            isStudent:{
                type: Boolean,
                value: false
            },
            isAdmin:{
                type: Boolean,
                value: false
            },
            email:{
                type: String,
            },
            uuid:{
                type: String,
            }
        };
    }

    constructor(){
        super();
        const unsubscribe = store.subscribe(()=>this.updateStatus(store.getState()));
    }

    static get styles(){
        return css`
            paper-button.white{
                background-color: white;
            }
            paper-button.small{
                padding-top: 5px;
                padding-bottom: 5px;
            }

        `;
    }

    render(){
        return html`
        ${(this.isLoggedIn ? 
            html`
            <paper-button @click="${this.logout}">Logg ut</paper-button>`
            : 
            html`
            <a href="${window.MyAppGlobals.rootPath}loginView">
                <paper-button class="white small" toggles raised>
                    Logg inn
                </paper-button>
            </a>
            <a href="${window.MyAppGlobals.rootPath}registerView">
                <paper-button class="small white" toggles>Registrer</paper-button>
            </a>
            `)}
        `;
    }


   /* render() {
        return html`
${this.isLoggedIn ? html`
    <!-- Bruker er logget inn. Vis email og logut knapp-->
    <div><p>${this.email}</p> <button @click="${this.logout}">Logg av</button></div>
    `:html`
        <!-- Bruker er ikke logger in. Vis login knapp-->
        <form onsubmit="javascript: return false;">
            <label for="email">Epost</label>
            <input type="text" id="email" name="email">
            <label for="password">Passord</label>
            <input type="password" id="password" name="password">
            <label for="rememberPassword">Husk meg?</label>
            <input type="checkbox" id="rememberPassword" name="rememberPassword">
            <button @click="${this.loginForm}">Logg inn</button>
        </form>
    `}        
        `;
    }*/

    firstUpdated(changedProperties){
        
        // Get the user logged in status from the server
        fetch(`${window.MyAppGlobals.apiURL}api/login.php`,{
            method: 'POST',
            credentials: "include",
        })
        .then(res=>res.json())
        .then(res=>{
            //console.log(res);
            if(res.status == 'SUCCESS'){ //Login successfull
                store.dispatch(login({isLoggedIn: true, email: res.email, uuid: res.uuid, isStudent: res.isStudent, isTeacher: res.isTeacher, isAdmin: res.isAdmin}));
                //console.log(res);
            }else{
                store.dispatch(login({isLoggedIn: false}));

            }
        })
    }


    logout(event){
        //console.log(event.target);
        fetch(`${window.MyAppGlobals.apiURL}api/logout.php`,{
            method: 'POST',
            credentials: "include",
        })
        .then(res=>res.json())
        .then(res=>{
            if(res.status == "SUCCESS")
            store.dispatch(login({isLoggedIn: false, isStudent: false, isTeacher: false, isAdmin: false}));
        })
    }

    /**
     * Gets called by the store. It's registeres as a listener in the constructor.
     * 
     * @param {*} status 
     */
    updateStatus(status){
        //console.log("updateStatus");
        //console.log(status);
        //console.log(window.location.pathname);
        this.isLoggedIn = status.userstatus.isLoggedIn;
    }
}
customElements.define('user-status', UserStatus);