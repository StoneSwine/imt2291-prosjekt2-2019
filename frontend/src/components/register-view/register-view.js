import { LitElement, html, css } from 'lit-element';
import '@polymer/paper-input/paper-input';
import '@polymer/paper-checkbox/paper-checkbox';
import '@polymer/paper-button/paper-button';
import {login} from './../../actions/index';
import store from './../../store';
class RegisterView extends LitElement {

    static get properties(){
        return {
            toast: Object
        };
    }
    static get styles(){
        return css`
        :host{
            width: 100%;
        }
        #register-container{
            padding-left: 50px;
            padding-right: 50px;
            padding-bottom: 50px;
            background: whitesmoke;
        }
        `;
    }

    render() {
        return html`
        <h1>Registrer deg</h1>
        <hr>
        <div id="register-container">
                <paper-input 
                        type="input"
                        id="email"
                        label="Epost">
                </paper-input>
                <paper-input 
                        type="input"
                        id="fname"
                        label="Fornavn">
                </paper-input>
                <paper-input 
                        type="input"
                        id="lname"
                        label="Etternavn">
                </paper-input>
                <paper-input 
                        type="password"
                        id="password"
                        label="Passord">
                </paper-input>
                <paper-input 
                        type="password"
                        id="confirmedPassword"
                        label="Gjenta passord">
                </paper-input>
                <paper-checkbox 
                        id="teacherCheckbox">
                Huk av hvis du er lærer
                </paper-checkbox>
                <paper-button @click="${this.register}">Fullfør registrering</paper-button>
        </div>
        `;
    }

    constructor(){
        super();
    }

    register(event){
        console.log("Bruker forsøker å registrere seg");
        const data = new FormData();
        const email = (this.shadowRoot.querySelector("#email")).value;
        if(email){
            data.append("email", email);
        }
        const firstname = (this.shadowRoot.querySelector("#fname")).value;
        if(firstname){
            data.append("firstname", firstname);
        }
        const lastname = (this.shadowRoot.querySelector("#lname")).value;
        if(lastname){
            data.append("lastname", lastname);
        }
        const password = (this.shadowRoot.querySelector("#password")).value;
        if(password){
            data.append("password", password);
        } 
        const confirmedPassword = (this.shadowRoot.querySelector("#confirmedPassword")).value;
        if(confirmedPassword){
            data.append("confirmedPassword", confirmedPassword);
        }
        const isTeacher = (this.shadowRoot.querySelector("#teacherCheckbox")).value;
        if(isTeacher){
            data.append("teacherCheckbox", password);
        }
        
        fetch(`${window.MyAppGlobals.apiURL}api/registerUser.php`,{
            method: "POST",
            credentials: "include",
            body: data
        }).then(res=>res.json())
        .then(res=>{
            console.log(res);
            if(res.status === 'SUCCESS'){ //Login successfull
                store.dispatch(login({isLoggedIn: true, email: res.email, uuid: res.uuid, isStudent: res.isStudent, isTeacher: res.isTeacher, isAdmin: res.isAdmin}));
                //store.dispatch(changePage("/recent"));
                //console.log(res);
            }else{
                //Show error message
                this.showError(res.message);
                store.dispatch(login({isLoggedIn: false}));
            }
        });
    }
    showError(msg){
        this.toast.text = msg;
        this.toast.open();
    }
}
customElements.define('register-view', RegisterView);